<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'C_edom';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['deploy-git'] = 'C_deploy';
$route['deploy-admin'] = 'C_deploy/add_admin';
$route['edom'] = 'C_edom';
$route['about'] = 'C_about';
$route['login'] = 'C_edom/login';
$route['register'] = "C_edom/register";
$route['forgot-password'] = "C_edom/forgot_password";
$route['logout'] = "C_edom/logout";
$route['home'] = 'C_home';
$route['building'] = 'C_building';
$route['farm'] = 'C_farm';
$route['army'] = 'C_army';
$route['messages'] = 'C_messages';
$route['reports'] = 'C_reports';
$route['players'] = 'C_players';
$route['player-profile'] = 'C_player_profile';
$route['send-attack'] = 'C_player_profile/send_attack';
$route['send-reinforcement'] = 'C_player_profile/send_reinforcement';
$route['send-resources'] = 'C_player_profile/send_resources';
$route['send-message'] = 'C_player_profile/send_message';
$route['send-report-to-mod'] = 'C_messages/send_report_to_mod';
$route['delete-message'] = 'C_messages/delete_message';
$route['read-message'] = 'C_messages/read_message';
$route['delete-report'] = 'C_reports/delete_report';
$route['read-report'] = 'C_reports/read_report';
$route['upgrade-farm'] = 'C_farm/upgrade_farm';
$route['upgrade-building'] = 'C_building/upgrade_building';
$route['make-soldiers'] = 'C_building/make_soldiers';
$route['return-reinforcement'] = 'C_army/return_reinforcement';
$route['bringback-reinforcement'] = 'C_army/bringback_reinforcement';
$route['admin'] = 'C_admin';
$route['moderator'] = 'C_moderator';
$route['player-profile-admin'] = 'C_player_profile_admin';
$route['player-profile-mod'] = 'C_player_profile_mod';
$route['messages-admin'] = 'C_messages_admin';
$route['messages-mod'] = 'C_messages_mod';
$route['send-report-to-admin'] = 'C_messages_mod/send_report_to_admin';
$route['create-new-mod'] = 'C_admin/create_new_mod';
$route['delete-user'] = 'C_admin/delete_user';
$route['block-user'] = 'C_admin/block_user';
$route['send-message-to-mod'] = 'C_admin/send_message_to_mod';
$route['read-message-mod'] = 'C_player_profile_mod/read_message';
$route['read-report-mod'] = 'C_player_profile_mod/read_report';
$route['change-info'] = 'C_player_profile_admin/change_info';
$route['delete-report-admin'] = 'C_player_profile_admin/delete_report';
$route['delete-message-admin'] = 'C_player_profile_admin/delete_message';
$route['send-message-mod'] = 'C_player_profile_mod/send_message';

