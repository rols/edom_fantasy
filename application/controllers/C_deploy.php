<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_deploy extends CI_Controller
{
    public function index()
    {
        $repo_dir = dirname(dirname(dirname(__FILE__)));
        //$repo_bitbucket_public = "https://rols@bitbucket.org/rols/edom_fantasy.git";


        print('<pre>' .$repo_dir ."</pre>");

        chdir($repo_dir);
        //exec('git pull '.$repo_bitbucket_public, $out);
        exec('git pull', $out);
        foreach($out as $msg){
            print('<pre>' ."git: " .$msg ."</pre>");
        }
        exec('php application/doctrine_schema_update.php', $out2);
        foreach($out2 as $msg){
            print('<pre>' ."Doctrine schema update: " .$msg ."</pre>");
        }
    }
    
    public function add_admin()
    {
        if ($this->m_user->register("admin", "admin@admin.admin", "dandelion55555", "admin") == true)
            $data = array ('alert_message' => 'Registration successful.', 'alert_class' => 'alert-success');

        else
            $data = array ('alert_message' => 'Username or email is already in use.', 'alert_class' => 'alert-danger');

        $this->load->view('v_edom', $data);
    }    
}