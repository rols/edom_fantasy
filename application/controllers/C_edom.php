<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_edom extends CI_Controller
{
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function index()
	{
		session_start();
		if (isset($_SESSION['username']) == false)
			$this->load->view('v_edom');
		else
		{
			if ($this->m_user->checkForRole($_SESSION['username'], 'player') == true)
				redirect('home');
			else if ($this->m_user->checkForRole($_SESSION['username'], 'moderator') == true)
				redirect('moderator');
			else if ($this->m_user->checkForRole($_SESSION['username'], 'admin') == true)
				redirect('admin');
			else
				$this->load->view('v_edom');
		}
	}

	public function login()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('username', 'username', 'required|max_length[20]');
		$this->form_validation->set_rules('password', 'password', 'required|max_length[20]');

		if ($this->form_validation->run() == false)
		{
			$data = array ('alert_message' => 'Please enter parameters in order to log in.', 'alert_class' => 'alert-danger');
			$this->load->view('v_edom', $data);
		}
		else
		{
			$username = $this->input->post('username');
			$password = $this->input->post('password');

			$role = $this->m_user->login($username, $password);

			if ($role != null)
			{
				if ($role == "blocked")
				{
					$data = array ('alert_message' => 'You can\'t log in because you have been blocked.', 'alert_class' => 'alert-danger');
					$this->load->view('v_edom', $data);
				}

				else
				{
					session_start();
					$_SESSION["username"] = $username;

					if (strcmp($role, 'admin') == 0)
						redirect('admin');
					elseif (strcmp($role, 'mod') == 0)
						redirect('moderator');
					else
						redirect('home');
				}
			}

			else
			{
				$data = array ('alert_message' => 'Either the username does not exist or the password is wrong.', 'alert_class' => 'alert-danger');
				$this->load->view('v_edom', $data);
			}
		}
	}

	public function register()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('username', 'username', 'required|max_length[20]');
		$this->form_validation->set_rules('email', 'email', 'required|max_length[30]|valid_email');
		$this->form_validation->set_rules('password', 'password', 'required|max_length[20]');
		$this->form_validation->set_rules('repeatPassword', 'repeat password', 'required|max_length[20]|matches[password]');

		if ($this->form_validation->run() == FALSE)
		{
			$data = array ('alert_message' => 'Please enter correct parameters in order to register.', 'alert_class' => 'alert-danger');
			$this->load->view('v_edom', $data);
		}

		else
		{
			$username = $this->input->post('username');
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$nation = $this->input->post('nation');

			if ($nation != 'Men' && $nation != 'Elves' && $nation != 'Dwarves' && $nation != 'Orcs')
			{
				$data = array ('alert_message' => 'Please choose correct nation.', 'alert_class' => 'alert-danger');
				$this->load->view('v_edom', $data);
			}

			else
			{
				if ($this->m_user->register($username, $email, $password, $nation) == true)
					$data = array('alert_message' => 'Registration successful.', 'alert_class' => 'alert-success');

				else
					$data = array('alert_message' => 'Username or email is already in use.', 'alert_class' => 'alert-danger');

				$this->load->view('v_edom', $data);
			}
		}
	}

	public function forgot_password()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('email', 'email', 'required|max_length[30]|valid_email');

		if ($this->form_validation->run() == FALSE)
		{
			$data = array ('alert_message' => 'Please enter correct parameters in order to retrieve password.', 'alert_class' => 'alert-danger');
			$this->load->view('v_edom', $data);
		}

		else
		{
			$email = $this->input->post('email');
			$user = $this->m_user->getPlayerByEmail($email);

			if ($user == false)
			{
				$data = array ('alert_message' => 'User with this email does not exists.', 'alert_class' => 'alert-danger');
				$this->load->view('v_edom', $data);
			}

			else
			{
				if ($this->sendEmail($user) == true)
				{
					$data = array ('alert_message' => 'Your password has been sent to your email.', 'alert_class' => 'alert-success');
					$this->load->view('v_edom', $data);
				}

				else
				{
					$data = array ('alert_message' => 'There were some errors with network, please try again later.', 'alert_class' => 'alert-danger');
					$this->load->view('v_edom', $data);
				}
			}
		}
	}

	public function logout()
	{
		session_start();
		session_destroy();
		redirect(base_url());
	}

	private function sendEmail($user)
	{
		try
		{
			$this->load->library('email');
			$this->email->from('admin@edomfantasy.xyz', 'Team Edom');
			$this->email->to($user->getEmail());
			$this->email->subject('Edom Fantasy - Password');
			$this->email->message('Hi ' . $user->getUsername() . "!\nYou have recently requested your password. Your password is: " . $user->getPassword() . "\nKind regards,\nEdom Team");
			$this->email->send();
			return true;
		}
		catch(Exception $e)
		{
			return false;
		}

	}
}
