<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_players extends CI_Controller
{
    public function index()
    {
        session_start();
        if (isset($_SESSION['username']) == false or $this->m_user->checkForRole($_SESSION['username'], 'player') == false)
        {
            redirect(base_url());
        }

        else
        {
            $data = $this->getDataForView();
            $this->load->view('v_players', $data);
        }
    }

    private function getDataForView()
    {
        $data = array();

        $data['numOfReports'] = $this->m_user->checkForNewReports($_SESSION['username']);
        $data['numOfMessages'] = $this->m_user->checkForNewMessages($_SESSION['username']);
        $data['currentResources'] = $this->m_user->getCurrentResources($_SESSION['username']);
        $data['productionIncrement'] = $this->m_user->getCurrentProductionIncrement($_SESSION['username']);
        $data['players'] = $this->m_user->getAllPlayers();

        return $data;
    }
}