<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_army extends CI_Controller
{
    public function index()
    {
        session_start();
        if (isset($_SESSION['username']) == false or $this->m_user->checkForRole($_SESSION['username'], 'player') == false)
        {
            redirect(base_url());
        }

        else
        {
            $data = $this->getDataForView();
            $this->load->view('v_army', $data);
        }
    }

    public function return_reinforcement() #return received reinforcement
    {
        $data = array();
        $data['outcome'] = 'false';
        if ($this->input->post('isAjaxCalling') == true) {
            session_start();
            $this->load->model('m_reinforcement');
            $id = $this->input->post('id');
            if ($this->m_reinforcement->checkIfReceiver($id, $_SESSION['username']) != false)
            {
                $soldiers = $this->m_reinforcement->removeReinforcement($id);
                if ($soldiers != false)
                {
                    $buildingType = '';
                    $soldierType = 1;
                    if ($soldiers['type'] == 'Sword')
                        $buildingType = 'Barracks';
                    else if ($soldiers['type'] == 'Spear')
                    {
                        $buildingType = 'Barracks';
                        $soldierType = 2;
                    }
                    else if($soldiers['type'] == 'Cavalry')
                        $buildingType = 'Stable';
                    else
                        $buildingType = 'Archery';

                    $this->load->model('m_army');
                    $this->m_army->updateCount($soldiers['sender'], $buildingType, $soldierType, $soldiers['count']);

                    $data['outcome'] = 'true';
                }
            }
        }
        echo json_encode($data);
    }

    public function bringback_reinforcement() #bring back sent reinforcement
    {
        $data = array();
        $data['outcome'] = 'false';
        if ($this->input->post('isAjaxCalling') == true) {
            session_start();
            $this->load->model('m_reinforcement');
            $id = $this->input->post('id');
            if ($this->m_reinforcement->checkIfSender($id, $_SESSION['username']) != false)
            {
                $soldiers = $this->m_reinforcement->removeReinforcement($id);
                if ($soldiers != false)
                {
                    $buildingType = '';
                    $soldierType = 1;
                    if ($soldiers['type'] == 'Sword')
                        $buildingType = 'Barracks';
                    else if ($soldiers['type'] == 'Spear')
                    {
                        $buildingType = 'Barracks';
                        $soldierType = 2;
                    }
                    else if($soldiers['type'] == 'Cavalry')
                        $buildingType = 'Stable';
                    else
                        $buildingType = 'Archery';

                    $this->load->model('m_army');
                    $this->m_army->updateCount($soldiers['sender'], $buildingType, $soldierType, $soldiers['count']);

                    $data['outcome'] = 'true';
                }

                $data['outcome'] = 'true';
            }
        }
        echo json_encode($data);
    }

    private function getDataForView()
    {
        $data = array();

        $data['numOfReports'] = $this->m_user->checkForNewReports($_SESSION['username']);
        $data['numOfMessages'] = $this->m_user->checkForNewMessages($_SESSION['username']);
        $data['currentResources'] = $this->m_user->getCurrentResources($_SESSION['username']);
        $data['armies'] = $this->m_user->getAllArmies($_SESSION['username']);
        $data['sentReinforcements'] = $this->m_user->getSentReinforcements($_SESSION['username']);
        $data['receivedReinforcements'] = $this->m_user->getReceivedReinforcements($_SESSION['username']);
        $data['productionIncrement'] = $this->m_user->getCurrentProductionIncrement($_SESSION['username']);

        return $data;
    }
}