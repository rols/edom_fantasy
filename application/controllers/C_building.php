<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_building extends CI_Controller
{

    public function index()
    {
        session_start();
        if (isset($_SESSION['username']) == false or $this->m_user->checkForRole($_SESSION['username'], 'player') == false)
        {
            redirect(base_url());
        }

        $type = $this->input->get('type');
        $building = $this->m_user->getBuilding($_SESSION['username'], $type);

        if($building == false)
        {
            redirect('home');
        }

        else
        {
            $data = $this->getDataForView($building);
            $data['idbuild'] = $type;
            $this->load->view('v_building', $data);
        }
    }

    public function upgrade_building(){
        $data = array();
        $data['outcome'] = "false";

        if($this->input->post('isAjaxCalling') == true)
        {
            session_start();

            $building = $this->m_user->getBuilding($_SESSION['username'], $this->input->post('building'));

            if($building != false && $building->getLevel() < 5)
            {
                $currentResources = $this->m_user->getCurrentResources($_SESSION['username']);
                $nation = $this->m_user->getPlayer($_SESSION['username'])->getNation();
                $resources = array();
                $resources['level'] = $building->getLevel();
                $resources['building'] = $building->getType();
                $resources = $this->getResourcesNeededForUpgrade($resources, $nation);

                if($currentResources['gold'] >= $resources['goldNeeded'] && $currentResources['food'] >= $resources['foodNeeded'] && $currentResources['wood'] >= $resources['woodNeeded'])
                {
                    $this->load->model('m_building');

                    if ($this->m_user->takeResources($_SESSION['username'], $resources['goldNeeded'], $resources['foodNeeded'], $resources['woodNeeded']) == true && $this->m_building->upgradeBuilding($building) == true
                        && $this->m_user->updateEdompoints($_SESSION['username'], 100) == true)
                        $data['outcome'] = "true";
                    else
                        $data['outcome'] = "false";
                }
            }
        }
        echo json_encode($data);
    }

    public function make_soldiers()
    {
        $data = array();
        $data['outcome'] = "false";

        if($this->input->post('isAjaxCalling') == true)
        {
            session_start();

            $this->load->library('form_validation');
            $this->form_validation->set_rules('numOfSoldiers', 'numOfSoldiers', 'is_natural');

            if ($this->form_validation->run() == true)
            {
                $building = $this->m_user->getBuilding($_SESSION['username'], $this->input->post('building'));
                $soldierType = $this->input->post('soldierType');

                if ($building != false && ($soldierType == 1 || ($soldierType == 2 && $building->getType() == 'Barracks')))
                {
                    $numOfSoldiers = $this->input->post('numOfSoldiers');
                    $user = $this->m_user->getPlayer($_SESSION['username']);
                    $nation = $user->getNation();

                    $varData = array();
                    $varData['building'] = $building->getType();
                    $varData['level'] = $building->getLevel();

                    if ($building->getLevel() < 5) {
                        $varData = $this->getNumOfTakenSlots($varData, $user);
                        $varData = $this->getNumOfAvailableSlots($varData, $nation);
                    }

                    if ($building->getLevel() == 5 || ($varData['slotsTaken'] + $numOfSoldiers <= $varData['slotsAvailable']))
                    {
                        $currentResources = $this->m_user->getCurrentResources($_SESSION['username']);
                        $varData = $this->getResourcesNeededForSoldiers($varData, $nation);
                        if ($soldierType == 1)
                        {
                            $goldNeeded = $varData['soldier1Gold'];
                            $foodNeeded = $varData['soldier1Food'];
                            $woodNeeded = $varData['soldier1Wood'];
                        }
                        else
                        {
                            $goldNeeded = $varData['soldier2Gold'];
                            $foodNeeded = $varData['soldier2Food'];
                            $woodNeeded = $varData['soldier2Wood'];
                        }
                        if ($currentResources['gold'] >= $goldNeeded * $numOfSoldiers && $currentResources['food'] >= $foodNeeded * $numOfSoldiers && $currentResources['wood'] >= $woodNeeded * $numOfSoldiers)
                        {
                            $this->load->model('m_army');

                            if ($this->m_user->takeResources($_SESSION['username'], $goldNeeded * $numOfSoldiers, $foodNeeded * $numOfSoldiers, $woodNeeded * $numOfSoldiers) == true
                                && $this->m_army->updateCount($user, $building->getType(), $soldierType, $numOfSoldiers) == true
                                && $this->m_user->updateEdompoints($_SESSION['username'], $numOfSoldiers) == true
                            )
                                $data['outcome'] = "true";
                            else
                                $data['outcome'] = "false";
                        }
                    }
                }
            }
        }
        echo json_encode($data);
    }

    private function getDataForView($building)
    {
        $data = array();

        $data['numOfReports'] = $this->m_user->checkForNewReports($_SESSION['username']);
        $data['numOfMessages'] = $this->m_user->checkForNewMessages($_SESSION['username']);
        $data['currentResources'] = $this->m_user->getCurrentResources($_SESSION['username']);
        $data['productionIncrement'] = $this->m_user->getCurrentProductionIncrement($_SESSION['username']);
        $data['building'] = $building->getType();
        $data['level'] = $building->getLevel();

        $user = $this->m_user->getPlayer($_SESSION['username']);
        $nation = $user->getNation();
        $data['slotsAvailable'] = 0;

        $data = $this->getNumOfTakenSlots($data, $user);
        $data = $this->getNumOfAvailableSlots($data, $nation);
        $data = $this->getResourcesNeededForUpgrade($data, $nation);
        $data = $this->getResourcesNeededForSoldiers($data, $nation);

        if ($data['building'] == 'Barracks')
        {
            $data['numOfSoldiers'] = 2;
        }
        else if($data['building'] == 'Stable')
        {
            $data['numOfSoldiers'] = 1;
        }
        else
        {
            $data['numOfSoldiers'] = 1;
        }

        if ($nation == 'Men')
        {
            if ($data['building'] == 'Barracks')
            {
                $data['building'] = 'Military Barracks';
                $data['soldier1Name'] = 'Swordsman';
                $data['soldier2Name'] = 'Spearman';
                $data['soldier1Picture'] = 'assets/images/army/Swordsman.jpg';
                $data['soldier2Picture'] = 'assets/images/army/Spearman.jpg';
                $data['soldier1Attack'] = 60;
                $data['soldier1Defense'] = 20;
                $data['soldier2Attack'] = 20;
                $data['soldier2Defense'] = 70;
            }
            else if ($data['building'] == 'Stable')
            {
                $data['soldier1Name'] = 'Horseman';
                $data['soldier1Picture'] = 'assets/images/army/Horseman.jpg';
                $data['soldier1Attack'] = 80;
                $data['soldier1Defense'] = 40;
            }
            else
            {
                $data['building'] = 'Archery Range';
                $data['soldier1Name'] = 'Archer';
                $data['soldier1Picture'] = 'assets/images/army/Archer.jpg';
                $data['soldier1Attack'] = 40;
                $data['soldier1Defense'] = 30;
            }
            $data['picture'] = 'assets/images/army/armyman.jpg';
        }
        else if ($nation == 'Elves')
        {
            if ($data['building'] == 'Barracks')
            {
                $data['building'] = 'Elven Barracks';
                $data['soldier1Name'] = 'Elven Swordsman';
                $data['soldier2Name'] = 'Elven Phalanx';
                $data['soldier1Picture'] = 'assets/images/army/ElvenSwordsman.jpg';
                $data['soldier2Picture'] = 'assets/images/army/ElvenPhalanx.jpg';
                $data['soldier1Attack'] = 65;
                $data['soldier1Defense'] = 20;
                $data['soldier2Attack'] = 15;
                $data['soldier2Defense'] = 80;
            }
            else if ($data['building'] == 'Stable')
            {
                $data['building'] = 'Elven Stable';
                $data['soldier1Name'] = 'Elven Rider';
                $data['soldier1Picture'] = 'assets/images/army/ElvenRider.jpg';
                $data['soldier1Attack'] = 60;
                $data['soldier1Defense'] = 45;
            }
            else
            {
                $data['building'] = 'Elven Archery';
                $data['soldier1Name'] = 'Elven Archer';
                $data['soldier1Picture'] = 'assets/images/army/ElvenArcher.jpg';
                $data['soldier1Attack'] = 80;
                $data['soldier1Defense'] = 35;
            }
            $data['picture'] = 'assets/images/army/armyelf.jpg';
        }
        else if ($nation == 'Dwarves')
        {
            if ($data['building'] == 'Barracks')
            {
                $data['building'] = 'Dwarven Barracks';
                $data['soldier1Name'] = 'Dwarven Infantry';
                $data['soldier2Name'] = 'Dvarven Phalanx';
                $data['soldier1Picture'] = 'assets/images/army/DwarvenInfantry.jpg';
                $data['soldier2Picture'] = 'assets/images/army/DwarvenPhalanx.jpg';
                $data['soldier1Attack'] = 35;
                $data['soldier1Defense'] = 50;
                $data['soldier2Attack'] = 15;
                $data['soldier2Defense'] = 100;
            }
            else if ($data['building'] == 'Stable')
            {
                $data['building'] = 'Dwarven Stable';
                $data['soldier1Name'] = 'Chariot';
                $data['soldier1Picture'] = 'assets/images/army/Chariot.jpg';
                $data['soldier1Attack'] = 55;
                $data['soldier1Defense'] = 30;
            }
            else
            {
                $data['building'] = 'Dwarven Axle';
                $data['soldier1Name'] = 'Axe Thrower';
                $data['soldier1Picture'] = 'assets/images/army/AxeThrower.jpg';
                $data['soldier1Attack'] = 25;
                $data['soldier1Defense'] = 60;
            }
            $data['picture'] = 'assets/images/army/armydwarf.jpg';
        }
        else
        {
            if ($data['building'] == 'Barracks')
            {
                $data['building'] = 'Orc Pit';
                $data['soldier1Name'] = 'Orc Swordsman';
                $data['soldier2Name'] = 'Orc Spearman';
                $data['soldier1Picture'] = 'assets/images/army/OrcSwordsman.jpg';
                $data['soldier2Picture'] = 'assets/images/army/OrcSpearman.jpg';
                $data['soldier1Attack'] = 45;
                $data['soldier1Defense'] = 5;
                $data['soldier2Attack'] = 5;
                $data['soldier2Defense'] = 50;
            }
            else if ($data['building'] == 'Stable')
            {
                $data['building'] = 'Troll Cave';
                $data['soldier1Name'] = 'Troll';
                $data['soldier1Picture'] = 'assets/images/army/Troll.jpg';
                $data['soldier1Attack'] = 100;
                $data['soldier1Defense'] = 50;
            }
            else
            {
                $data['building'] = 'Orc Archery';
                $data['soldier1Name'] = 'Orc Archer';
                $data['soldier1Picture'] = 'assets/images/army/OrcArcher.jpg';
                $data['soldier1Attack'] = 40;
                $data['soldier1Defense'] = 30;
            }
            $data['picture'] = 'assets/images/army/armyorc.png';
        }

        return $data;
    }

    private function getResourcesNeededForUpgrade($data, $nation)
    {
        if ($nation == 'Men') {
            $priceScale = 1.3;
            if ($data['building'] == 'Barracks')
            {
                $data['goldNeeded'] = 4000 * pow($priceScale, $data['level']);
                $data['foodNeeded'] = 3500 * pow($priceScale, $data['level']);
                $data['woodNeeded'] = 5000 * pow($priceScale, $data['level']);
            }
            else if ($data['building'] == 'Stable')
            {
                $data['goldNeeded'] = 6000 * pow($priceScale, $data['level']);
                $data['foodNeeded'] = 4300 * pow($priceScale, $data['level']);
                $data['woodNeeded'] = 5800 * pow($priceScale, $data['level']);
            }
            else
            {
                $data['goldNeeded'] = 3300 * pow($priceScale, $data['level']);
                $data['foodNeeded'] = 3500 * pow($priceScale, $data['level']);
                $data['woodNeeded'] = 4200 * pow($priceScale, $data['level']);
            }
        }
        else if ($nation == 'Elves')
        {
            $priceScale = 1.35;
            if ($data['building'] == 'Barracks')
            {
                $data['goldNeeded'] = 3800 * pow($priceScale, $data['level']);
                $data['foodNeeded'] = 3600 * pow($priceScale, $data['level']);
                $data['woodNeeded'] = 4800 * pow($priceScale, $data['level']);
            }
            else if ($data['building'] == 'Stable')
            {
                $data['goldNeeded'] = 5500 * pow($priceScale, $data['level']);
                $data['foodNeeded'] = 4400 * pow($priceScale, $data['level']);
                $data['woodNeeded'] = 6000 * pow($priceScale, $data['level']);
            }
            else
            {
                $data['goldNeeded'] = 3500 * pow($priceScale, $data['level']);
                $data['foodNeeded'] = 3200 * pow($priceScale, $data['level']);
                $data['woodNeeded'] = 5000 * pow($priceScale, $data['level']);
            }
        }
        else if ($nation == 'Dwarves')
        {
            $priceScale = 1.4;
            if ($data['building'] == 'Barracks')
            {
                $data['goldNeeded'] = 4300 * pow($priceScale, $data['level']);
                $data['foodNeeded'] = 3700 * pow($priceScale, $data['level']);
                $data['woodNeeded'] = 5200 * pow($priceScale, $data['level']);
            }
            else if ($data['building'] == 'Stable')
            {
                $data['goldNeeded'] = 6200 * pow($priceScale, $data['level']);
                $data['foodNeeded'] = 5200 * pow($priceScale, $data['level']);
                $data['woodNeeded'] = 6100 * pow($priceScale, $data['level']);
            }
            else
            {
                $data['goldNeeded'] = 4000 * pow($priceScale, $data['level']);
                $data['foodNeeded'] = 3300 * pow($priceScale, $data['level']);
                $data['woodNeeded'] = 4000 * pow($priceScale, $data['level']);
            }
        }
        else
        {
            $priceScale = 1.4;
            if ($data['building'] == 'Barracks')
            {
                $data['goldNeeded'] = 3000 * pow($priceScale, $data['level']);
                $data['foodNeeded'] = 2800 * pow($priceScale, $data['level']);
                $data['woodNeeded'] = 3200 * pow($priceScale, $data['level']);
            }
            else if ($data['building'] == 'Stable')
            {
                $data['goldNeeded'] = 5500 * pow($priceScale, $data['level']);
                $data['foodNeeded'] = 4200 * pow($priceScale, $data['level']);
                $data['woodNeeded'] = 5500 * pow($priceScale, $data['level']);
            }
            else
            {
                $data['goldNeeded'] = 2300 * pow($priceScale, $data['level']);
                $data['foodNeeded'] = 2800 * pow($priceScale, $data['level']);
                $data['woodNeeded'] = 3200 * pow($priceScale, $data['level']);
            }
        }
        $data['goldNeeded'] = intval($data['goldNeeded']);
        $data['foodNeeded'] = intval($data['foodNeeded']);
        $data['woodNeeded'] = intval($data['woodNeeded']);
        return $data;
    }

    private function getResourcesNeededForSoldiers($data, $nation)
    {
        if ($nation == 'Men')
        {
            if ($data['building'] == 'Barracks')
            {
                $data['soldier1Gold'] = 180;
                $data['soldier1Food'] = 200;
                $data['soldier1Wood'] = 120;
                $data['soldier2Gold'] = 160;
                $data['soldier2Food'] = 150;
                $data['soldier2Wood'] = 200;
            }
            else if ($data['building'] == 'Stable')
            {
                $data['soldier1Gold'] = 300;
                $data['soldier1Food'] = 250;
                $data['soldier1Wood'] = 280;
            }
            else
            {
                $data['soldier1Gold'] = 200;
                $data['soldier1Food'] = 180;
                $data['soldier1Wood'] = 250;
            }
        }
        else if ($nation == 'Elves')
        {
            if ($data['building'] == 'Barracks')
            {
                $data['soldier1Gold'] = 270;
                $data['soldier1Food'] = 250;
                $data['soldier1Wood'] = 220;
                $data['soldier2Gold'] = 240;
                $data['soldier2Food'] = 260;
                $data['soldier2Wood'] = 290;
            }
            else if ($data['building'] == 'Stable')
            {
                $data['soldier1Gold'] = 380;
                $data['soldier1Food'] = 390;
                $data['soldier1Wood'] = 450;
            }
            else
            {
                $data['soldier1Gold'] = 450;
                $data['soldier1Food'] = 430;
                $data['soldier1Wood'] = 550;
            }
        }
        else if ($nation == 'Dwarves')
        {
            if ($data['building'] == 'Barracks')
            {
                $data['soldier1Gold'] = 200;
                $data['soldier1Food'] = 180;
                $data['soldier1Wood'] = 100;
                $data['soldier2Gold'] = 250;
                $data['soldier2Food'] = 150;
                $data['soldier2Wood'] = 180;
            }
            else if ($data['building'] == 'Stable')
            {
                $data['soldier1Gold'] = 400;
                $data['soldier1Food'] = 270;
                $data['soldier1Wood'] = 250;
            }
            else
            {
                $data['soldier1Gold'] = 250;
                $data['soldier1Food'] = 200;
                $data['soldier1Wood'] = 250;
            }
        }
        else
        {
            if ($data['building'] == 'Barracks')
            {
                $data['soldier1Gold'] = 120;
                $data['soldier1Food'] = 130;
                $data['soldier1Wood'] = 100;
                $data['soldier2Gold'] = 80;
                $data['soldier2Food'] = 90;
                $data['soldier2Wood'] = 110;
            }
            else if ($data['building'] == 'Stable')
            {
                $data['soldier1Gold'] = 400;
                $data['soldier1Food'] = 420;
                $data['soldier1Wood'] = 400;
            }
            else
            {
                $data['soldier1Gold'] = 100;
                $data['soldier1Food'] = 120;
                $data['soldier1Wood'] = 190;
            }
        }
        return $data;
    }

    private function getNumOfTakenSlots($data, $user)
    {
        $this->load->model('m_reinforcement');
        if ($data['building'] == 'Barracks')
        {
            $data['slotsTaken'] = $user->getSword()->getCount() + $user->getSpear()->getCount() +
                $this->m_reinforcement->getNumOfSoldiersInSentReinforcement($user,  $user->getSword()->getType()) +
                $this->m_reinforcement->getNumOfSoldiersInSentReinforcement($user,  $user->getSpear()->getType());
        }
        else if($data['building'] == 'Stable')
        {
            $data['slotsTaken'] = $user->getCavalry()->getCount() +
                $this->m_reinforcement->getNumOfSoldiersInSentReinforcement($user,  $user->getCavalry()->getType());
        }
        else
        {
            $data['slotsTaken'] = $user->getBow()->getCount() +
                $this->m_reinforcement->getNumOfSoldiersInSentReinforcement($user,  $user->getBow()->getType());
        }
        return $data;
    }

    private function getNumOfAvailableSlots($data, $nation){ #total number of available slots(including taken and non taken)
        if ($nation == 'Men')
        {
            if ($data['building'] == 'Barracks')
            {
                if($data['level'] > 0) $data['slotsAvailable'] = 1000 * pow(1.5, $data['level'] - 1);
                else $data['slotsAvailable'] = 0;

            }
            else if ($data['building'] == 'Stable')
            {
                if($data['level'] > 0) $data['slotsAvailable'] = 500 * pow(1.5, $data['level'] - 1);
                else $data['slotsAvailable'] = 0;
            }
            else
            {
                if($data['level'] > 0) $data['slotsAvailable'] = 1000 * pow(1.5, $data['level'] - 1);
                else $data['slotsAvailable'] = 0;
            }
        }
        else if ($nation == 'Elves')
        {
            if ($data['building'] == 'Barracks')
            {
                if($data['level'] > 0) $data['slotsAvailable'] = 500 * pow(1.4, $data['level'] - 1);
                else $data['slotsAvailable'] = 0;
            }
            else if ($data['building'] == 'Stable')
            {
                if($data['level'] > 0) $data['slotsAvailable'] = 200 * pow(1.4, $data['level'] - 1);
                else $data['slotsAvailable'] = 0;
            }
            else
            {
                if($data['level'] > 0) $data['slotsAvailable'] = 600 * pow(1.4, $data['level'] - 1);
                else $data['slotsAvailable'] = 0;
            }
        }
        else if ($nation == 'Dwarves')
        {
            if ($data['building'] == 'Barracks')
            {
                if($data['level'] > 0) $data['slotsAvailable'] = 1000 * pow(1.4, $data['level'] - 1);
                else $data['slotsAvailable'] = 0;
            }
            else if ($data['building'] == 'Stable')
            {
                if($data['level'] > 0) $data['slotsAvailable'] = 250 * pow(1.4, $data['level'] - 1);
                else $data['slotsAvailable'] = 0;
            }
            else
            {
                if($data['level'] > 0) $data['slotsAvailable'] = 800 * pow(1.4, $data['level'] - 1);
                else $data['slotsAvailable'] = 0;
            }
        }
        else
        {
            if ($data['building'] == 'Barracks')
            {
                if($data['level'] > 0) $data['slotsAvailable'] = 2000 * pow(1.7, $data['level'] - 1);
                else $data['slotsAvailable'] = 0;
            }
            else if ($data['building'] == 'Stable')
            {
                if($data['level'] > 0) $data['slotsAvailable'] = 1000 * pow(1.7, $data['level'] - 1);
                else $data['slotsAvailable'] = 0;
            }
            else
            {
                if($data['level'] > 0) $data['slotsAvailable'] = 2000 * pow(1.7, $data['level'] - 1);
                else $data['slotsAvailable'] = 0;
            }
        }

        $data['slotsAvailable'] = intval($data['slotsAvailable']);

        return $data;
    }
}