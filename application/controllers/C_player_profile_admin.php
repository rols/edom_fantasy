<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_player_profile_admin extends CI_Controller
{
    public function index()
    {
        session_start();
        if (isset($_SESSION['username']) == false or $this->m_user->checkForRole($_SESSION['username'], 'admin') == false)
        {
            redirect(base_url());
        }

        $username = $this->input->get('username');
        $player = $this->m_user->getPlayer($username);

        if($username == false || $username == $_SESSION['username'] || $player == false)
        {
            redirect('admin');
        }

        else
        {
            $data = $this->getDataForView($player);
            $this->load->view('v_player-profile-admin', $data);
        }
    }

    public function change_info()
    {
        if ($this->input->post('isAjaxCalling') == true)
        {
            session_start();
            $this->load->library('form_validation');

            $this->form_validation->set_rules('resgold', 'resgold', 'is_natural');
            $this->form_validation->set_rules('resfood', 'resfood', 'is_natural');
            $this->form_validation->set_rules('reswood', 'reswood', 'is_natural');
            $this->form_validation->set_rules('gold1level', 'gold1level', 'greater_than[-1]|less_than[6]');
            $this->form_validation->set_rules('gold2level', 'gold2level', 'greater_than[-1]|less_than[6]');
            $this->form_validation->set_rules('food1level', 'food1level', 'greater_than[-1]|less_than[6]');
            $this->form_validation->set_rules('food2level', 'food2level', 'greater_than[-1]|less_than[6]');
            $this->form_validation->set_rules('wood1level', 'wood1level', 'greater_than[-1]|less_than[6]');
            $this->form_validation->set_rules('wood2level', 'wood2level', 'greater_than[-1]|less_than[6]');
            $this->form_validation->set_rules('barrackslevel', 'barrackslevel', 'greater_than[-1]|less_than[6]');
            $this->form_validation->set_rules('stablelevel', 'stablelevel', 'greater_than[-1]|less_than[6]');
            $this->form_validation->set_rules('archerylevel', 'archerylevel', 'greater_than[-1]|less_than[6]');
            $this->form_validation->set_rules('sword', 'sword', 'is_natural');
            $this->form_validation->set_rules('spear', 'spear', 'is_natural');
            $this->form_validation->set_rules('cavalry', 'cavalry', 'is_natural');
            $this->form_validation->set_rules('bow', 'bow', 'is_natural');

            $player = $this->m_user->getPlayer($this->input->post('username'));

            if ($this->form_validation->run() == false || $player == false)
            {
                echo false;
            }

            else
            {
                $this->load->model('m_farm');
                $this->load->model('m_building');
                $this->load->model('m_army');

                $resgold = $this->input->post('resgold');
                if ($resgold != null)
                    $this->m_user->setGold($player, $resgold);

                $resfood = $this->input->post('resfood');
                if ($resfood != null)
                    $this->m_user->setFood($player, $resfood);

                $reswood = $this->input->post('reswood');
                if ($reswood != null)
                    $this->m_user->setWood($player, $reswood);

                $gold1level = $this->input->post('gold1level');
                if ($gold1level != null)
                    $this->m_farm->updateFarmLevel($player->getGoldfarm1(), $gold1level);

                $gold2level = $this->input->post('gold2level');
                if ($gold2level != null)
                    $this->m_farm->updateFarmLevel($player->getGoldfarm2(), $gold2level);

                $food1level = $this->input->post('food1level');
                if ($food1level != null)
                    $this->m_farm->updateFarmLevel($player->getFoodfarm1(), $food1level);

                $food2level = $this->input->post('food2level');
                if ($food2level != null)
                    $this->m_farm->updateFarmLevel($player->getFoodfarm2(), $food2level);

                $wood1level = $this->input->post('wood1level');
                if ($wood1level != null)
                    $this->m_farm->updateFarmLevel($player->getWoodfarm1(), $wood1level);

                $wood2level = $this->input->post('wood2level');
                if ($wood2level != null)
                    $this->m_farm->updateFarmLevel($player->getWoodfarm2(), $wood2level);

                $barrackslevel = $this->input->post('barrackslevel');
                if ($barrackslevel != null)
                    $this->m_building->updateBuildingLevel($player->getBarracks(), $barrackslevel);

                $stablelevel = $this->input->post('stablelevel');
                if ($stablelevel != null)
                    $this->m_building->updateBuildingLevel($player->getStable(), $stablelevel);

                $archerylevel = $this->input->post('archerylevel');
                if ($archerylevel != null)
                    $this->m_building->updateBuildingLevel($player->getArchery(), $archerylevel);

                $sword = $this->input->post('sword');
                if ($sword != null)
                    $this->m_army->updateArmyCount($player->getSword(), $sword);

                $spear = $this->input->post('spear');
                if ($spear != null)
                    $this->m_army->updateArmyCount($player->getSpear(), $spear);

                $cavalry = $this->input->post('cavalry');
                if ($cavalry != null)
                    $this->m_army->updateArmyCount($player->getCavalry(), $cavalry);

                $bow = $this->input->post('bow');
                if ($bow != null)
                    $this->m_army->updateArmyCount($player->getBow(), $bow);

                echo true;
            }
        }
    }

    public function delete_report()
    {
        if ($this->input->post('isAjaxCalling') == true)
        {
            session_start();
            $this->load->model('m_report');
            $idrep = $this->input->post('idrep');
            if ($this->m_user->checkForRole($_SESSION['username'], 'admin') == true)
                $this->m_report->deleteReport($idrep);
        }
    }

    public function delete_message()
    {
        if ($this->input->post('isAjaxCalling') == true)
        {
            session_start();
            $this->load->model('m_message');
            $idmes = $this->input->post('idmes');
            if ($this->m_user->checkForRole($_SESSION['username'], 'admin') == true)
                $this->m_message->deleteMessage($idmes);
        }
    }

    private function getDataForView($player)
    {
        $data = array();

        if (strcmp($player->getNation(), 'Men') == 0)
            $data['picture'] = 'assets/images/bgr/bard.jpg';

        elseif (strcmp ($player->getNation(), 'Elves') == 0)
            $data['picture'] = 'assets/images/bgr/thranduil.jpg';


        elseif (strcmp($player->getNation(), "Dwarves") == 0)
            $data['picture'] = 'assets/images/bgr/thorin.jpg';

        else
            $data['picture'] = 'assets/images/bgr/azog.png';

        $data['numOfMessages'] = $this->m_user->checkForNewMessages($_SESSION['username']);
        $data['username'] = $player->getUsername();
        $data['nation'] = $player->getNation();
        $data['edompoints'] = $player->getEdompoints();
        $data['currentResources'] = $this->m_user->getCurrentResourcesUser($player);
        $data['currentProduction'] = $this->m_user->getCurrentProductionUser($player);
        $data['armies'] = $this->m_user->getAllArmiesUser($player);
        $data['sentReinforcements'] = $this->m_user->getSentReinforcementsUser($player);
        $data['receivedReinforcements'] = $this->m_user->getReceivedReinforcementsUser($player);
        $data['reports'] = $this->m_user->getAllReportsUser($player);
        $data['messages'] = $this->m_user->getAllMessagesUser($player);

        $farms = array();
        $farms['gold1'] = $player->getGoldfarm1()->getLevel();
        $farms['gold2'] = $player->getGoldfarm2()->getLevel();
        $farms['food1'] = $player->getFoodfarm1()->getLevel();
        $farms['food2'] = $player->getFoodfarm2()->getLevel();
        $farms['wood1'] = $player->getWoodfarm1()->getLevel();
        $farms['wood2'] = $player->getWoodfarm2()->getLevel();
        $data['farms'] = $farms;

        $buildings = array();
        $nation = $player->getNation();
        if (strcmp($nation, 'Men') == 0)
        {
            $buildings['barracks'] = 'Military Barracks';
            $buildings['stable'] = 'Stable';
            $buildings['archery'] = 'Archery Range';
        }

        elseif (strcmp($nation, 'Elves') == 0)
        {
            $buildings['barracks'] = 'Elven Barracks';
            $buildings['stable'] = 'Elven Stable';
            $buildings['archery'] = 'Elven Archery';
        }


        elseif (strcmp($nation, "Dwarves") == 0)
        {
            $buildings['barracks'] = 'Dwarven Barracks';
            $buildings['stable'] = 'Dwarven Stable';
            $buildings['archery'] = 'Dwarven Axle';
        }

        else
        {
            $buildings['barracks'] = 'Orc Pit';
            $buildings['stable'] = 'Troll Cave';
            $buildings['archery'] = 'Orc Archery';
        }
        $buildings['barracksLevel'] = $player->getBarracks()->getLevel();
        $buildings['stableLevel'] = $player->getStable()->getLevel();
        $buildings['archeryLevel'] = $player->getArchery()->getLevel();
        $data['buildings'] = $buildings;


        return $data;
    }
}