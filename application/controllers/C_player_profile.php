<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class C_player_profile extends CI_Controller
{
    public function index()
    {
        session_start();
        if (isset($_SESSION['username']) == false or $this->m_user->checkForRole($_SESSION['username'], 'player') == false)
        {
            redirect(base_url());
        }

        $username = $this->input->get('username');
        $player = $this->m_user->getPlayer($username);

        if($username == false || $username == $_SESSION['username'] || $player == false)
        {
            redirect('players');
        }

        else
        {
            $data = $this->getDataForView($player);
            $this->load->view('v_player-profile', $data);
        }
    }

    public function send_attack()
    {
        if ($this->input->post('isAjaxCalling') == true)
        {
            session_start();
            $this->load->library('form_validation');
            $this->form_validation->set_rules('sword', 'sword', 'is_natural');
            $this->form_validation->set_rules('spear', 'spear', 'is_natural');
            $this->form_validation->set_rules('cavalry', 'cavalry', 'is_natural');
            $this->form_validation->set_rules('bow', 'bow', 'is_natural');
            $attacker = $this->m_user->getPlayer($_SESSION['username']);
            $defender = $this->m_user->getPlayer($this->input->post('defender'));

            if ($this->form_validation->run() == false || $defender == false || strcmp($attacker->getUsername(), $defender->getUsername()) == 0)
                echo false;
            else
            {
                $sword = $this->input->post('sword');
                $spear = $this->input->post('spear');
                $cavalry = $this->input->post('cavalry');
                $bow = $this->input->post('bow');


                if ($attacker->getSword()->getCount() >= $sword && $attacker->getSpear()->getCount() >= $spear &&
                    $attacker->getCavalry()->getCount() >= $cavalry && $attacker->getBow()->getCount() >= $bow)
                {
                    $timeToWait = $this->m_user->canSendAttack($attacker);

                    if($timeToWait == -1 || $timeToWait > 0)
                    {
                        echo $timeToWait;
                        return;
                    }

                    $this->load->model("M_soldier");

                    $offPoints = 0;
                    $soldier = M_soldier::$factory[M_soldier::getSoldierName($attacker->getNation(), $attacker->getSword()->getType())];
                    $offPoints += $sword * $soldier->offence;
                    $soldier = M_soldier::$factory[M_soldier::getSoldierName($attacker->getNation(), $attacker->getSpear()->getType())];
                    $offPoints += $spear * $soldier->offence;
                    $soldier = M_soldier::$factory[M_soldier::getSoldierName($attacker->getNation(), $attacker->getCavalry()->getType())];
                    $offPoints += $cavalry * $soldier->offence;
                    $soldier = M_soldier::$factory[M_soldier::getSoldierName($attacker->getNation(), $attacker->getBow()->getType())];
                    $offPoints += $bow * $soldier->offence;


                    $this->load->model("M_battlesimulator");
                    $this->M_battlesimulator->initialise($offPoints, $defender);
                    $winner = $this->M_battlesimulator->simulate();


                    if ($winner == "attacker")
                    {
                        $isWinner = true;
                        $this->m_user->updateResources($defender);
                        $goldTaken = intval(M_soldier::$raidPercentage[$attacker->getNation()] * $defender->getGold());
                        $foodTaken = intval(M_soldier::$raidPercentage[$attacker->getNation()] * $defender->getFood());
                        $woodTaken = intval(M_soldier::$raidPercentage[$attacker->getNation()] * $defender->getWood());
                        $this->m_user->addResourcesUser($attacker, $goldTaken, $foodTaken, $woodTaken);
                        $this->m_user->takeResourcesUser($defender, $goldTaken, $foodTaken, $woodTaken);
                        $this->m_user->updateEdompointsUser($attacker, 100);
                    }

                    else
                    {
                        $isWinner = false;
                        $goldTaken = 0;
                        $foodTaken = 0;
                        $woodTaken = 0;
                        $this->m_user->updateEdompointsUser($defender, 100);
                    }

                    $this->load->model("m_report");
                    $battleReport = $this->m_report->createBattleReport($attacker, $sword, $spear, $cavalry, $bow, $defender, $this->M_battlesimulator->offLostPerc, $this->M_battlesimulator->defSurvivedPerc, $isWinner, $goldTaken, $foodTaken, $woodTaken);

                    $this->load->model('m_army');

                    $this->m_army->updateArmyCount($attacker->getSword(), $attacker->getSword()->getCount() - intval(round($this->M_battlesimulator->offLostPerc * $sword / 100.0)));
                    $this->m_army->updateArmyCount($attacker->getSpear(), $attacker->getSpear()->getCount() - intval(round($this->M_battlesimulator->offLostPerc * $spear / 100.0)));
                    $this->m_army->updateArmyCount($attacker->getCavalry(), $attacker->getCavalry()->getCount() - intval(round($this->M_battlesimulator->offLostPerc * $cavalry / 100.0)));
                    $this->m_army->updateArmyCount($attacker->getBow(), $attacker->getBow()->getCount() - intval(round($this->M_battlesimulator->offLostPerc * $bow / 100.0)));

                    $this->m_army->updateArmyCount($defender->getSword(), intval(round($defender->getSword()->getCount() * $this->M_battlesimulator->defSurvivedPerc / 100.0)));
                    $this->m_army->updateArmyCount($defender->getSpear(), intval(round($defender->getSpear()->getCount() * $this->M_battlesimulator->defSurvivedPerc / 100.0)));
                    $this->m_army->updateArmyCount($defender->getCavalry(), intval(round($defender->getCavalry()->getCount() * $this->M_battlesimulator->defSurvivedPerc / 100.0)));
                    $this->m_army->updateArmyCount($defender->getBow(), intval(round($defender->getBow()->getCount() * $this->M_battlesimulator->defSurvivedPerc / 100.0)));

                    $this->load->model("m_reinforcement");
                    foreach ($defender->getReinforcementsReceived() as $reinforcement)
                    {
                        $this->m_reinforcement->updateReinforcementCount($reinforcement, intval(round($reinforcement->getCount() * $this->M_battlesimulator->defSurvivedPerc / 100.0)));
                    }

                    $this->m_report->sendReport($attacker, "Attack", $battleReport);
                    $this->m_report->sendReport($defender, "Defence", $battleReport);

                    echo -1;

                }
                else
                    echo false;
            }
        }
    }

    public function send_reinforcement()
    {
        if ($this->input->post('isAjaxCalling') == true)
        {
            session_start();
            $this->load->library('form_validation');
            $this->form_validation->set_rules('sword', 'sword', 'is_natural');
            $this->form_validation->set_rules('spear', 'spear', 'is_natural');
            $this->form_validation->set_rules('cavalry', 'cavalry', 'is_natural');
            $this->form_validation->set_rules('bow', 'bow', 'is_natural');
            $sender = $this->m_user->getPlayer($_SESSION['username']);
            $receiver = $this->m_user->getPlayer($this->input->post('receiver'));

            if ($this->form_validation->run() == false || $receiver == false || strcmp($sender->getUsername(), $receiver->getUsername()) == 0)
                echo false;
            else
            {
                $sword = $this->input->post('sword');
                $spear = $this->input->post('spear');
                $cavalry = $this->input->post('cavalry');
                $bow = $this->input->post('bow');

                $this->load->model('m_reinforcement');
                if ($this->m_reinforcement->sendReinforcement($sender, $receiver, $sword, $spear, $cavalry, $bow) == true)
                {
                    $this->load->model('m_army');

                    if ($sword > 0)
                    {
                        $this->m_army->updateCount($sender, "Barracks", 1 , -$sword);
                    }
                    if ($spear > 0)
                    {
                        $this->m_army->updateCount($sender, "Barracks", 2 , -$spear);
                    }
                    if ($cavalry > 0)
                    {
                        $this->m_army->updateCount($sender, "Stable", 1 , -$cavalry);
                    }
                    if ($bow > 0)
                    {
                        $this->m_army->updateCount($sender, "Archery", 1 , -$bow);
                    }

                    $this->load->model('m_report');

                    $content = $_SESSION['username'] . " has sent reinforcement to you:\n";
                    $content = $content . $this->m_user->getArmyFullName("Sword", $sender->getNation()) .": " . $sword . "\n";
                    $content = $content . $this->m_user->getArmyFullName("Spear", $sender->getNation()) .": " . $spear . "\n";
                    $content = $content . $this->m_user->getArmyFullName("Cavalry", $sender->getNation()) .": " . $cavalry . "\n";
                    $content = $content . $this->m_user->getArmyFullName("Bow", $sender->getNation()) .": " . $bow . "\n";

                    $this->m_report->sendReport($receiver, "Reinforcement", $content);
                    echo true;
                }
                else
                    echo false;
            }
        }
    }

    public function send_resources()
    {
        if ($this->input->post('isAjaxCalling') == true)
        {
            session_start();
            $this->load->library('form_validation');
            $this->form_validation->set_rules('gold', 'gold', 'is_natural');
            $this->form_validation->set_rules('food', 'food', 'is_natural');
            $this->form_validation->set_rules('wood', 'wood', 'is_natural');
            $sender = $this->m_user->getPlayer($_SESSION['username']);
            $receiver = $this->m_user->getPlayer($this->input->post('receiver'));

            if ($this->form_validation->run() == false || $receiver == false || strcmp($sender->getUsername(), $receiver->getUsername()) == 0)
                echo false;

            else
            {
                $gold = $this->input->post('gold');
                $food = $this->input->post('food');
                $wood = $this->input->post('wood');

                if($this->m_user->sendResources($sender, $receiver, $gold, $food, $wood) == true)
                {
                    $this->load->model('m_report');

                    $content = $_SESSION['username'] . " has sent resources to " . $receiver->getUsername() . ":\n";
                    $content = $content . "Gold: " . $gold . "\n";
                    $content = $content . "Food: " . $food . "\n";
                    $content = $content . "Wood: " . $wood . "\n";

                    $this->m_report->sendReport($receiver, "Resources received", $content);
                    $this->m_report->sendReport($sender, "Resources sent", $content);
                    echo true;
                }

                else
                    echo false;
            }
        }
    }

    public function send_message()
    {
        if ($this->input->post('isAjaxCalling') == true)
        {
            session_start();
            $this->load->library('form_validation');
            $this->form_validation->set_rules('subject', 'subject', 'required|max_length[30]');
            $this->form_validation->set_rules('content', 'content', 'required|max_length[200]');
            $this->form_validation->set_rules('receiver', 'receiver', 'required');
            $sender = $this->m_user->getPlayer($_SESSION['username']);
            $receiver = $this->m_user->getPlayer($this->input->post('receiver'));

            if ($this->form_validation->run() == false || $receiver == false || strcmp($sender->getUsername(), $receiver->getUsername()) == 0)
                echo false;

            else
            {
                $this->load->model('m_message');
                $subject = $this->input->post('subject');
                $content = $this->input->post('content');
                $subject = $this->m_user->cleanMessage($subject);
                $content = $this->m_user->cleanMessage($content);
                $result = $this->m_message->sendMessage($sender, $receiver, $subject, $content);
                echo $result;
            }
        }
    }

    private function getDataForView($player)
    {
        $data = array();

        $data['numOfReports'] = $this->m_user->checkForNewReports($_SESSION['username']);
        $data['numOfMessages'] = $this->m_user->checkForNewMessages($_SESSION['username']);
        $data['currentResources'] = $this->m_user->getCurrentResources($_SESSION['username']);
        $data['productionIncrement'] = $this->m_user->getCurrentProductionIncrement($_SESSION['username']);
        $data['username'] = $player->getUsername();
        $data['nation'] = $player->getNation();
        $data['edompoints'] = $player->getEdompoints();
        $data['numOfSoldiers'] = $this->m_user->getNumberOfSoldiers($_SESSION['username']);

        if (strcmp($player->getNation(), 'Men') == 0)
            $data['picture'] = 'assets/images/bgr/bard.jpg';

        elseif (strcmp ($player->getNation(), 'Elves') == 0)
            $data['picture'] = 'assets/images/bgr/thranduil.jpg';


        elseif (strcmp($player->getNation(), "Dwarves") == 0)
            $data['picture'] = 'assets/images/bgr/thorin.jpg';

        else
            $data['picture'] = 'assets/images/bgr/azog.png';


        $army = array();
        $attackerNation = $this->m_user->getNation($_SESSION['username']);

        if (strcmp($attackerNation, 'Men') == 0)
        {
            $army['sword'] = 'Swordsman';
            $army['spear'] = 'Spearman';
            $army['cavalry'] = 'Horseman';
            $army['bow'] = 'Archer';
        }

        elseif (strcmp($attackerNation, 'Elves') == 0)
        {
            $army['sword'] = 'Elven Swordsman';
            $army['spear'] = 'Elven Phalanx';
            $army['cavalry'] = 'Elven Rider';
            $army['bow'] = 'Elven Archer';
        }

        elseif (strcmp($attackerNation, "Dwarves") == 0)
        {
            $army['sword'] = 'Dwarven Infantry';
            $army['spear'] = 'Dwarven Phalanx';
            $army['cavalry'] = 'Chariot';
            $army['bow'] = 'Axe Thrower';
        }

        else
        {
            $army['sword'] = 'Orc Swordsman';
            $army['spear'] = 'Orc Spearman';
            $army['cavalry'] = 'Troll';
            $army['bow'] = 'Orc Archer';
        }

        $data['army'] = $army;

        return $data;
    }
}