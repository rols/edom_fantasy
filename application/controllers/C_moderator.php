<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_moderator extends CI_Controller
{
    public function index()
    {
        session_start();
        if (isset($_SESSION['username']) == false or $this->m_user->checkForRole($_SESSION['username'], 'moderator') == false)
        {
            redirect(base_url());
        }

        else
        {
            $data = $this->getDataForView();
            $this->load->view('v_moderator', $data);
        }
    }

    private function getDataForView()
    {
        $data = array();

        $data['numOfMessages'] = $this->m_user->checkForNewMessages($_SESSION['username']);
        $data['players'] = $this->m_user->getAllPlayers();

        return $data;
    }
}