<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_admin extends CI_Controller
{
    public function index()
    {
        session_start();
        if (isset($_SESSION['username']) == false or $this->m_user->checkForRole($_SESSION['username'], 'admin') == false)
        {
            redirect(base_url());
        }

        else
        {
            $data = $this->getDataForView();
            $this->load->view('v_admin', $data);
        }
    }

    public function create_new_mod()
    {
        if ($this->input->post('isAjaxCalling') == true)
        {
            $username = 'moderator';
            $number = 1;

            while (true)
            {
                if ($this->m_user->register($username . $number, $username . $number . "@edomfantasy.xyz", $this->generatePassword(), "mod") == true)
                    break;
                else
                    $number++;
            }
            echo true;
        }
    }

    public function delete_user()
    {
        if ($this->input->post('isAjaxCalling') == true)
        {
            session_start();
            $this->load->library('form_validation');
            $this->form_validation->set_rules('username', 'username', 'required');
            $user = $this->m_user->getPlayer($this->input->post('username'));
            if ($user == false)
                $user = $this->m_user->getModerator($this->input->post('username'));

            if ($this->form_validation->run() == false || $user == false || $this->m_user->checkForRole($_SESSION['username'], 'admin') == false)
                echo false;
            else
            {
                $this->load->model('m_reinforcement');
                $this->load->model('m_army');
                foreach($user->getReinforcementsReceived() as $reinforcement)
                {
                    $soldiers = $this->m_reinforcement->removeReinforcement($reinforcement->getIdrein());

                    $buildingType = '';
                    $soldierType = 1;
                    if ($soldiers['type'] == 'Sword')
                        $buildingType = 'Barracks';
                    else if ($soldiers['type'] == 'Spear')
                    {
                        $buildingType = 'Barracks';
                        $soldierType = 2;
                    }
                    else if($soldiers['type'] == 'Cavalry')
                        $buildingType = 'Stable';
                    else
                        $buildingType = 'Archery';

                    $this->m_army->updateCount($soldiers['sender'], $buildingType, $soldierType, $soldiers['count']);
                }

                if ($this->m_user->deleteUser($user) == true)
                    echo true;
                else
                    echo false;
            }
        }
    }

    public function block_user()
    {
        if ($this->input->post('isAjaxCalling') == true)
        {
            session_start();
            $this->load->library('form_validation');
            $this->form_validation->set_rules('username', 'username', 'required');
            $player = $this->m_user->getPlayer($this->input->post('username'));

            if ($this->form_validation->run() == false || $player == false || $this->m_user->checkForRole($_SESSION['username'], 'admin') == false)
                echo false;
            else
            {
                if ($this->m_user->blockPlayer($player) == true)
                    echo true;
                else
                    echo false;
            }
        }
    }

    public function send_message_to_mod()
    {
        if ($this->input->post('isAjaxCalling') == true)
        {
            session_start();
            $this->load->library('form_validation');
            $this->form_validation->set_rules('subject', 'subject', 'required|max_length[30]');
            $this->form_validation->set_rules('content', 'content', 'required|max_length[200]');
            $this->form_validation->set_rules('receiver', 'receiver', 'required');
            $sender = $this->m_user->getAdmin($_SESSION['username']);
            $receiver = $this->m_user->getModerator($this->input->post('receiver'));

            if ($this->form_validation->run() == false || $receiver == false || $this->m_user->checkForRole($_SESSION['username'], 'admin') == false)
                echo false;

            else
            {
                $this->load->model('m_message');
                $subject = $this->input->post('subject');
                $content = $this->input->post('content');
                $subject = $this->m_user->cleanMessage($subject);
                $content = $this->m_user->cleanMessage($content);
                $result = $this->m_message->sendMessage($sender, $receiver, $subject, $content);
                echo $result;
            }
        }
    }

    private function getDataForView()
    {
        $data = array();

        $data['numOfMessages'] = $this->m_user->checkForNewMessages($_SESSION['username']);
        $data['players'] = $this->m_user->getAllPlayersForAdmin();
        $data['moderators'] = $this->m_user->getAllModerators();

        return $data;
    }

    private function generatePassword()
    {
        $password = "";
        $charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        for($i = 0; $i < 8; $i++)
        {
            $random_int = mt_rand();
            $password .= $charset[$random_int % strlen($charset)];
        }

        return $password;
    }
}