<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_farm extends CI_Controller
{
    public function index()
    {
        session_start();
        if (isset($_SESSION['username']) == false or $this->m_user->checkForRole($_SESSION['username'], 'player') == false)
        {
            redirect(base_url());
        }

        $type = $this->input->get('type');
        $farm = $this->m_user->getFarm($_SESSION['username'], $type);

        if($farm == false)
        {
            redirect('home');
        }

        else
        {
            $data = $this->getDataForView($farm);
            $data['idfarm'] = $type;
            $this->load->view('v_farm', $data);
        }
    }

    public function upgrade_farm()
    {
        $data = array();
        $data['outcome'] = "false";

        if($this->input->post('isAjaxCalling') == true)
        {
            session_start();

            $farm = $this->m_user->getFarm($_SESSION['username'], $this->input->post('farm'));

            if($farm != false && $farm->getLevel() < 5)
            {
                $currentResources = $this->m_user->getCurrentResources($_SESSION['username']);

                $nation = $this->m_user->getPlayer($_SESSION['username'])->getNation();

                if ($nation == 'Men')
                {
                    $priceScale = 1.3;
                }
                else if ($nation == 'Elves')
                {
                    $priceScale = 1.35;
                }
                else if ($nation == 'Dwarves')
                {
                    $priceScale = 1.4;
                }
                else
                {
                    $priceScale = 1.4;
                }

                if($farm->getType() == 'Gold')
                {
                    $goldNeeded = (5000 + 500) * pow($priceScale, $farm->getLevel());
                    $foodNeeded = 5000 * pow($priceScale, $farm->getLevel());
                    $woodNeeded = 5000 * pow($priceScale, $farm->getLevel());
                }
                else if($farm->getType() == 'Food')
                {
                    $goldNeeded = 5000 * pow($priceScale, $farm->getLevel());
                    $foodNeeded = (5000 + 500) * pow($priceScale, $farm->getLevel());
                    $woodNeeded = 5000 * pow($priceScale, $farm->getLevel());
                }
                else
                {
                    $goldNeeded = 5000 * pow($priceScale, $farm->getLevel());
                    $foodNeeded = 5000 * pow($priceScale, $farm->getLevel());
                    $woodNeeded = (5000 + 500) * pow($priceScale, $farm->getLevel());
                }

                $goldNeeded = intval($goldNeeded);
                $foodNeeded = intval($foodNeeded);
                $woodNeeded = intval($woodNeeded);

                if ($currentResources['gold'] > $goldNeeded && $currentResources['food'] > $foodNeeded && $currentResources['wood'] > $woodNeeded)
                {
                    $this->load->model('m_farm');

                    if ($this->m_user->takeResources($_SESSION['username'], $goldNeeded, $foodNeeded, $woodNeeded) == true && $this->m_farm->upgradeFarm($farm) == true
                        && $this->m_user->updateEdompoints($_SESSION['username'], 100) == true)
                        $data['outcome'] = "true";
                    else
                        $data['outcome'] = "false";
                }
            }
        }
        echo json_encode($data);
    }

    private function getDataForView($farm)
    {
        $data = array();

        $data['numOfReports'] = $this->m_user->checkForNewReports($_SESSION['username']);
        $data['numOfMessages'] = $this->m_user->checkForNewMessages($_SESSION['username']);
        $data['currentResources'] = $this->m_user->getCurrentResources($_SESSION['username']);
        $data['productionIncrement'] = $this->m_user->getCurrentProductionIncrement($_SESSION['username']);
        $data['farm'] = $farm->getType();
        $data['level'] = $farm->getLevel();

        $nation = $this->m_user->getPlayer($_SESSION['username'])->getNation();

        $priceScale = 1;

        if ($nation == 'Men')
        {
            if ($data['farm'] == 'Gold') $data['production'] = 1500 * pow(1.5, $data['level']);
            else if ($data['farm'] == 'Food') $data['production'] = 1800 * pow(1.5, $data['level']);
            else $data['production'] = 1500 * pow(1.5, $data['level']);
            $priceScale = 1.3;
        }
        else if ($nation == 'Elves')
        {
            if ($data['farm'] == 'Gold') $data['production'] = 1500 * pow(1.5, $data['level']);
            else if ($data['farm'] == 'Food') $data['production'] = 1500 * pow(1.5, $data['level']);
            else $data['production'] = 1800 * pow(1.5, $data['level']);
            $priceScale = 1.35;
        }
        else if ($nation == 'Dwarves')
        {
            if ($data['farm'] == 'Gold') $data['production'] = 2100 * pow(1.5, $data['level']);
            else if ($data['farm'] == 'Food') $data['production'] = 1500 * pow(1.5, $data['level']);
            else $data['production'] = 1500 * pow(1.5, $data['level']);
            $priceScale = 1.4;
        }
        else
        {
            if ($data['farm'] == 'Gold') $data['production'] = 1400 * pow(1.5, $data['level']);
            else if ($data['farm'] == 'Food') $data['production'] = 1400 * pow(1.5, $data['level']);
            else $data['production'] = 1400 * pow(1.5, $data['level']);
            $priceScale = 1.4;
        }

        $data['production'] = intval($data['production']);

        if($data['farm'] == 'Gold')
        {
            $data['goldNeeded'] = (5000 + 500) * pow($priceScale, $data['level']);
            $data['foodNeeded'] = 5000 * pow($priceScale, $data['level']);
            $data['woodNeeded'] = 5000 * pow($priceScale, $data['level']);
            $data['picture'] = 'assets/images/struct/gold.jpg';
        }
        else if($data['farm'] == 'Food')
        {
            $data['goldNeeded'] = 5000 * pow($priceScale, $data['level']);
            $data['foodNeeded'] = (5000 + 500) * pow($priceScale, $data['level']);
            $data['woodNeeded'] = 5000 * pow($priceScale, $data['level']);
            $data['picture'] = 'assets/images/struct/grain.jpg';
        }
        else
        {
            $data['goldNeeded'] = 5000 * pow($priceScale, $data['level']);
            $data['foodNeeded'] = 5000 * pow($priceScale, $data['level']);
            $data['woodNeeded'] = (5000 + 500) * pow($priceScale, $data['level']);
            $data['picture'] = 'assets/images/struct/wood.jpg';
        }
        $data['goldNeeded'] = intval($data['goldNeeded']);
        $data['foodNeeded'] = intval($data['foodNeeded']);
        $data['woodNeeded'] = intval($data['woodNeeded']);

        return $data;
    }
}