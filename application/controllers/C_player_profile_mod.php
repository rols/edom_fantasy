<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_player_profile_mod extends CI_Controller
{
    public function index()
    {
        session_start();
        if (isset($_SESSION['username']) == false or $this->m_user->checkForRole($_SESSION['username'], 'moderator') == false)
        {
            redirect(base_url());
        }

        $username = $this->input->get('username');
        $player = $this->m_user->getPlayer($username);

        if($username == false || $username == $_SESSION['username'] || $player == false)
        {
            redirect('moderator');
        }

        else
        {
            $data = $this->getDataForView($player);
            $this->load->view('v_player-profile-mod', $data);
        }
    }

    public function read_message()
    {
        if ($this->input->post('isAjaxCalling') == true)
        {
            session_start();
            $this->load->model('m_message');
            $idmes = $this->input->post('idmes');
            if ($this->m_user->checkForRole($_SESSION['username'], 'moderator') == true || $this->m_user->checkForRole($_SESSION['username'], 'admin') == true)
                echo $this->m_message->readMessage($idmes, 'false');
            else
                echo false;
        }
    }

    public function read_report()
    {
        if ($this->input->post('isAjaxCalling') == true)
        {
            session_start();
            $this->load->model('m_report');
            $idrep = $this->input->post('idrep');
            if ($this->m_user->checkForRole($_SESSION['username'], 'moderator') == true || $this->m_user->checkForRole($_SESSION['username'], 'admin') == true)
                echo $this->m_report->readReport($idrep, 'false');
            else
                echo false;
        }
    }

    public function send_message()
    {
        if ($this->input->post('isAjaxCalling') == true)
        {
            session_start();
            $this->load->library('form_validation');
            $this->form_validation->set_rules('content', 'content', 'required|max_length[200]');
            $this->form_validation->set_rules('receiver', 'receiver', 'required');
            $sender = $this->m_user->getModerator($_SESSION['username']);
            $receiver = $this->m_user->getPlayer($this->input->post('receiver'));

            if ($this->form_validation->run() == false || $receiver == false || $this->m_user->checkForRole($_SESSION['username'], 'moderator') == false)
                echo false;

            else
            {
                $this->load->model('m_message');
                $subject = $this->input->post('subject');
                $content = $this->input->post('content');
                $result = $this->m_message->sendMessage($sender, $receiver, $subject, $content);
                echo $result;
            }
        }
    }

    private function getDataForView($player)
    {
        $data = array();

        if (strcmp($player->getNation(), 'Men') == 0)
            $data['picture'] = 'assets/images/bgr/bard.jpg';

        elseif (strcmp ($player->getNation(), 'Elves') == 0)
            $data['picture'] = 'assets/images/bgr/thranduil.jpg';


        elseif (strcmp($player->getNation(), "Dwarves") == 0)
            $data['picture'] = 'assets/images/bgr/thorin.jpg';

        else
            $data['picture'] = 'assets/images/bgr/azog.png';

        $data['numOfMessages'] = $this->m_user->checkForNewMessages($_SESSION['username']);
        $data['username'] = $player->getUsername();
        $data['nation'] = $player->getNation();
        $data['edompoints'] = $player->getEdompoints();
        $data['currentResources'] = $this->m_user->getCurrentResourcesUser($player);
        $data['currentProduction'] = $this->m_user->getCurrentProductionUser($player);
        $data['armies'] = $this->m_user->getAllArmiesUser($player);
        $data['sentReinforcements'] = $this->m_user->getSentReinforcementsUser($player);
        $data['receivedReinforcements'] = $this->m_user->getReceivedReinforcementsUser($player);
        $data['reports'] = $this->m_user->getAllReportsUser($player);
        $data['messages'] = $this->m_user->getAllMessagesUser($player);

        $farms = array();
        $farms['gold1'] = $player->getGoldfarm1()->getLevel();
        $farms['gold2'] = $player->getGoldfarm2()->getLevel();
        $farms['food1'] = $player->getFoodfarm1()->getLevel();
        $farms['food2'] = $player->getFoodfarm2()->getLevel();
        $farms['wood1'] = $player->getWoodfarm1()->getLevel();
        $farms['wood2'] = $player->getWoodfarm2()->getLevel();
        $data['farms'] = $farms;

        $buildings = array();
        $nation = $player->getNation();
        if (strcmp($nation, 'Men') == 0)
        {
            $buildings['barracks'] = 'Military Barracks';
            $buildings['stable'] = 'Stable';
            $buildings['archery'] = 'Archery Range';
        }

        elseif (strcmp($nation, 'Elves') == 0)
        {
            $buildings['barracks'] = 'Elven Barracks';
            $buildings['stable'] = 'Elven Stable';
            $buildings['archery'] = 'Elven Archery';
        }


        elseif (strcmp($nation, "Dwarves") == 0)
        {
            $buildings['barracks'] = 'Dwarven Barracks';
            $buildings['stable'] = 'Dwarven Stable';
            $buildings['archery'] = 'Dwarven Axle';
        }

        else
        {
            $buildings['barracks'] = 'Orc Pit';
            $buildings['stable'] = 'Troll Cave';
            $buildings['archery'] = 'Orc Archery';
        }
        $buildings['barracksLevel'] = $player->getBarracks()->getLevel();
        $buildings['stableLevel'] = $player->getStable()->getLevel();
        $buildings['archeryLevel'] = $player->getArchery()->getLevel();
        $data['buildings'] = $buildings;


        return $data;
    }
}