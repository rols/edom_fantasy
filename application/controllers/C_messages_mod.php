<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_messages_mod extends CI_Controller
{
    public function index()
    {
        session_start();
        if (isset($_SESSION['username']) == false or $this->m_user->checkForRole($_SESSION['username'], 'moderator') == false)
        {
            redirect(base_url());
        }

        else
        {
            $data = $this->getDataForView();
            $this->load->view('v_messages-mod', $data);
        }
    }

    public function send_report_to_admin()
    {
        if ($this->input->post('isAjaxCalling') == true)
        {
            session_start();
            $this->load->library('form_validation');
            $this->form_validation->set_rules('subject', 'subject', 'required|max_length[30]');
            $this->form_validation->set_rules('content', 'content', 'required|max_length[200]');
            $moderator = $this->m_user->getModerator($_SESSION['username']);
            $admin = $this->m_user->getAdmin('admin');

            if ($this->form_validation->run() == false || $moderator == false || $admin == false)
                echo false;

            else
            {
                $this->load->model('m_message');
                $subject = $this->input->post('subject');
                $content = $this->input->post('content');
                $subject = $this->m_user->cleanMessage($subject);
                $content = $this->m_user->cleanMessage($content);
                $result = $this->m_message->sendMessage($moderator, $admin, $subject, $content);
                echo $result;
            }
        }
    }

    private function getDataForView()
    {
        $data = array();

        $data['numOfMessages'] = $this->m_user->checkForNewMessages($_SESSION['username']);
        $data['messages'] = $this->m_user->getAllMessages($_SESSION['username']);

        return $data;
    }
}