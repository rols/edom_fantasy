<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_messages_admin extends CI_Controller
{
    public function index()
    {
        session_start();
        if (isset($_SESSION['username']) == false or $this->m_user->checkForRole($_SESSION['username'], 'admin') == false)
        {
            redirect(base_url());
        }

        else
        {
            $data = $this->getDataForView();
            $this->load->view('v_messages-admin', $data);
        }
    }

    private function getDataForView()
    {
        $data = array();

        $data['numOfMessages'] = $this->m_user->checkForNewMessages($_SESSION['username']);
        $data['messages'] = $this->m_user->getAllMessages($_SESSION['username']);

        return $data;
    }
}