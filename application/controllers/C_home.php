<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_home extends CI_Controller
{
    public function index()
    {
        session_start();
        if (isset($_SESSION['username']) == false or $this->m_user->checkForRole($_SESSION['username'], 'player') == false)
        {
            redirect(base_url());
        }

        else
        {
            $data = $this->getDataForView();
            $this->load->view('v_home', $data);
        }
    }

    private function getDataForView()
    {
        $data = array();

        $nation = $this->m_user->getNation($_SESSION['username']);
        if (strcmp($nation, 'Men') == 0)
        {
            $data['picture'] = 'assets/images/bgr/bard.jpg';
            $data['armyPicture'] = 'assets/images/army/armyman.jpg';
            $data['barracks'] = 'Military Barracks';
            $data['stable'] = 'Stable';
            $data['archery'] = 'Archery Range';
        }
        elseif (strcmp ($nation, 'Elves') == 0)
        {
            $data['picture'] = 'assets/images/bgr/thranduil.jpg';
            $data['armyPicture'] = 'assets/images/army/armyelf.jpg';
            $data['barracks'] = 'Elven Barracks';
            $data['stable'] = 'Elven Stable';
            $data['archery'] = 'Elven Archery';
        }
        elseif (strcmp($nation, "Dwarves") == 0)
        {
            $data['picture'] = 'assets/images/bgr/thorin.jpg';
            $data['armyPicture'] = 'assets/images/army/armydwarf.jpg';
            $data['barracks'] = 'Dwarven Barracks';
            $data['stable'] = 'Dwarven Stable';
            $data['archery'] = 'Dwarven Axle';
        }
        else
        {
            $data['picture'] = 'assets/images/bgr/azog.png';
            $data['armyPicture'] = 'assets/images/army/armyorc.png';
            $data['barracks'] = 'Orc Pit';
            $data['stable'] = 'Troll Cave';
            $data['archery'] = 'Orc Archery';
        }


        $data['numOfReports'] = $this->m_user->checkForNewReports($_SESSION['username']);
        $data['numOfMessages'] = $this->m_user->checkForNewMessages($_SESSION['username']);
        $data['currentResources'] = $this->m_user->getCurrentResources($_SESSION['username']);
        $data['currentProduction'] = $this->m_user->getCurrentProduction($_SESSION['username']);
        $data['productionIncrement'] = $this->m_user->getCurrentProductionIncrement($_SESSION['username']);

        return $data;
    }
}