<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_reports extends CI_Controller
{
    public function index()
    {
        session_start();
        if (isset($_SESSION['username']) == false or $this->m_user->checkForRole($_SESSION['username'], 'player') == false)
        {
            redirect(base_url());
        }

        else
        {
            $data = $this->getDataForView();
            $this->load->view('v_reports', $data);
        }
    }

    public function delete_report()
    {
        if ($this->input->post('isAjaxCalling') == true)
        {
            session_start();
            $this->load->model('m_report');
            $idrep = $this->input->post('idrep');
            if ($this->m_report->checkForAccess($idrep, $_SESSION['username']))
                $this->m_report->deleteReport($idrep);
        }
    }

    public function read_report()
    {
        if ($this->input->post('isAjaxCalling') == true)
        {
            session_start();
            $this->load->model('m_report');
            $idrep = $this->input->post('idrep');
            $updateNeeded = $this->input->post('updateNeeded');
            if ($this->m_report->checkForAccess($idrep, $_SESSION['username']))
                echo $this->m_report->readReport($idrep, $updateNeeded);
            else
                echo false;
        }
    }

    private function getDataForView()
    {
        $data = array();

        $data['numOfReports'] = $this->m_user->checkForNewReports($_SESSION['username']);
        $data['numOfMessages'] = $this->m_user->checkForNewMessages($_SESSION['username']);
        $data['currentResources'] = $this->m_user->getCurrentResources($_SESSION['username']);
        $data['productionIncrement'] = $this->m_user->getCurrentProductionIncrement($_SESSION['username']);
        $data['reports'] = $this->m_user->getAllReports($_SESSION['username']);

        return $data;
    }
}