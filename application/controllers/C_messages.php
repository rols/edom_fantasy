<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_messages extends CI_Controller
{
    public function index()
    {
        session_start();
        if (isset($_SESSION['username']) == false or $this->m_user->checkForRole($_SESSION['username'], 'player') == false)
        {
            redirect(base_url());
        }

        else
        {
            $data = $this->getDataForView();
            $this->load->view('v_messages', $data);
        }
    }

    public function send_report_to_mod()
    {
        if ($this->input->post('isAjaxCalling') == true)
        {
            session_start();
            $this->load->library('form_validation');
            $this->form_validation->set_rules('subject', 'subject', 'required|max_length[30]');
            $this->form_validation->set_rules('content', 'content', 'required|max_length[200]');
            $player = $this->m_user->getPlayer($_SESSION['username']);
            $moderator = $this->m_user->getRandomModerator();

            if ($this->form_validation->run() == false || $player == false || $moderator == false)
                echo false;

            else
            {
                $this->load->model('m_message');
                $subject = $this->input->post('subject');
                $content = $this->input->post('content');
                $subject = $this->m_user->cleanMessage($subject);
                $content = $this->m_user->cleanMessage($content);
                $result = $this->m_message->sendMessage($player, $moderator, $subject, $content);
                echo $result;
            }
        }
    }

    public function delete_message()
    {
        if ($this->input->post('isAjaxCalling') == true)
        {
            session_start();
            $this->load->model('m_message');
            $idmes = $this->input->post('idmes');
            if ($this->m_message->checkForAccess($idmes, $_SESSION['username']))
                $this->m_message->deleteMessage($idmes);
        }
    }

    public function read_message()
    {
        if ($this->input->post('isAjaxCalling') == true)
        {
            session_start();
            $this->load->model('m_message');
            $idmes = $this->input->post('idmes');
            $updateNeeded = $this->input->post('updateNeeded');
            if ($this->m_message->checkForAccess($idmes, $_SESSION['username']))
                echo $this->m_message->readMessage($idmes, $updateNeeded);
            else
                echo false;
        }
    }

    private function getDataForView()
    {
        $data = array();

        $data['numOfReports'] = $this->m_user->checkForNewReports($_SESSION['username']);
        $data['numOfMessages'] = $this->m_user->checkForNewMessages($_SESSION['username']);
        $data['currentResources'] = $this->m_user->getCurrentResources($_SESSION['username']);
        $data['productionIncrement'] = $this->m_user->getCurrentProductionIncrement($_SESSION['username']);
        $data['messages'] = $this->m_user->getAllMessages($_SESSION['username']);

        return $data;
    }
}