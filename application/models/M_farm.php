<?php

class M_farm extends CI_Model
{
    public function upgradeFarm($farm)
    {
        $em = $this->doctrine->em;

        $farm->setLevel($farm->getLevel() + 1);

        try
        {
            $em->flush();
            return true;
        }
        catch(Exception $e)
        {
            return false;
        }
    }

    public function updateFarmLevel($farm, $newLevel)
    {
        $em = $this->doctrine->em;

        $farm->setLevel($newLevel);

        try
        {
            $em->flush();
            return true;
        }
        catch(Exception $e)
        {
            return false;
        }
    }
}
