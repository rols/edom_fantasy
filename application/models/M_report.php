<?php



class M_report extends CI_Model
{
    public function checkForAccess($idrep, $username)
    {
        $em = $this->doctrine->em;

        $query = "SELECT r FROM Entity\Report r WHERE r.idrep = :idrep";
        $report = $em->createQuery($query)->setParameter('idrep', $idrep)->getResult()[0];

        $query = "SELECT u FROM Entity\User u WHERE u.username = :username";
        $user = $em->createQuery($query)->setParameter('username', $username)->getResult()[0];

        if ($report->getIduser()->getIdUser() == $user->getIduser())
            return true;
        else
            return false;
    }

    public function deleteReport($idrep)
    {
        $em = $this->doctrine->em;

        $query = "SELECT r FROM Entity\Report r WHERE r.idrep = :idrep";
        $report = $em->createQuery($query)->setParameter('idrep', $idrep)->getResult()[0];

        $em->remove($report);
        try
        {
            $em->flush();
        }
        catch (Exception $e) {}
    }

    public function readReport($idrep, $updateNeeded)
    {
        $em = $this->doctrine->em;

        $query = "SELECT r FROM Entity\Report r WHERE r.idrep = :idrep";
        $report = $em->createQuery($query)->setParameter('idrep', $idrep)->getResult()[0];


        $data = array();
        $data['type'] = $report->getType();
        $data['content'] = $report->getContent();
        $data['time'] = $report->getTime();

        if ($updateNeeded == 'true')
        {
            $report->setIsread(1);
            try
            {
                $em->flush();
            }
            catch (Exception $e)
            {
                return false;
            }
        }

        return json_encode($data);
    }

    public function sendReport($userReceiver, $type, $content)
    {
        $em = $this->doctrine->em;

        $report = new Entity\Report();
        $report->setIduser($userReceiver);
        $report->setType($type);
        $report->setContent($content);
        $report->setTime(new DateTime("now"));
        $report->setIsread(0);

        $em->persist($report);
        try
        {
            $em->flush();
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    public function createBattleReport($attacker, $sword, $spear, $cavalry, $bow, $defender, $offLostPerc, $defSurvivedPerc, $isWinner, $goldTaken, $foodTaken, $woodTaken)
    {
        $attackerSwordLooses = intval(round($offLostPerc * $sword / 100.0));
        $attackerSpearLooses = intval(round($offLostPerc * $spear / 100.0));
        $attackerCavalryLooses = intval(round($offLostPerc * $cavalry / 100.0));
        $attackerBowLooses = intval(round($offLostPerc * $bow / 100.0));

        $defenderSwordLooses = $defender->getSword()->getCount() - intval(round($defender->getSword()->getCount() * $defSurvivedPerc / 100.0));
        $defenderSpearLooses = $defender->getSpear()->getCount() - intval(round($defender->getSpear()->getCount() * $defSurvivedPerc / 100.0));
        $defenderCavalryLooses = $defender->getCavalry()->getCount() - intval(round($defender->getCavalry()->getCount() * $defSurvivedPerc / 100.0));
        $defenderBowLooses = $defender->getBow()->getCount() - intval(round($defender->getBow()->getCount() * $defSurvivedPerc / 100.0));

        $report ="<table class=\"table table-bordered\" align=\"center\"><tbody style=\"text-align: center\"><tr style=\"font-weight: bold\"><td><a href=\"player-profile?username=";
        $report .= $attacker->getUsername() . "\">".$attacker->getUsername();
        $report .= "</a></td><td>Sword</td><td>Spear</td><td>Cavalry</td><td>Bow</td></tr>";
        $report .= "<tr><td><b>Soldiers</b></td><td>" . $sword . "</td><td>" . $spear . "</td><td>" . $cavalry . "</td><td>" . $bow . "</td></tr>";
        $report .= "<tr><td><b>Losses</b></td><td>" . $attackerSwordLooses . "</td><td>" . $attackerSpearLooses . "</td><td>" . $attackerCavalryLooses . "</td><td>" . $attackerBowLooses . "</td></tr>";
        $report .= "<tr class=\"info\"><td colspan=\"5\"></td></tr>";

        $report .= "<tr style=\"font-weight: bold\"><td><a href=\"player-profile?username=";
        $report .= $defender->getUsername()."\">" . $defender->getUsername();
        $report .= "</a></td><td>Sword</td><td>Spear</td><td>Cavalry</td><td>Bow</td></tr>";

        if ($attackerSwordLooses == $sword && $attackerSpearLooses == $spear && $attackerCavalryLooses == $cavalry && $attackerBowLooses == $bow)
        {
            $report .= "<tr class=\"info\"><td colspan=\"5\"></td></tr>";
            $report .= "<tr style=\"font-weight: bold\"><td>Winner: " . $defender->getUsername() . "</td><td colspan=\"4\">None of soldiers have returned.</td></tr>";
        }

        else
        {
            $report .= "<tr><td><b>Soldiers</b></td><td>" . $defender->getSword()->getCount() . "</td><td>" . $defender->getSpear()->getCount() . "</td><td>" . $defender->getCavalry()->getCount() . "</td><td>" . $defender->getBow()->getCount() . "</td></tr>";
            $report .= "<tr><td><b>Losses</b></td><td>" . $defenderSwordLooses . "</td><td>" . $defenderSpearLooses . "</td><td>" . $defenderCavalryLooses . "</td><td>" . $defenderBowLooses . "</td></tr>";

            $reinforcements = $defender->getReinforcementsReceived();

            if ($reinforcements == null)
                $report .= "<tr class=\"info\"><td colspan=\"5\"></td></tr>";

            else
            {
                $firstRein = true;
                $msg = "<b>Reinforcements</b>";
                $class = "info";
                foreach ($reinforcements as $rein)
                {
                    $report .= "<tr class =\"" . $class . "\"><td colspan=\"5\">" .$msg. "</td></tr>";

                    if ($firstRein)
                    {
                        $firstRein = false;
                        $msg = "";
                        $class = "";
                    }

                    $defenderType = $rein->getType();
                    $defenderLooses = $rein->getCount() - intval(round($rein->getCount() * $defSurvivedPerc / 100.0));

                    $report .= "<tr style=\"font-weight: bold\"><td><a href=\"player-profile?username=";
                    $report .= $rein->getUserSender()->getUsername()."\">" . $rein->getUserSender()->getUsername();
                    $report .= "</a></td><td>Sword</td><td>Spear</td><td>Cavalry</td><td>Bow</td></tr>";

                    if ($defenderType == "Sword")
                    {
                        $report .= "<tr><td><b>Soldiers</b></td><td>" . $rein->getCount() . "</td><td>0</td><td>0</td><td>0</td></tr>";
                        $report .= "<tr><td><b>Losses</b></td><td>" . $defenderLooses . "</td><td>0</td><td>0</td><td>0</td></tr>";
                    }

                    else if ($defenderType == "Spear")
                    {
                        $report .= "<tr><td><b>Soldiers</b></td><td>0</td><td>" . $rein->getCount() . "</td><td>0</td><td>0</td></tr>";
                        $report .= "<tr><td><b>Losses</b></td><td>0</td><td>" . $defenderLooses . "</td><td>0</td><td>0</td></tr>";
                    }

                    else if ($defenderType == "Cavalry")
                    {
                        $report .= "<tr><td><b>Soldiers</b></td><td>0</td><td>0</td><td>" . $rein->getCount() . "</td><td>0</td></tr>";
                        $report .= "<tr><td><b>Losses</b></td><td>0</td><td>0</td><td>" . $defenderLooses . "</td><td>0</td></tr>";
                    }

                    else
                    {
                        $report .= "<tr><td><b>Soldiers</b></td><td>0</td><td>0</td><td>0</td><td>" . $rein->getCount() . "</td></tr>";
                        $report .= "<tr><td><b>Losses</b></td><td>0</td><td>0</td><td>0</td><td>" . $defenderLooses . "</td></tr>";
                    }
                }
                $report .= "<tr class=\"info\"><td colspan=\"5\"></td></tr>";
            }

            if ($isWinner)
                $report .= "<tr style=\"font-weight: bold\"><td>Winner: " . $attacker->getUsername() . "</td><td colspan=\"4\">Plunder: Gold: " . $goldTaken . " Food: " . $foodTaken . " Wood: " . $woodTaken ."</td></tr>";

            else
                $report .= "<tr style=\"font-weight: bold\"><td>Winner: " . $defender->getUsername() . "</td><td colspan=\"4\"></td></tr>";
        }

        $report .= "</tbody></table>";

        return $report;
    }
}