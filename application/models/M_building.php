<?php

class M_building extends CI_Model
{
    public function upgradeBuilding($building)
    {
        $em = $this->doctrine->em;

        $building->setLevel($building->getLevel() + 1);

        try
        {
            $em->flush();
            return true;
        }
        catch(Exception $e)
        {
            return false;
        }
    }

    public function updateBuildingLevel($building, $newLevel)
    {
        $em = $this->doctrine->em;

        $building->setLevel($newLevel);

        try
        {
            $em->flush();
            return true;
        }
        catch(Exception $e)
        {
            return false;
        }
    }
}
