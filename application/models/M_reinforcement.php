<?php

class M_reinforcement extends CI_Model
{
    public function sendReinforcement($userSender, $userReceiver, $sword, $spear, $cavalry, $bow)
    {
        $em = $this->doctrine->em;

        if ($userSender->getSword()->getCount() >= $sword && $userSender->getSpear()->getCount() >= $spear &&
        $userSender->getCavalry()->getCount() >= $cavalry && $userSender->getBow()->getCount() >= $bow)
        {
            if ($sword > 0) {
                $reinforcement = new Entity\Reinforcement();
                $reinforcement->setType("Sword");
                $reinforcement->setUserSender($userSender);
                $reinforcement->setUserReceiver($userReceiver);
                $reinforcement->setCount($sword);
                $em->persist($reinforcement);
            }
            if ($spear > 0) {
                $reinforcement = new Entity\Reinforcement();
                $reinforcement->setType("Spear");
                $reinforcement->setUserSender($userSender);
                $reinforcement->setUserReceiver($userReceiver);
                $reinforcement->setCount($spear);
                $em->persist($reinforcement);
            }
            if ($cavalry > 0) {
                $reinforcement = new Entity\Reinforcement();
                $reinforcement->setType("Cavalry");
                $reinforcement->setUserSender($userSender);
                $reinforcement->setUserReceiver($userReceiver);
                $reinforcement->setCount($cavalry);
                $em->persist($reinforcement);
            }
            if ($bow > 0) {
                $reinforcement = new Entity\Reinforcement();
                $reinforcement->setType("Bow");
                $reinforcement->setUserSender($userSender);
                $reinforcement->setUserReceiver($userReceiver);
                $reinforcement->setCount($bow);
                $em->persist($reinforcement);
            }

            try
            {
                $em->flush();
                return true;
            }
            catch (Exception $e)
            {
                return false;
            }
        }
        else
            return false;
    }

    public function updateReinforcementCount($reinforcement, $count)
    {
        $em = $this->doctrine->em;

        if ($count == 0)
            $em->remove($reinforcement);
        else
            $reinforcement->setCount($count);

        try
        {
            $em->flush();
            return true;
        }
        catch(Exception $e)
        {
            return false;
        }
    }

    public function checkIfSender($idrein, $username)
    {
        $em = $this->doctrine->em;

        $query = "SELECT r FROM Entity\Reinforcement r WHERE r.idrein = :idrein";
        $reinforcement = $em->createQuery($query)->setParameter('idrein', $idrein)->getResult()[0];
        if($reinforcement == false) return false;

        $query = "SELECT u FROM Entity\User u WHERE u.username = :username";
        $user = $em->createQuery($query)->setParameter('username', $username)->getResult()[0];
        if($user == false) return false;

        if ($reinforcement->getUserSender()->getIdUser() == $user->getIduser())
            return true;
        else
            return false;
    }

    public function checkIfReceiver($idrein, $username)
    {
        $em = $this->doctrine->em;

        $query = "SELECT r FROM Entity\Reinforcement r WHERE r.idrein = :idrein";
        $reinforcement = $em->createQuery($query)->setParameter('idrein', $idrein)->getResult()[0];
        if($reinforcement == false) return false;

        $query = "SELECT u FROM Entity\User u WHERE u.username = :username";
        $user = $em->createQuery($query)->setParameter('username', $username)->getResult()[0];
        if($user == false) return false;

        if ($reinforcement->getUserReceiver()->getIdUser() == $user->getIduser())
            return true;
        else
            return false;
    }

    public function removeReinforcement($idrein)
    {
        $em = $this->doctrine->em;

        $query = "SELECT r FROM Entity\Reinforcement r WHERE r.idrein = :idrein";
        $reinforcement = $em->createQuery($query)->setParameter('idrein', $idrein)->getResult()[0];
        $data = array();
        $data['count'] = $reinforcement->getCount();
        $data['type'] = $reinforcement->getType();
        $data['sender'] = $reinforcement->getUserSender();

        $em->remove($reinforcement);
        try
        {
            $em->flush();
            return $data;
        }
        catch (Exception $e) {
            return false;
        }
    }

    public function getNumOfSoldiersInSentReinforcement($user, $type) #for available slots
    {
        $reinforcement = $user->getReinforcementsSent();
        $count = 0;

        foreach ($reinforcement as $r)
        {
            if ($r->getType() == $type)
                $count += $r->getCount();
        }

        return $count;
    }
}