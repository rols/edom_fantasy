<?php


class M_battlesimulator extends CI_Model
{
    public $offPoints;
    public $offLostPerc = 0; #out

    public $defender;
    public $defPoints = 0;
    public $defSurvivedPerc = 0; #out

    public function initialise($offPoints, $defender)
    {
        $this->offPoints = $offPoints;

        $reinforcement = $defender->getReinforcementsReceived();

        foreach($reinforcement as $r)
        {
            $soldier = M_soldier::$factory[M_soldier::getSoldierName($r->getUserSender()->getNation(), $r->getType())];
            $this->defPoints += $r->getCount() * $soldier->defence;
        }

        $soldier = M_soldier::$factory[M_soldier::getSoldierName($defender->getNation(), $defender->getSword()->getType())];
        $this->defPoints += $defender->getSword()->getCount() * $soldier->defence;
        $soldier = M_soldier::$factory[M_soldier::getSoldierName($defender->getNation(), $defender->getSpear()->getType())];
        $this->defPoints += $defender->getSpear()->getCount() * $soldier->defence;
        $soldier = M_soldier::$factory[M_soldier::getSoldierName($defender->getNation(), $defender->getCavalry()->getType())];
        $this->defPoints += $defender->getCavalry()->getCount() * $soldier->defence;
        $soldier = M_soldier::$factory[M_soldier::getSoldierName($defender->getNation(), $defender->getBow()->getType())];
        $this->defPoints += $defender->getBow()->getCount() * $soldier->defence;
    }

    public function simulate()
    {
        if ($this->offPoints > $this->defPoints)
        {
            $x = 100.0 * pow(($this->defPoints / $this->offPoints), 1.5);
            $this->offLostPerc = (100.0 * $x / (100.0 + $x)); //Lost
            $this->defSurvivedPerc = 100.0 - $this->offLostPerc; //Lost
            $this->defSurvivedPerc = 100.0 - $this->defSurvivedPerc; //Survived
            return 'attacker'; #winner
        }

        else
        {
            $x = 100.0 * pow(($this->offPoints / $this->defPoints), 1.5);
            $this->defSurvivedPerc = (100.0 * $x / (100.0 + $x)); //Lost
            $this->offLostPerc = 100.0 - $this->defSurvivedPerc; //Lost
            $this->defSurvivedPerc = 100.0 - $this->defSurvivedPerc; //Survived
            return 'defender'; #winner
        }
    }
};
