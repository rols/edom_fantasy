<?php

class M_user extends CI_Model
{
    private static $armyNames;

    public function cleanUsername($string)
    {
        $string = str_replace(' ', '_', $string);

        return preg_replace('/[^A-Za-z0-9\_]/', '', $string);
    }

    public function cleanMessage($string)
    {
        $string = str_replace('<', '&lt;', $string);
        $string = str_replace('>', '&gt;', $string);
        return $string;
    }

    //page = ('player', 'moderator', 'admin')
    public function checkForRole($username, $page)
    {
        $em = $this->doctrine->em;

        $query = "SELECT u FROM Entity\User u WHERE u.username = :username";
        $user = $em->createQuery($query)->setParameter('username', $username)->getResult();

        $role = $user[0]->getNation();

        if (strcmp('admin', $role) == 0)
        {
            if (strcmp($page, 'admin') == 0)
                return true;
            else
                return false;
        }

        elseif (strcmp('mod', $role) == 0)
        {
            if (strcmp($page, 'moderator') == 0)
                return true;
            else
                return false;
        }

        else
        {
            if (strcmp($page, 'player') == 0)
                return true;
            else
                return false;
        }
    }

    public function login($username, $password)
    {
        $em = $this->doctrine->em;

        $query = "SELECT u FROM Entity\User u WHERE u.username = :username AND u.password = :password";
        $user = $em->createQuery($query)->setParameter('username', $username)->setParameter('password', $password)->getResult();

        if ($user == null || $user[0]->getUsername() != $username || $user[0]->getPassword() != $password)
            return null;

        else if ($user[0]->getBlocked() == 1)
            return "blocked";

        else
            return $user[0]->getNation();
    }

    public function register($username, $email, $password, $nation)
    {
        $em = $this->doctrine->em;

        $username = $this->cleanUsername($username);
        if (strlen($username) == 0)
            return false;

        $query = "SELECT u FROM Entity\User u WHERE u.email = :email OR u.username = :username";
        $users = $em->createQuery($query)->setParameter('email', $email)->setParameter('username', $username)->getResult();


        if ($users != null)
            return false;

        else
        {
            $user = new Entity\User();
            $user->setUsername($username);
            $user->setEmail($email);
            $user->setPassword($password);
            $user->setNation($nation);
            $user->setEdompoints(0);
            if (strcmp($nation, "Dwarves") == 0)
            {
                $user->setGold(15000);
                $user->setFood(15000);
                $user->setWood(15000);
            }
            elseif (strcmp($nation, "Orcs") == 0) {
                $user->setGold(8000);
                $user->setFood(8000);
                $user->setWood(8000);
            }
            else #Men and Elves
            {
                $user->setGold(10000);
                $user->setFood(10000);
                $user->setWood(10000);
            }
            $user->setTimestamp(new DateTime("now"));
            $user->setBlocked(0);

            $farm1 = new Entity\Farm();
            $farm1->setLevel(0);
            $farm1->setType("Gold");
            $user->setGoldfarm1($farm1);

            $farm2 = new Entity\Farm();
            $farm2->setLevel(0);
            $farm2->setType("Gold");
            $user->setGoldfarm2($farm2);

            $farm3 = new Entity\Farm();
            $farm3->setLevel(0);
            $farm3->setType("Food");
            $user->setFoodfarm1($farm3);

            $farm4 = new Entity\Farm();
            $farm4->setLevel(0);
            $farm4->setType("Food");
            $user->setFoodfarm2($farm4);

            $farm5 = new Entity\Farm();
            $farm5->setLevel(0);
            $farm5->setType("Wood");
            $user->setWoodfarm1($farm5);

            $farm6 = new Entity\Farm();
            $farm6->setLevel(0);
            $farm6->setType("Wood");
            $user->setWoodfarm2($farm6);

            $barracks = new Entity\Building();
            $barracks->setLevel(0);
            $barracks->setType("Barracks");
            $user->setBarracks($barracks);

            $stable = new Entity\Building();
            $stable->setLevel(0);
            $stable->setType("Stable");
            $user->setStable($stable);

            $archery = new Entity\Building();
            $archery->setLevel(0);
            $archery->setType("Archery");
            $user->setArchery($archery);

            $sword = new Entity\Army();
            $sword->setCount(0);
            $sword->setType("Sword");
            $user->setSword($sword);

            $spear = new Entity\Army();
            $spear->setCount(0);
            $spear->setType("Spear");
            $user->setSpear($spear);

            $cavalry = new Entity\Army();
            $cavalry->setCount(0);
            $cavalry->setType("Cavalry");
            $user->setCavalry($cavalry);

            $bow = new Entity\Army();
            $bow->setCount(0);
            $bow->setType("Bow");
            $user->setBow($bow);


            $em->persist($user);
            try
            {
                $em->flush();
                return true;
            }
            catch (Exception $e)
            {
                return false;
            }
        }
    }

    public function getPlayer($username)
    {
        $em = $this->doctrine->em;

        $query = "SELECT u FROM Entity\User u WHERE u.username = :username";
        $user = $em->createQuery($query)->setParameter('username', $username)->getResult();

        if ($user != null && $user[0]->getUsername() == $username && (strcmp($user[0]->getNation(), 'admin') != 0 && strcmp($user[0]->getNation(), 'mod') != 0))
            return $user[0];
        else
            return false;
    }

    public function getPlayerByEmail($email)
    {
        $em = $this->doctrine->em;

        $query = "SELECT u FROM Entity\User u WHERE u.email = :email";
        $user = $em->createQuery($query)->setParameter('email', $email)->getResult();

        if ($user != null && $user[0]->getEmail() == $email && (strcmp($user[0]->getNation(), 'admin') != 0 && strcmp($user[0]->getNation(), 'mod') != 0))
            return $user[0];
        else
            return false;
    }

    public function blockPlayer($user)
    {
        $em = $this->doctrine->em;

        $block = $user->getBlocked();
        $block += 1;
        $block %= 2;
        $user->setBlocked($block);

        try
        {
            $em->flush();
            return true;
        }
        catch(Exception $e)
        {
            return false;
        }
    }

    public function deleteUser($user)
    {
        $em = $this->doctrine->em;

        $em->remove($user->getGoldfarm1());
        $em->remove($user->getGoldfarm2());
        $em->remove($user->getFoodfarm1());
        $em->remove($user->getFoodfarm2());
        $em->remove($user->getWoodfarm1());
        $em->remove($user->getWoodfarm2());

        $em->remove($user->getBarracks());
        $em->remove($user->getArchery());
        $em->remove($user->getStable());

        $em->remove($user->getSword());
        $em->remove($user->getSpear());
        $em->remove($user->getCavalry());
        $em->remove($user->getBow());

        foreach($user->getMessagesSent() as $message)
            $em->remove($message);

        foreach($user->getMessagesReceived() as $message)
            $em->remove($message);

        foreach($user->getReports() as $report)
            $em->remove($report);

        foreach($user->getReinforcementsSent() as $reinforcement)
            $em->remove($reinforcement);

        $em->remove($user);

        try
        {
            $em->flush();
            return true;
        }
        catch(Exception $e)
        {
            return false;
        }
    }

    public function getModerator($username)
    {
        $em = $this->doctrine->em;

        $query = "SELECT u FROM Entity\User u WHERE u.username = :username";
        $user = $em->createQuery($query)->setParameter('username', $username)->getResult();

        if ($user != null && strcmp($user[0]->getNation(), 'mod') == 0)
            return $user[0];
        else
            return false;
    }

    public function getRandomModerator()
    {
        $em = $this->doctrine->em;

        $query = "SELECT u FROM Entity\User u WHERE u.nation = 'mod'";
        $mods = $em->createQuery($query)->getResult();

        if ($mods != null)
        {
            $random_int = mt_rand();
            return $mods[$random_int % count($mods)];
        }
        else
            return false;
    }

    public function getAdmin($username)
    {
        $em = $this->doctrine->em;

        $query = "SELECT u FROM Entity\User u WHERE u.username = :username";
        $user = $em->createQuery($query)->setParameter('username', $username)->getResult();

        if ($user != null && strcmp($user[0]->getNation(), 'admin') == 0)
            return $user[0];
        else
            return false;
    }

    public function checkForNewReports($username)
    {
        $em = $this->doctrine->em;

        $query = "SELECT u FROM Entity\User u WHERE u.username = :username";
        $reports = $em->createQuery($query)->setParameter('username', $username)->getResult()[0]->getReports();

        $numOfUnreadReports = 0;

        foreach($reports as $report)
        {
            if ($report->getIsread() == 0)
                $numOfUnreadReports++;
        }

        return $numOfUnreadReports;
    }

    public function checkForNewMessages($username)
    {
        $em = $this->doctrine->em;

        $query = "SELECT u FROM Entity\User u WHERE u.username = :username";
        $messages = $em->createQuery($query)->setParameter('username', $username)->getResult()[0]->getMessagesReceived();

        $numOfUnreadMessages = 0;

        foreach($messages as $report)
        {
            if ($report->getIsread() == 0)
                $numOfUnreadMessages++;
        }

        return $numOfUnreadMessages;
    }

    public function getNation($username)
    {
        $em = $this->doctrine->em;

        $query = "SELECT u FROM Entity\User u WHERE u.username = :username";
        $user = $em->createQuery($query)->setParameter('username', $username)->getResult();

        return $user[0]->getNation();
    }

    public function sendResources($userSender, $userReceiver, $gold, $food, $wood)
    {
        $em = $this->doctrine->em;

        if ($userSender->getGold() >= $gold && $userSender->getFood() >= $food && $userSender->getWood() >= $wood)
        {
            $userSender->setGold($userSender->getGold() - $gold);
            $userSender->setFood($userSender->getFood() - $food);
            $userSender->setWood($userSender->getWood() - $wood);

            $userReceiver->setGold($userReceiver->getGold() + $gold);
            $userReceiver->setFood($userReceiver->getFood() + $food);
            $userReceiver->setWood($userReceiver->getWood() + $wood);

            try
            {
                $em->flush();
                return true;
            }
            catch (Exception $e)
            {
                return false;
            }
        }

        else
            return false;
    }

    public function getCurrentResources($username)
    {
        $em = $this->doctrine->em;

        $query = "SELECT u FROM Entity\User u WHERE u.username = :username";
        $user = $em->createQuery($query)->setParameter('username', $username)->getResult()[0];

        return $this->getCurrentResourcesUser($user);
    }

    public function getCurrentResourcesUser($user)
    {
        $this->updateResources($user);

        $resources = array();
        $resources['gold'] = $user->getGold();
        $resources['food'] = $user->getFood();
        $resources['wood'] = $user->getWood();

        return $resources;
    }

    public function updateResources($user)
    {
        $em = $this->doctrine->em;

        $time = new DateTime("now");
        $diff = $user->getTimestamp()->diff($time);
        $user->setTimestamp($time);
        $seconds = $diff->days * 24 * 60 * 60;
        $seconds += $diff->h * 60 * 60;
        $seconds += $diff->i * 60;
        $seconds += $diff->s;

        $production = $this->getCurrentProductionUser($user);

        $user->setGold($user->getGold() + intval(($seconds / 3600.0) * $production['gold']));
        $user->setFood($user->getFood() + intval(($seconds / 3600.0) * $production['food']));
        $user->setWood($user->getWood() + intval(($seconds / 3600.0) * $production['wood']));

        try
        {
            $em->flush();
            return true;
        }
        catch(Exception $e)
        {
            return false;
        }
    }

    public function canSendAttack($user)
    {
        $em = $this->doctrine->em;

        if ($user->getLastattack() == null)
        {
            $user->setLastattack(new DateTime("now"));
            try
            {
                $em->flush();
                return -2;
            }
            catch(Exception $e)
            {
                return 0;
            }
        }
        else
        {
            $em = $this->doctrine->em;

            if ($user->getNation() == "Dwarves")
                $timeToWait = 180;
            else if ($user->getNation() == "Orcs")
                $timeToWait = 60;
            else
                $timeToWait = 120;

            $time = new DateTime("now");
            $diff = $user->getLastattack()->diff($time);
            $seconds = $diff->days * 24 * 60 * 60;
            $seconds += $diff->h * 60 * 60;
            $seconds += $diff->i * 60;
            $seconds += $diff->s;

            if ($timeToWait > $seconds)
                return $timeToWait - $seconds;
            else
            {
                $user->setLastattack(new DateTime("now"));
                try
                {
                    $em->flush();
                    return -2;
                }
                catch(Exception $e)
                {
                    return 0;
                }
            }
        }
    }

    public function setGold($user, $value)
    {
        $em = $this->doctrine->em;

        $user->setTimestamp(new DateTime("now"));
        $user->setGold($value);

        try
        {
            $em->flush();
            return true;
        }
        catch(Exception $e)
        {
            return false;
        }
    }

    public function setFood($user, $value)
    {
        $em = $this->doctrine->em;

        $user->setTimestamp(new DateTime("now"));
        $user->setFood($value);

        try
        {
            $em->flush();
            return true;
        }
        catch(Exception $e)
        {
            return false;
        }
    }

    public function setWood($user, $value)
    {
        $em = $this->doctrine->em;

        $user->setTimestamp(new DateTime("now"));
        $user->setWood($value);

        try
        {
            $em->flush();
            return true;
        }
        catch(Exception $e)
        {
            return false;
        }
    }

    public function addResourcesUser($user, $gold, $food, $wood)
    {
        $em = $this->doctrine->em;

        $user->setGold($user->getGold() + $gold);
        $user->setFood($user->getFood() + $food);
        $user->setWood($user->getWood() + $wood);

        try
        {
            $em->flush();
            return true;
        }
        catch(Exception $e)
        {
            return false;
        }
    }

    public function takeResourcesUser($user, $gold, $food, $wood)
    {
        $em = $this->doctrine->em;

        $user->setGold($user->getGold() - $gold);
        $user->setFood($user->getFood() - $food);
        $user->setWood($user->getWood() - $wood);

        try
        {
            $em->flush();
            return true;
        }
        catch(Exception $e)
        {
            return false;
        }
    }

    public function takeResources($username, $gold, $food, $wood)
    {
        $user = $this->getPlayer($username);

        return $this->takeResourcesUser($user, $gold, $food, $wood);
    }

    public function getCurrentProduction($username)
    {
        $em = $this->doctrine->em;

        $query = "SELECT u FROM Entity\User u WHERE u.username = :username";
        $user = $em->createQuery($query)->setParameter('username', $username)->getResult()[0];

        return $this->getCurrentProductionUser($user);
    }

    public function getCurrentProductionUser($user)
    {
        $nation = $user->getNation();
        $goldfarm1Level = $user->getGoldfarm1()->getLevel();
        $goldfarm2Level = $user->getGoldfarm2()->getLevel();
        $foodfarm1Level = $user->getFoodfarm1()->getLevel();
        $foodfarm2Level = $user->getFoodfarm2()->getLevel();
        $woodfarm1Level = $user->getWoodfarm1()->getLevel();
        $woodfarm2Level = $user->getWoodfarm2()->getLevel();

        $production = array();

        if (strcmp($nation, 'Men') == 0)
        {
            $production['gold'] = intval(1500 * pow(1.5, $goldfarm1Level)) + intval(1500 * pow(1.5, $goldfarm2Level));
            $production['food'] = intval(1800 * pow(1.5, $foodfarm1Level)) + intval(1800 * pow(1.5, $foodfarm2Level));
            $production['wood'] = intval(1500 * pow(1.5, $woodfarm1Level)) + intval(1500 * pow(1.5, $woodfarm2Level));
        }
        elseif(strcmp($nation, 'Elves') == 0)
        {
            $production['gold'] = intval(1500 * pow(1.5, $goldfarm1Level)) + intval(1500 * pow(1.5, $goldfarm2Level));
            $production['food'] = intval(1500 * pow(1.5, $foodfarm1Level)) + intval(1500 * pow(1.5, $foodfarm2Level));
            $production['wood'] = intval(1800 * pow(1.5, $woodfarm1Level)) + intval(1800 * pow(1.5, $woodfarm2Level));
        }
        elseif(strcmp($nation, 'Dwarves') == 0)
        {
            $production['gold'] = intval(2100 * pow(1.5, $goldfarm1Level)) + intval(2100 * pow(1.5, $goldfarm2Level));
            $production['food'] = intval(1500 * pow(1.5, $foodfarm1Level)) + intval(1500 * pow(1.5, $foodfarm2Level));
            $production['wood'] = intval(1500 * pow(1.5, $woodfarm1Level)) + intval(1500 * pow(1.5, $woodfarm2Level));
        }
        else
        {
            $production['gold'] = intval(1400 * pow(1.5, $goldfarm1Level)) + intval(1400 * pow(1.5, $goldfarm2Level));
            $production['food'] = intval(1400 * pow(1.5, $foodfarm1Level)) + intval(1400 * pow(1.5, $foodfarm2Level));
            $production['wood'] = intval(1400 * pow(1.5, $woodfarm1Level)) + intval(1400 * pow(1.5, $woodfarm2Level));
        }

        return $production;
    }

    public function getCurrentProductionIncrement($username)
    {
        $production = $this->getCurrentProduction($username);

        $data = array();

        $data['goldinc'] =  intval(round((1.0 / ($production['gold'] / 3600.0)) * 1000));
        $data['foodinc'] = intval(round((1.0 / ($production['food'] / 3600.0)) * 1000));
        $data['woodinc'] = intval(round((1.0 / ($production['wood'] / 3600.0)) * 1000));

        return $data;
    }

    public function getAllPlayers()
    {
        $em = $this->doctrine->em;

        $query = "SELECT u FROM Entity\User u ORDER BY u.edompoints DESC";
        $users = $em->createQuery($query)->getResult();

        $data = array();

        foreach($users as $user)
        {
            if (strcmp($user->getNation(), 'admin') != 0 && strcmp($user->getNation(), 'mod') != 0)
            {
                $player = array();
                $player['username'] = $user->getUsername();
                $player['nation'] = $user->getNation();
                $player['edompoints'] = $user->getEdompoints();

                array_push($data, $player);
            }
        }

        return $data;
    }

    public function getAllPlayersForAdmin()
    {
        $em = $this->doctrine->em;

        $query = "SELECT u FROM Entity\User u ORDER BY u.edompoints DESC";
        $users = $em->createQuery($query)->getResult();

        $data = array();

        foreach($users as $user)
        {
            if (strcmp($user->getNation(), 'admin') != 0 && strcmp($user->getNation(), 'mod') != 0)
            {
                $player = array();
                $player['username'] = $user->getUsername();
                $player['nation'] = $user->getNation();
                $player['edompoints'] = $user->getEdompoints();
                $player['blocked'] = $user->getBlocked();

                array_push($data, $player);
            }
        }

        return $data;
    }

    public function getAllModerators()
    {
        $em = $this->doctrine->em;

        $query = "SELECT u FROM Entity\User u WHERE u.nation = 'mod'";
        $users = $em->createQuery($query)->getResult();

        $data = array();

        foreach($users as $user)
        {
            $player = array();
            $player['username'] = $user->getUsername();
            $player['password'] = $user->getPassword();

            array_push($data, $player);
        }

        return $data;
    }

    public function getAllMessages($username)
    {
        $em = $this->doctrine->em;

        $query = "SELECT u FROM Entity\User u WHERE u.username = :username";
        $user = $em->createQuery($query)->setParameter('username', $username)->getResult()[0];

        return $this->getAllMessagesUser($user);
    }

    public function getAllMessagesUser($user)
    {
        $messagesSent = $user->getMessagesSent();
        $messagesReceived = $user->getMessagesReceived();
        $data = array();

        foreach($messagesSent as $message)
        {
            $msg = array();
            $msg['idmes'] = $message->getIdmes();
            $msg['sender'] = $message->getUserSender()->getUsername();
            $msg['receiver'] = $message->getUserReceiver()->getUsername();
            $msg['subject'] = $message->getSubject();
            $msg['isread'] = $message->getIsread();

            array_push($data, $msg);
        }

        foreach($messagesReceived as $message)
        {
            $msg = array();
            $msg['idmes'] = $message->getIdmes();
            $msg['sender'] = $message->getUserSender()->getUsername();
            $msg['receiver'] = $message->getUserReceiver()->getUsername();
            $msg['subject'] = $message->getSubject();
            $msg['isread'] = $message->getIsread();

            array_push($data, $msg);
        }

        foreach ($data as $key => $row)
            $idmes[$key] = $row['idmes'];

        if ($data) array_multisort($idmes, SORT_DESC, $data);

        return $data;
    }

    public function getAllReports($username)
    {
        $em = $this->doctrine->em;

        $query = "SELECT u FROM Entity\User u WHERE u.username = :username";
        $user = $em->createQuery($query)->setParameter('username', $username)->getResult()[0];

        return $this->getAllReportsUser($user);
    }

    public function getAllReportsUser($user)
    {
        $reports = $user->getReports();
        $data = array();

        foreach($reports as $report)
        {
            $rep = array();
            $rep['idrep'] = $report->getIdrep();
            $rep['type'] = $report->getType();
            $rep['time'] = $report->getTime()->format('Y-m-d H:i:s');
            $rep['isread'] = $report->getIsread();

            array_push($data, $rep);
        }

        foreach ($data as $key => $row)
            $idrep[$key] = $row['idrep'];

        if ($data) array_multisort($idrep, SORT_DESC, $data);

        return $data;
    }

    public function getNumberOfSoldiers($username)
    {
        $em = $this->doctrine->em;

        $query = "SELECT u FROM Entity\User u WHERE u.username = :username";
        $user = $em->createQuery($query)->setParameter('username', $username)->getResult()[0];

        $soldiers = array();

        $soldiers['sword'] = $user->getSword()->getCount();
        $soldiers['spear'] = $user->getSpear()->getCount();
        $soldiers['cavalry'] = $user->getCavalry()->getCount();
        $soldiers['bow'] = $user->getBow()->getCount();

        return $soldiers;
    }

    public function getAllArmies($username)
    {
        $em = $this->doctrine->em;

        $query = "SELECT u FROM Entity\User u WHERE u.username = :username";
        $user = $em->createQuery($query)->setParameter('username', $username)->getResult()[0];

        return $this->getAllArmiesUser($user);
    }

    public function getAllArmiesUser($user)
    {
        $data = array();
        $sword = $user->getSword()->getCount();
        $spear = $user->getSpear()->getCount();
        $cavalry = $user->getCavalry()->getCount();
        $bow = $user->getBow()->getCount();

        $army = array();

        if($sword > 0)
        {
            $army['name'] = $this->getArmyFullName('Sword', $user->getNation());
            $army['count'] = $sword;
            array_push($data, $army);
        }

        if($spear > 0)
        {
            $army['name'] = $this->getArmyFullName('Spear', $user->getNation());
            $army['count'] = $spear;
            array_push($data, $army);
        }

        if($bow > 0)
        {
            $army['name'] = $this->getArmyFullName('Bow', $user->getNation());
            $army['count'] = $bow;
            array_push($data, $army);
        }

        if($cavalry > 0)
        {
            $army['name'] = $this->getArmyFullName('Cavalry', $user->getNation());
            $army['count'] = $cavalry;
            array_push($data, $army);
        }

        return $data;
    }

    public function getSentReinforcements($username)
    {
        $em = $this->doctrine->em;

        $query = "SELECT u FROM Entity\User u WHERE u.username = :username";
        $user = $em->createQuery($query)->setParameter('username', $username)->getResult()[0];

        return $this->getSentReinforcementsUser($user);
    }

    public function getSentReinforcementsUser($user)
    {
        $data = array();

        $reinforcementsSent = $user->getReinforcementsSent();
        foreach ($reinforcementsSent as $reinforcement)
        {
            $rein = array();
            $rein['player'] = $reinforcement->getUserReceiver()->getUsername();
            $rein['type'] = $this->getArmyFullName($reinforcement->getType(), $user->getNation());
            $rein['count'] = $reinforcement->getCount();
            $rein['id'] = $reinforcement->getIdrein();
            array_push($data, $rein);
        }

        foreach ($data as $key => $row)
            $player[$key] = $row['player'];

        if($data) array_multisort($player, SORT_DESC, $data);

        return $data;
    }

    public function getReceivedReinforcements($username)
    {
        $em = $this->doctrine->em;

        $query = "SELECT u FROM Entity\User u WHERE u.username = :username";
        $user = $em->createQuery($query)->setParameter('username', $username)->getResult()[0];

        return $this->getReceivedReinforcementsUser($user);
    }

    public function getReceivedReinforcementsUser($user)
    {
        $data = array();

        $reinforcementsReceived = $user->getReinforcementsReceived();
        foreach ($reinforcementsReceived as $reinforcement)
        {
            $rein = array();
            $rein['player'] = $reinforcement->getUserSender()->getUsername();
            $rein['type'] = $this->getArmyFullName($reinforcement->getType(), $reinforcement->getUserSender()->getNation());
            $rein['count'] = $reinforcement->getCount();
            $rein['id'] = $reinforcement->getIdrein();
            array_push($data, $rein);
        }

        foreach ($data as $key => $row)
            $player[$key] = $row['player'];

        if($data) array_multisort($player, SORT_DESC, $data);

        return $data;
    }

    public static function getArmyNames() #initializes private variable that contains soldier names
    {
        $data = array();
        $armyNames = array();

        $armyNames['Sword'] = 'Swordsman';
        $armyNames['Spear'] = 'Spearman';
        $armyNames['Bow'] = 'Archer';
        $armyNames['Cavalry'] = 'Horseman';
        $data['Men'] = $armyNames;

        $armyNames['Sword'] = 'Elven Swordsman';
        $armyNames['Spear'] = 'Elven Phalanx';
        $armyNames['Bow'] = 'Elven Archer';
        $armyNames['Cavalry'] = 'Elven Rider';
        $data['Elves'] = $armyNames;

        $armyNames['Sword'] = 'Dwarven Infantry';
        $armyNames['Spear'] = 'Dwarven Phalanx';
        $armyNames['Bow'] = 'Axe Thrower';
        $armyNames['Cavalry'] = 'Chariot';
        $data['Dwarves'] = $armyNames;

        $armyNames['Sword'] = 'Orc Swordsman';
        $armyNames['Spear'] = 'Orc Spearman';
        $armyNames['Bow'] = 'Orc Archer';
        $armyNames['Cavalry'] = 'Troll';
        $data['Orcs'] = $armyNames;

        M_user::$armyNames = $data;
    }

    public function getArmyFullName($type, $nation)
    {
        $names = M_user::$armyNames[$nation];
        return $names[$type];
    }

    public function getFarm($username, $type)
    {
        $user = $this->getPlayer($username);
        if ($user->getNation() == 'admin' || $user->getNation() == 'mod') return false;

        switch($type)
        {
            case "gold1":
                return $user->getGoldfarm1();
            case "gold2":
                return $user->getGoldfarm2();
            case "food1":
                return $user->getFoodfarm1();
            case "food2":
                return $user->getFoodfarm2();
            case "wood1":
                return $user->getWoodfarm1();
            case "wood2":
                return $user->getWoodfarm2();
            default:
                return false;
        }
    }

    public function getBuilding($username, $type){
        $user = $this->getPlayer($username);
        if ($user->getNation() == 'admin' || $user->getNation() == 'mod') return false;

        switch($type)
        {
            case "1":
                return $user->getBarracks();
            case "3":
                return $user->getStable();
            case "2":
                return $user->getArchery();
            default:
                return false;
        }
    }

    public function updateEdompointsUser($user, $edompointsToAdd)
    {
        $em = $this->doctrine->em;
        $user->setEdompoints($user->getEdompoints() + $edompointsToAdd);

        try
        {
            $em->flush();
            return true;
        }
        catch(Exception $e)
        {
            return false;
        }
    }

    public function updateEdompoints($username, $edompointsToAdd)
    {
        $user = $this->getPlayer($username);

        return $this->updateEdompointsUser($user, $edompointsToAdd);
    }
}

M_user::getArmyNames();
