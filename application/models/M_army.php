<?php


class M_army extends CI_Model
{
    public function updateCount($user, $buildingType, $soldierType, $count)
    {
        $em = $this->doctrine->em;

        if($buildingType == 'Barracks')
        {
            if($soldierType == 1)
                $army = $user->getSword();
            else
                $army = $user->getSpear();
        }
        else if($buildingType == 'Stable')
            $army = $user->getCavalry();
        else
            $army = $user->getBow();

        $army->setCount($army->getCount() + $count);

        try
        {
            $em->flush();
            return true;
        }
        catch(Exception $e)
        {
            return false;
        }
    }

    public function updateArmyCount($army, $count)
    {
        $em = $this->doctrine->em;

        $army->setCount($count);

        try
        {
            $em->flush();
            return true;
        }
        catch(Exception $e)
        {
            return false;
        }
    }
}