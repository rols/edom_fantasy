<?php

namespace Entity;

/**
 * User Model
 *
 * @Entity
 * @Table(name="army")
 */
class Army
{
    /**
     * @Id
     * @Column(type="integer", length=11, nullable=false, name="idarmy")
     * @GeneratedValue(strategy="AUTO")
     */
    protected $idarmy;

    /**
     * @Column(type="integer", length=11, unique=false,  nullable=false)
     */
    protected $count;

    /**
     * @Column(type="string", length=30, unique=false,  nullable=false)
     */
    protected $type;

    /**
     * @return mixed
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param mixed $count
     */
    public function setCount($count)
    {
        $this->count = $count;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

}
