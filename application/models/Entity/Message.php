<?php

namespace Entity;

/**
 * User Model
 *
 * @Entity
 * @Table(name="message")
 */
class Message
{
    /**
     * @Id
     * @Column(type="integer", length=11, nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    protected $idmes;

    /**
     * @Column(type="string", length=30, unique=false, nullable=false)
     */
    protected $subject;

    /**
     * @Column(type="text", unique=false, nullable=false)
     */
    protected $content;

    /**
     * @Column(type="boolean", unique=false, nullable=false)
     */
    protected $isread;

    /**
    * @ManyToOne(targetEntity="User", inversedBy="messagesSent")
     * @JoinColumn(name="idsender", referencedColumnName="iduser", nullable=false, unique=false, onDelete="CASCADE")
    */
    protected $userSender;

    /**
     * @ManyToOne(targetEntity="User", inversedBy="messagesReceived")
     * @JoinColumn(name="idreceiver", referencedColumnName="iduser", nullable=false, unique=false, onDelete="CASCADE")
     */
    protected $userReceiver;

    /**
     * @return mixed
     */
    public function getIdmes()
    {
        return $this->idmes;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param mixed $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getIsread()
    {
        return $this->isread;
    }

    /**
     * @param mixed $read
     */
    public function setIsread($isread)
    {
        $this->isread = $isread;
    }

    /**
     * @return mixed
     */
    public function getUserSender()
    {
        return $this->userSender;
    }

    /**
     * @param mixed $userSender
     */
    public function setUserSender($userSender)
    {
        $this->userSender = $userSender;
    }

    /**
     * @return mixed
     */
    public function getUserReceiver()
    {
        return $this->userReceiver;
    }

    /**
     * @param mixed $userReceiver
     */
    public function setUserReceiver($userReceiver)
    {
        $this->userReceiver = $userReceiver;
    }

}
