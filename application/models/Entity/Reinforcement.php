<?php

namespace Entity;

/**
 * User Model
 *
 * @Entity
 * @Table(name="reinforcement")
 */
class Reinforcement
{
    /**
     * @Id
     * @Column(type="integer", length=11, nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    protected $idrein;

    /**
     * @Column(type="integer", length=11, unique=false,  nullable=false)
     */
    protected $count;

    /**
     * @Column(type="string", length=30, unique=false,  nullable=false)
     */
    protected $type;

    /**
     * @ManyToOne(targetEntity="User", inversedBy="reinforcementsSent")
     * @JoinColumn(name="idsender", referencedColumnName="iduser", nullable=false, unique=false, onDelete="CASCADE")
     */
    protected $userSender;

    /**
     * @ManyToOne(targetEntity="User", inversedBy="reinforcementsReceived")
     * @JoinColumn(name="idreceiver", referencedColumnName="iduser", nullable=false, unique=false, onDelete="CASCADE")
     */
    protected $userReceiver;

    /**
     * @return mixed
     */
    public function getIdrein()
    {
        return $this->idrein;
    }

    /**
     * @return mixed
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param mixed $count
     */
    public function setCount($count)
    {
        $this->count = $count;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getUserSender()
    {
        return $this->userSender;
    }

    /**
     * @param mixed $userSender
     */
    public function setUserSender($userSender)
    {
        $this->userSender = $userSender;
    }

    /**
     * @return mixed
     */
    public function getUserReceiver()
    {
        return $this->userReceiver;
    }

    /**
     * @param mixed $userReceiver
     */
    public function setUserReceiver($userReceiver)
    {
        $this->userReceiver = $userReceiver;
    }
}
