<?php

namespace Entity;

/**
 * User Model
 *
 * @Entity
 * @Table(name="report")
 */
class Report
{
    /**
     * @Id
     * @Column(type="integer", length=11, nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    protected $idrep;

    /**
     * @Column(type="string", length=30, unique=false,  nullable=false)
     */
    protected $type;

    /**
     * @Column(type="text", unique=false,  nullable=false)
     */
    protected $content;

    /**
     * @Column(type="datetime", unique=false, nullable=false)
     */
    protected $time;

    /**
     * @Column(type="boolean", unique=false,  nullable=false)
     */
    protected $isread;

    /**
     * @ManyToOne(targetEntity="User", inversedBy="reports")
     * @JoinColumn(name="iduser", referencedColumnName="iduser", unique=false, nullable=false, onDelete="CASCADE")
     */
    protected $iduser;

    /**
     * @return mixed
     */
    public function getIdrep()
    {
        return $this->idrep;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param mixed $time
     */
    public function setTime($time)
    {
        $this->time = $time;
    }

    /**
     * @return mixed
     */
    public function getIsread()
    {
        return $this->isread;
    }

    /**
     * @param mixed $read
     */
    public function setIsread($isread)
    {
        $this->isread = $isread;
    }

    /**
     * @return mixed
     */
    public function getIduser()
    {
        return $this->iduser;
    }

    /**
     * @param mixed $iduser
     */
    public function setIduser($iduser)
    {
        $this->iduser = $iduser;
    }
}
