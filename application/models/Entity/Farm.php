<?php

namespace Entity;

/**
 * User Model
 *
 * @Entity
 * @Table(name="farm")
 */
class Farm
{
    /**
     * @Id
     * @Column(type="integer", length=11, nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    protected $idfarm;

    /**
     * @Column(type="integer", length=11, unique=false,  nullable=false)
     */
    protected $level;

    /**
     * @Column(type="string", length=4, unique=false,  nullable=false)
     */
    protected $type;

    /**
     * @return mixed
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param mixed $level
     */
    public function setLevel($level)
    {
        $this->level = $level;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

}
