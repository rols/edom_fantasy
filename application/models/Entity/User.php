<?php

namespace Entity;

/**
 * User Model
 *
 * @Entity
 * @Table(name="user")
 */
class User
{
    /**
     * @Id
     * @Column(type="integer", length=11, nullable=false, unique=true)
     * @GeneratedValue(strategy="AUTO")
     */
    protected $iduser;

    /**
     * @Column(type="string", length=20, unique=true, nullable=false)
     */
    protected $username;

    /**
     * @Column(type="string", length=30, unique=false, nullable=false)
     */
    protected $email;

    /**
     * @Column(type="string", length=20, unique=false, nullable=false)
     */
    protected $password;

    /**
     * @Column(type="string", length=7, unique=false, nullable=false)
     */
    protected $nation;

    /**
     * @Column(type="integer", length=11, unique=false, nullable=false)
     */
    protected $edompoints;

    /**
     * @Column(type="integer", length=11, unique=false, nullable=false)
     */
    protected $gold;

    /**
     * @Column(type="integer", length=11, unique=false, nullable=false)
     */
    protected $food;

    /**
     * @Column(type="integer", length=11, unique=false, nullable=false)
     */
    protected $wood;

    /**
     * @Column(type="datetime", unique=false, nullable=false)
     */
    protected $timestamp;

    /**
     * @Column(type="datetime", unique=false, nullable=true)
     */
    protected $lastattack;

    /**
     * @Column(type="boolean", length=11, unique=false, nullable=false)
     */
    protected $blocked;

    /**
     * @OneToOne(targetEntity="Farm", cascade={"persist", "remove"}, orphanRemoval=true)
     * @JoinColumn(name="goldfarm1", referencedColumnName="idfarm", nullable=false, unique=true)
     */
    protected $goldfarm1;

    /**
    *  @OneToOne(targetEntity="Farm", cascade={"persist", "remove"}, orphanRemoval=true)
    *  @JoinColumn(name="goldfarm2", referencedColumnName="idfarm", nullable=false, unique=true)
    */
    protected $goldfarm2;

    /**
     * @OneToOne(targetEntity="Farm", cascade={"persist", "remove"}, orphanRemoval=true)
     * @JoinColumn(name="foodfarm1", referencedColumnName="idfarm", nullable=false, unique=true)
     */
    protected $foodfarm1;

    /**
    * @OneToOne(targetEntity="Farm", cascade={"persist", "remove"}, orphanRemoval=true)
     * @JoinColumn(name="foodfarm2", referencedColumnName="idfarm", nullable=false, unique=true)
    */
    protected $foodfarm2;

    /**
     * @OneToOne(targetEntity="Farm", cascade={"persist", "remove"}, orphanRemoval=true)
     * @JoinColumn(name="woodfarm1", referencedColumnName="idfarm", nullable=false, unique=true)
     */
    protected $woodfarm1;

    /**
    * @OneToOne(targetEntity="Farm", cascade={"persist", "remove"}, orphanRemoval=true)
     * @JoinColumn(name="woodfarm2", referencedColumnName="idfarm", nullable=false, unique=true)
    */
    protected $woodfarm2;

    /**
     * @OneToOne(targetEntity="Building", cascade={"persist", "remove"}, orphanRemoval=true)
     * @JoinColumn(name="barracks", referencedColumnName="idbuild", nullable=false, unique=true)
    */
    protected $barracks;

    /**
     * @OneToOne(targetEntity="Building", cascade={"persist", "remove"}, orphanRemoval=true)
     * @JoinColumn(name="stable", referencedColumnName="idbuild", nullable=false, unique=true)
    */
    protected $stable;

    /**
     * @OneToOne(targetEntity="Building", cascade={"persist", "remove"}, orphanRemoval=true)
     * @JoinColumn(name="archery", referencedColumnName="idbuild", nullable=false, unique=true)
    */
    protected $archery;

    /**
     * @OneToOne(targetEntity="Army", cascade={"persist", "remove"}, orphanRemoval=true)
     * @JoinColumn(name="sword", referencedColumnName="idarmy", nullable=false, unique=true)
    */
    protected $sword;

    /**
     * @OneToOne(targetEntity="Army", cascade={"persist", "remove"}, orphanRemoval=true)
     * @JoinColumn(name="spear", referencedColumnName="idarmy", nullable=false, unique=true)
    */
    protected $spear;

    /**
     * @OneToOne(targetEntity="Army", cascade={"persist", "remove"}, orphanRemoval=true)
     * @JoinColumn(name="cavalry", referencedColumnName="idarmy", nullable=false, unique=true)
    */
    protected $cavalry;

    /**
     * @OneToOne(targetEntity="Army", cascade={"persist", "remove"}, orphanRemoval=true)
     * @JoinColumn(name="bow", referencedColumnName="idarmy", nullable=false, unique=true)
    */
    protected $bow;

    /**
    * @OneToMany(targetEntity="Reinforcement", mappedBy="userSender")
    */
    protected $reinforcementsSent;

    /**
     * @OneToMany(targetEntity="Reinforcement", mappedBy="userReceiver")
     */
    protected $reinforcementsReceived;

    /**
     * @OneToMany(targetEntity="Report", mappedBy="iduser")
     */
    protected $reports;

    /**
     * @OneToMany(targetEntity="Message", mappedBy="userSender")
     */
    protected $messagesSent;

    /**
     * @OneToMany(targetEntity="Message", mappedBy="userReceiver")
     */
    protected $messagesReceived;

    /**
     * @return mixed
     */
    public function getIduser()
    {
        return $this->iduser;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getNation()
    {
        return $this->nation;
    }

    /**
     * @param mixed $nation
     */
    public function setNation($nation)
    {
        $this->nation = $nation;
    }

    /**
     * @return mixed
     */
    public function getEdompoints()
    {
        return $this->edompoints;
    }

    /**
     * @param mixed $edompoints
     */
    public function setEdompoints($edompoints)
    {
        $this->edompoints = $edompoints;
    }

    /**
     * @return mixed
     */
    public function getGold()
    {
        return $this->gold;
    }

    /**
     * @param mixed $gold
     */
    public function setGold($gold)
    {
        $this->gold = $gold;
    }

    /**
     * @return mixed
     */
    public function getFood()
    {
        return $this->food;
    }

    /**
     * @param mixed $food
     */
    public function setFood($food)
    {
        $this->food = $food;
    }

    /**
     * @return mixed
     */
    public function getWood()
    {
        return $this->wood;
    }

    /**
     * @param mixed $wood
     */
    public function setWood($wood)
    {
        $this->wood = $wood;
    }

    /**
     * @return mixed
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @param mixed $timestamp
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    }

    /**
     * @return mixed
     */
    public function getLastattack()
    {
        return $this->lastattack;
    }

    /**
     * @param mixed $lastattack
     */
    public function setLastattack($lastattack)
    {
        $this->lastattack = $lastattack;
    }

    /**
     * @return mixed
     */
    public function getBlocked()
    {
        return $this->blocked;
    }

    /**
     * @param mixed $blocked
     */
    public function setBlocked($blocked)
    {
        $this->blocked = $blocked;
    }

    /**
     * @return mixed
     */
    public function getGoldfarm1()
    {
        return $this->goldfarm1;
    }

    /**
     * @param mixed $goldfarm1
     */
    public function setGoldfarm1($goldfarm1)
    {
        $this->goldfarm1 = $goldfarm1;
    }

    /**
     * @return mixed
     */
    public function getGoldfarm2()
    {
        return $this->goldfarm2;
    }

    /**
     * @param mixed $goldfarm2
     */
    public function setGoldfarm2($goldfarm2)
    {
        $this->goldfarm2 = $goldfarm2;
    }

    /**
     * @return mixed
     */
    public function getFoodfarm1()
    {
        return $this->foodfarm1;
    }

    /**
     * @param mixed $foodfarm1
     */
    public function setFoodfarm1($foodfarm1)
    {
        $this->foodfarm1 = $foodfarm1;
    }

    /**
     * @return mixed
     */
    public function getFoodfarm2()
    {
        return $this->foodfarm2;
    }

    /**
     * @param mixed $foodfarm2
     */
    public function setFoodfarm2($foodfarm2)
    {
        $this->foodfarm2 = $foodfarm2;
    }

    /**
     * @return mixed
     */
    public function getWoodfarm1()
    {
        return $this->woodfarm1;
    }

    /**
     * @param mixed $woodfarm1
     */
    public function setWoodfarm1($woodfarm1)
    {
        $this->woodfarm1 = $woodfarm1;
    }

    /**
     * @return mixed
     */
    public function getWoodfarm2()
    {
        return $this->woodfarm2;
    }

    /**
     * @param mixed $woodfarm2
     */
    public function setWoodfarm2($woodfarm2)
    {
        $this->woodfarm2 = $woodfarm2;
    }

    /**
     * @return mixed
     */
    public function getBarracks()
    {
        return $this->barracks;
    }

    /**
     * @param mixed $barracks
     */
    public function setBarracks($barracks)
    {
        $this->barracks = $barracks;
    }

    /**
     * @return mixed
     */
    public function getStable()
    {
        return $this->stable;
    }

    /**
     * @param mixed $stable
     */
    public function setStable($stable)
    {
        $this->stable = $stable;
    }

    /**
     * @return mixed
     */
    public function getArchery()
    {
        return $this->archery;
    }

    /**
     * @param mixed $archery
     */
    public function setArchery($archery)
    {
        $this->archery = $archery;
    }

    /**
     * @return mixed
     */
    public function getSword()
    {
        return $this->sword;
    }

    /**
     * @param mixed $sword
     */
    public function setSword($sword)
    {
        $this->sword = $sword;
    }

    /**
     * @return mixed
     */
    public function getSpear()
    {
        return $this->spear;
    }

    /**
     * @param mixed $spear
     */
    public function setSpear($spear)
    {
        $this->spear = $spear;
    }

    /**
     * @return mixed
     */
    public function getCavalry()
    {
        return $this->cavalry;
    }

    /**
     * @param mixed $cavalry
     */
    public function setCavalry($cavalry)
    {
        $this->cavalry = $cavalry;
    }

    /**
     * @return mixed
     */
    public function getBow()
    {
        return $this->bow;
    }

    /**
     * @param mixed $bow
     */
    public function setBow($bow)
    {
        $this->bow = $bow;
    }

    /**
     * @return mixed
     */
    public function getReinforcementsSent()
    {
        return $this->reinforcementsSent;
    }

    /**
     * @param mixed $reinforcementsSent
     */
    public function setReinforcementsSent($reinforcementsSent)
    {
        $this->reinforcementsSent = $reinforcementsSent;
    }

    /**
     * @return mixed
     */
    public function getReinforcementsReceived()
    {
        return $this->reinforcementsReceived;
    }

    /**
     * @param mixed $reinforcementsReceived
     */
    public function setReinforcementsReceived($reinforcementsReceived)
    {
        $this->reinforcementsReceived = $reinforcementsReceived;
    }

    /**
     * @return mixed
     */
    public function getReports()
    {
        return $this->reports;
    }

    /**
     * @param mixed $reports
     */
    public function setReports($reports)
    {
        $this->reports = $reports;
    }

    /**
     * @return mixed
     */
    public function getMessagesSent()
    {
        return $this->messagesSent;
    }

    /**
     * @param mixed $messagesSent
     */
    public function setMessagesSent($messagesSent)
    {
        $this->messagesSent = $messagesSent;
    }

    /**
     * @return mixed
     */
    public function getMessagesReceived()
    {
        return $this->messagesReceived;
    }

    /**
     * @param mixed $messagesReceived
     */
    public function setMessagesReceived($messagesReceived)
    {
        $this->messagesReceived = $messagesReceived;
    }

    
}
