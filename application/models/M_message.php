<?php

class M_message extends CI_Model
{
    public function sendMessage($sender, $receiver, $subject, $content)
    {
        $em = $this->doctrine->em;

        $newMessage = new Entity\Message();
        $newMessage->setUserSender($sender);
        $newMessage->setUserReceiver($receiver);
        $newMessage->setSubject($subject);
        $newMessage->setContent($content);
        $newMessage->setIsread(0);

        $em->persist($newMessage);
        try
        {
            $em->flush();
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    public function checkForAccess($idmes, $username)
    {
        $em = $this->doctrine->em;

        $query = "SELECT m FROM Entity\Message m WHERE m.idmes = :idmes";
        $message = $em->createQuery($query)->setParameter('idmes', $idmes)->getResult()[0];

        $query = "SELECT u FROM Entity\User u WHERE u.username = :username";
        $user = $em->createQuery($query)->setParameter('username', $username)->getResult()[0];

        if ($message->getUserSender()->getIduser() == $user->getIduser() || $message->getUserReceiver()->getIduser() == $user->getIduser())
            return true;
        else
            return false;
    }

    public function deleteMessage($idmes)
    {
        $em = $this->doctrine->em;

        $query = "SELECT m FROM Entity\Message m WHERE m.idmes = :idmes";
        $message = $em->createQuery($query)->setParameter('idmes', $idmes)->getResult()[0];

        $em->remove($message);
        try
        {
            $em->flush();
        }
        catch (Exception $e) {}
    }

    public function readMessage($idmes, $updateNeeded)
    {
        $em = $this->doctrine->em;

        $query = "SELECT m FROM Entity\Message m WHERE m.idmes = :idmes";
        $message = $em->createQuery($query)->setParameter('idmes', $idmes)->getResult()[0];


        $data = array();
        $data['subject'] = $message->getSubject();
        $data['content'] = $message->getContent();

        if ($updateNeeded == 'true')
        {
            $message->setIsread(1);
            try
            {
                $em->flush();
            }
            catch (Exception $e)
            {
                return false;
            }
        }

        return json_encode($data);
    }
}