<?php


class M_soldier extends CI_Model
{
    public static $factory;
    public static $soldiers;
    public static $raidPercentage;

    public $offence;
    public $defence;

    public function initialise($offence, $defence)
    {
        $this->offence = $offence;
        $this->defence = $defence;
    }

    public static function getSoldierName($nation, $type)
    {
        $names = M_soldier::$soldiers[$nation];
        return $names[$type];
    }

    public static function initSoldiersFactories()
    {
        M_soldier::$factory = array();
        M_soldier::$factory['Swordsman'] = new M_soldier();
        M_soldier::$factory['Swordsman']->initialise(60, 20);
        M_soldier::$factory['Spearman'] = new M_soldier();
        M_soldier::$factory['Spearman']->initialise(20, 70);
        M_soldier::$factory['Archer'] = new M_soldier();
        M_soldier::$factory['Archer']->initialise(40, 30);
        M_soldier::$factory['Horseman'] = new M_soldier();
        M_soldier::$factory['Horseman']->initialise(80, 40);
        M_soldier::$factory['Elven Swordsman'] = new M_soldier();
        M_soldier::$factory['Elven Swordsman']->initialise(65, 20);
        M_soldier::$factory['Elven Phalanx'] = new M_soldier();
        M_soldier::$factory['Elven Phalanx']->initialise(15, 80);
        M_soldier::$factory['Elven Archer'] = new M_soldier();
        M_soldier::$factory['Elven Archer']->initialise(80, 35);
        M_soldier::$factory['Elven Rider'] = new M_soldier();
        M_soldier::$factory['Elven Rider']->initialise(60, 45);
        M_soldier::$factory['Dwarven Infantry'] = new M_soldier();
        M_soldier::$factory['Dwarven Infantry']->initialise(35, 50);
        M_soldier::$factory['Dwarven Phalanx'] = new M_soldier();
        M_soldier::$factory['Dwarven Phalanx']->initialise(15, 100);
        M_soldier::$factory['Axe Thrower'] = new M_soldier();
        M_soldier::$factory['Axe Thrower']->initialise(25, 60);
        M_soldier::$factory['Chariot'] = new M_soldier();
        M_soldier::$factory['Chariot']->initialise(55, 30);
        M_soldier::$factory['Orc Swordsman'] = new M_soldier();
        M_soldier::$factory['Orc Swordsman']->initialise(45, 5);
        M_soldier::$factory['Orc Spearman'] = new M_soldier();
        M_soldier::$factory['Orc Spearman']->initialise(5, 50);
        M_soldier::$factory['Orc Archer'] = new M_soldier();
        M_soldier::$factory['Orc Archer']->initialise(40, 30);
        M_soldier::$factory['Troll'] = new M_soldier();
        M_soldier::$factory['Troll']->initialise(100, 50);

        M_soldier::$raidPercentage['Men'] = 0.25;
        M_soldier::$raidPercentage['Elves'] = 0.20;
        M_soldier::$raidPercentage['Dwarves'] = 0.15;
        M_soldier::$raidPercentage['Orcs'] = 0.3;
    }

    public static function initSoldierNames()
    {
        $data = array();
        $armyNames = array();

        $armyNames['Sword'] = 'Swordsman';
        $armyNames['Spear'] = 'Spearman';
        $armyNames['Bow'] = 'Archer';
        $armyNames['Cavalry'] = 'Horseman';
        $data['Men'] = $armyNames;

        $armyNames['Sword'] = 'Elven Swordsman';
        $armyNames['Spear'] = 'Elven Phalanx';
        $armyNames['Bow'] = 'Elven Archer';
        $armyNames['Cavalry'] = 'Elven Rider';
        $data['Elves'] = $armyNames;

        $armyNames['Sword'] = 'Dwarven Infantry';
        $armyNames['Spear'] = 'Dwarven Phalanx';
        $armyNames['Bow'] = 'Axe Thrower';
        $armyNames['Cavalry'] = 'Chariot';
        $data['Dwarves'] = $armyNames;

        $armyNames['Sword'] = 'Orc Swordsman';
        $armyNames['Spear'] = 'Orc Spearman';
        $armyNames['Bow'] = 'Orc Archer';
        $armyNames['Cavalry'] = 'Troll';
        $data['Orcs'] = $armyNames;

        M_soldier::$soldiers = $data;
    }
};

M_soldier::initSoldiersFactories();
M_soldier::initSoldierNames();