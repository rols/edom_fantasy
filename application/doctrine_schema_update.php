#!/usr/bin/env php
<?php

    $models_dir = dirname(__FILE__);

    chdir($models_dir . "/models");

    exec("mkdir ../models_backup", $out);
    print("Made temp dir application/models_backup\n");

    exec("mv M_* ../models_backup/", $out);
    print("Moved all models to models_backup\nExecuting doctrine update\n");

    exec("php ../doctrine orm:schema-tool:update --force", $out);
    foreach($out as $msg){
        print("Doctrine: " .$msg ."\n");
    }

    exec("mv ../models_backup/* ./", $out);
    exec("rmdir ../models_backup", $out);
    print("Moved back models\n");

?>
