<?php

class UserAccountDeletion_Test extends TestCase
{
    public function setUp(){
        $this->request('POST', "C_edom/login", ['username' => 'admin', 'password' => 'dandelion55555']);

    }

    public function testUserDelete(){
        $this->request('POST', "C_edom/register", ['username' => 'nikola2', 'email' => 'nikola@gmail.com', 'password' => 'nikola', 'repeatPassword' => 'nikola', 'nation' => 'Men']);
        $output = $this->request('POST', "C_admin/delete_user", ['isAjaxCalling' => true, 'username' => 'nikola2']);
        $this->assertEquals(1, $output);
    }

    /*
     * @depends test_user_delete
     */
    public function testUserDeleted(){
        $output = $this->request('GET', "C_admin/index");
        $this->assertNotContains('nikola2', $output);
    }

    public function testModeratorDeleted(){
        $output = $this->request('POST', "C_admin/delete_user", ['isAjaxCalling' => true, 'username' => 'moderator4']);
        $this->assertEquals(1, $output);
    }

}
