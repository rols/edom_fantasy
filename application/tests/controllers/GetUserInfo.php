<?php

class GetUserInfo_Test extends TestCase
{
    public function setUp(){
        $this->request('POST', "C_edom/login", ['username' => 'moderator1', 'password' => 'FeChDokG']);
    }

    public function testGetUserInfo(){
        $output = $this->request('GET', "C_player_profile_mod/index", ['username' => 'Nikola']);
        $this->assertContains("<title>Edom Fantasy - ModPlayer</title>", $output);
        $this->assertContains("Username: Nikola", $output);
    }
}

