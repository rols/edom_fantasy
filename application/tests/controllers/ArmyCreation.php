<?php

/**
 * Created by PhpStorm.
 * User: rols
 * Date: 6/17/16
 * Time: 11:06 PM
 */
class ArmyCreation_Test extends TestCase
{
  public function setUp()
  {
    $this->request('POST', "C_edom/login", ['username' => 'rols', 'password' => 'rols']);
  }

  public function testMessagesDelete()
  {
    $output = $this->request('POST', "C_building/make_soldiers", ["building" => 1, "soldierType" => 1, "numOfSoldiers" => 1, "isAjaxCalling" => true]);
    $this->assertContains("true", $output);
  }
}