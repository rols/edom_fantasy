<?php

/**
 * Created by PhpStorm.
 * User: rols
 * Date: 6/17/16
 * Time: 9:19 PM
 */
class ReportsRead_Test extends TestCase
{
  public function setUp()
  {
    $this->request('POST', "C_edom/login", ['username' => 'rols', 'password' => 'rols']);
  }

  public function testReportsRead()
  {
    $output = $this->request('POST', "C_reports/read_report", ["isAjaxCalling" => true, "idrep" => 5, "updateNeeded" => false]);
    $this->assertContains("Attack", $output);
  }
}