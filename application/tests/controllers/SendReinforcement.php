<?php

/**
 * Created by PhpStorm.
 * User: rols
 * Date: 6/17/16
 * Time: 11:16 PM
 */
class SendReinforcement_Test extends TestCase
{
  public function setUp()
  {
    $this->request('POST', "C_edom/login", ['username' => 'rols', 'password' => 'rols']);
  }

  public function testMessagesDelete()
  {
    $output = $this->request('POST', "C_player_profile/send_reinforcement", ["receiver" => "alk", "sword" => 0, "spear" => 0, "cavalry" => 0, "bow" => 1, "isAjaxCalling" => true]);
    $this->assertEquals(1, $output);
  }
}