<?php

/**
 * Created by PhpStorm.
 * User: rols
 * Date: 6/17/16
 * Time: 11:25 PM
 */
class UpgradeFarm_Test extends TestCase
{
  public function setUp()
  {
    $this->request('POST', "C_edom/login", ['username' => 'rols', 'password' => 'rols']);
  }

  public function testMessagesDelete()
  {
    $output = $this->request('POST', "C_farm/upgrade_farm", ["farm" => "food2", "isAjaxCalling" => true]);
    $this->assertContains("true", $output);
  }
}