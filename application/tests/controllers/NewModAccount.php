<?php

class NewModAccount_Test extends TestCase
{
    public function setUp(){
    $this->request('POST', "C_edom/login", ['username' => 'admin', 'password' => 'dandelion55555']);
}

    public function testModeratorCreation(){
        $output = $this->request('POST', "C_admin/create_new_mod", ['isAjaxCalling' => true]);
        $this->assertEquals(1, $output);
    }

    /*
     * @depends test_moderator_creation
     */
    public function testModeratorCreated(){
        $output = $this->request('GET', "C_admin/index", ['isAjaxCalling' => true]);
        $this->assertContains('moderator2', $output);
    }
}
