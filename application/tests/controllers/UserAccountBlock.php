<?php

class UserAccountBlock_Test extends TestCase
{
    public function setUp(){
        $this->request('POST', "C_edom/login", ['username' => 'admin', 'password' => 'dandelion55555']);
    }

    public function testBlockUser(){
        $output = $this->request('POST', "C_admin/block_user", ['isAjaxCalling' => true, 'username' => 'nikola3']);
        $this->assertEquals(1, $output);
    }
}
