<?php

class Login extends TestCase
{

    public function testFailedLogin(){
        $output = $this->request('POST', "C_edom/login", ['username' => 'Aleksa', 'password' => '1234']);
        $this->assertContains("<title>Edom Fantasy</title>", $output);
    }
}
