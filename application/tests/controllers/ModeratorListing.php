<?php

class ModeratorListing_Test extends TestCase
{
    public function setUp(){
        $this->request('POST', "C_edom/login", ['username' => 'admin', 'password' => 'dandelion55555']);
    }

    public function testModeratorListing(){
        $output = $this->request('GET', "C_admin/index");
        $this->assertContains('id="moderator1"', $output);
    }
}
