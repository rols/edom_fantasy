<?php

class UserInfoChange_Test extends TestCase
{
    public function setUp(){
        $this->request('POST', "C_edom/login", ['username' => 'admin', 'password' => 'dandelion55555']);
    }

    public function testUserInfoChange(){
        $output = $this->request('POST', "C_player_profile_admin/change_info",
            ['username' => 'nikola3',
                'isAjaxCalling' => true,
                'resgold' => '500000',
                'resfood' => '1500000',
                'reswood' => '50000',
                'gold1level' => '5',
                'gold2level' => '4',
                'food1level' => '5',
                'food2level' => '4',
                'wood1level' => '5',
                'wood2level' => '4',
                'barrackslevel' => '5',
                'stablelevel' => '4',
                'archerylevel' => '3',
                'sword' => '10000',
                'spear' => '20000',
                'cavalry' => '300000',
                'bow' => '400000']);
        $this->assertEquals(1, $output);
    }
}
