<?php

/**
 * Created by PhpStorm.
 * User: rols
 * Date: 6/17/16
 * Time: 9:13 PM
 */
class ReportsDeletion_Test extends TestCase
{
  public function setUp()
  {
    $this->request('POST', "C_edom/login", ['username' => 'rols', 'password' => 'rols']);
  }

  public function testReportsDelete()
  {
    $output = $this->request('POST', "C_reports/delete_report", ["isAjaxCalling" => true, "idrep" => 5]);
    $this->assertEquals(1, $output);
  }
}