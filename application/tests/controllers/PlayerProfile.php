<?php

class PlayerProfile extends TestCase
{
    public function setUp()
    {
        $this->request('POST', "C_edom/login", ['username' => 'Aleksa', 'password' => '123']);
    }

    public function testPlayerProfileOverview(){
        $output = $this->request('GET', "C_player_profile/index", ['username' => 'Andjela']);
        $this->assertContains("<title>Edom Fantasy - Player</title>", $output);
    }
}
