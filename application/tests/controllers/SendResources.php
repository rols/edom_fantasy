<?php

/**
 * Created by PhpStorm.
 * User: rols
 * Date: 6/17/16
 * Time: 11:19 PM
 */
class SendResources_Test extends TestCase
{
  public function setUp()
  {
    $this->request('POST', "C_edom/login", ['username' => 'rols', 'password' => 'rols']);
  }

  public function testMessagesDelete()
  {
    $output = $this->request('POST', "C_player_profile/send_resources", ["receiver" => "alk", "gold" => 100, "food" => 0, "wood" => 0, "isAjaxCalling" => true]);
    $this->assertContains(1, $output);
  }

}