<?php

/**
 * Created by PhpStorm.
 * User: rols
 * Date: 6/17/16
 * Time: 9:19 PM
 */
class MessagesRead_Test extends TestCase
{
  public function setUp()
  {
    $this->request('POST', "C_edom/login", ['username' => 'rols', 'password' => 'rols']);
  }

  public function testMessagesRead()
  {
    $output = $this->request('POST', "C_messages/read_message", ["idmes" => 1, "updateNeeded" => false, "isAjaxCalling" => true]);
    $this->assertContains("subject", $output);
  }
}