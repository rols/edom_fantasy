<?php

/**
 * Created by PhpStorm.
 * User: rols
 * Date: 6/17/16
 * Time: 10:50 PM
 */
class MessagesDeletion_Test extends TestCase
{
  public function setUp()
  {
    $this->request('POST', "C_edom/login", ['username' => 'rols', 'password' => 'rols']);
  }

  public function testMessagesDelete()
  {
    $output = $this->request('POST', "C_messages/delete_message", ["idmes" => 2, "isAjaxCalling" => true]);
    $this->assertEquals(1, $output);
  }
}