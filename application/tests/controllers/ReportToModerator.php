<?php

/**
 * Created by PhpStorm.
 * User: rols
 * Date: 6/17/16
 * Time: 11:01 PM
 */
class ReportToModerator_Test extends TestCase
{
  public function setUp()
  {
    $this->request('POST', "C_edom/login", ['username' => 'rols', 'password' => 'rols']);
  }

  public function testReportsRead()
  {
    $output = $this->request('POST', "C_messages/send_report_to_mod", ["isAjaxCalling" => true, "subject" => "some+report", "content" => "some+report+description"]);
    $this->assertEquals(1, $output);
  }
}