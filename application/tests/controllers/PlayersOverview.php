<?php

class PlayersOverview extends TestCase
{
    public function setUp()
    {
        $this->request('POST', "C_edom/login", ['username' => 'Aleksa', 'password' => '123']);
    }

    public function testPlayersOverview(){
        $output = $this->request('POST', "C_players/index");
        $this->assertContains("<title>Edom Fantasy - Players</title>", $output);
    }
}
