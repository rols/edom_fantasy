<?php

/**
 * Created by PhpStorm.
 * User: rols
 * Date: 6/17/16
 * Time: 11:18 PM
 */
class SendMessage_Test extends TestCase
{
  public function setUp()
  {
    $this->request('POST', "C_edom/login", ['username' => 'rols', 'password' => 'rols']);
  }

  public function testMessagesDelete()
  {
    $output = $this->request('POST', "C_player_profile/send_message", ["receiver" => "alk", "subject" => "some+message", "content" => "some+content", "isAjaxCalling" => true]);
    $this->assertEquals(1, $output);
  }
}