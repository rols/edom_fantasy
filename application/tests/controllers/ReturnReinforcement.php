<?php

/**
 * Created by PhpStorm.
 * User: rols
 * Date: 6/17/16
 * Time: 11:29 PM
 */
class ReturnReinforcement_Test extends TestCase
{
  public function setUp()
  {
    $this->request('POST', "C_edom/login", ['username' => 'rols', 'password' => 'rols']);
  }

  public function testMessagesDelete()
  {
    $output = $this->request('POST', "C_army/return_reinforcement", ["id" => 1, "isAjaxCalling" => true]);
    $this->assertContains("true", $output);
  }
}