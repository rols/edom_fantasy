<?php

class ToModCommunication_Test extends TestCase
{
    public function setUp(){
        $this->request('POST', "C_edom/login", ['username' => 'admin', 'password' => 'dandelion55555']);
    }

    public function testToModCommunication(){
        $output = $this->request('POST', "C_admin/send_message_to_mod", ['isAjaxCalling' => true, 'subject' => 'New message', 'content' => 'Contenttt', 'receiver' => 'moderator1']);
        $this->assertEquals(1, $output);
    }
}
