<?php

/**
 * Created by PhpStorm.
 * User: rols
 * Date: 6/17/16
 * Time: 11:22 PM
 */
class UpgradeBuilding_Test extends TestCase
{
  public function setUp()
  {
    $this->request('POST', "C_edom/login", ['username' => 'rols', 'password' => 'rols']);
  }

  public function testMessagesDelete()
  {
    $output = $this->request('POST', "C_building/upgrade_building", ["building" => 3, "isAjaxCalling" => true]);
    $this->assertContains("true", $output);
  }
}