<?php

class ToAdminCommunication_Test extends TestCase
{
    public function setUp(){
        $this->request('POST', "C_edom/login", ['username' => 'moderator1', 'password' => 'FeChDokG']);
    }

    public function testToAdminCommunication(){
        $output = $this->request('POST', "C_messages_mod/send_report_to_admin", ['isAjaxCalling' => true, 'subject' => 'Subjec ttest', 'content' => 'content_test']);
        $this->assertEquals(1, $output);
    }
}
