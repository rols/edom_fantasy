<?php

class ArmyOverview extends TestCase
{
    public function setUp()
    {
        $this->request('POST', "C_edom/login", ['username' => 'Aleksa', 'password' => '123']);
    }

    public function testReportsOverview(){
        $output = $this->request('POST', "C_army/index");
        $this->assertContains("<title>Edom Fantasy - Army</title>", $output);
    }
}
