<?php

class MessagesOverview extends TestCase
{
    public function setUp()
    {
        $this->request('POST', "C_edom/login", ['username' => 'Aleksa', 'password' => '123']);
    }

    public function testReportsOverview()
    {
        $output = $this->request('POST', "C_messages/index");
        $this->assertContains("<title>Edom Fantasy - Messages</title>", $output);
    }
}
