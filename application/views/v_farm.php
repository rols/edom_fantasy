<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Edom Fantasy - Farm</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
		
		<link rel="icon" href="assets/images/bgr/favicon.ico" type="image" sizes="16x16">
		
		<style>
			body {
				background-image:url("assets/images/bgr/nationbgr.jpg");
			}
			@font-face {font-family: "Vivaldi Italic V1";
				src: url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.eot"); /* IE9*/
				src: url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.eot?#iefix") format("embedded-opentype"), /* IE6-IE8 */
				url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.woff2") format("woff2"), /* chrome、firefox */
				url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.woff") format("woff"), /* chrome、firefox */
				url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.ttf") format("truetype"), /* chrome、firefox、opera、Safari, Android, iOS 4.2+*/
				url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.svg#Vivaldi Italic V1") format("svg"); /* iOS 4.1- */
			}
			.navbar-brand {
				font-family: Vivaldi Italic V1;
				font-weight: bold;
				font-size: 40px;
				color: white;
			}
			h1 {
				font-family: Vivaldi Italic V1;
				font-weight: bold;
				font-size: 40px;
				color: white;
			}
			h4 {
				font-family: Vivaldi Italic V1;
				font-weight: bold;
				font-size: 20px;
				color: white;
			}
			h5 {
				font-weight: bold;
				font-size: 15px;
				color: white;
			}
		</style>
	</head>
	
	<body>
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="about">Edom Fantasy</a>
				</div>
				<ul class="nav navbar-nav">
					<li><a href="home"><span class="glyphicon glyphicon-home"></span> Home</a></li>
					<li><a href="players">Players</a></li>
					<li><a href="reports">Reports<?php if ($numOfReports != 0) echo " (".$numOfReports.")"; ?></a></li>
					<li><a href="messages">Messages<?php if ($numOfMessages != 0) echo " (".$numOfMessages.")"; ?></a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><a><span class="glyphicon glyphicon-queen"></span><span id="goldinc"> <?php echo $currentResources['gold']; ?></span></a></li>
					<li><a><span class="glyphicon glyphicon-grain"></span><span id="foodinc"> <?php echo $currentResources['food']; ?></span></a></li>
					<li><a><span class="glyphicon glyphicon-tree-conifer"></span><span id="woodinc"> <?php echo $currentResources['wood']; ?></span></a></li>
					<li><a></a></li> <!--Empty space-->
					<li><a href="logout"><span class="glyphicon glyphicon-log-out"></span> Log out</a></li>
				</ul>
			</div>
		</nav>

		<div class="container text-center" id="container">
			<h1 id="type"><?php echo $farm; ?> farm</h1>
			<h5>Level: <?php echo $level; ?></h5>
			<h5><?php echo $farm ?> Production: <?php echo $production; ?>/h</h5>
			<button type="button" id="button" class="btn btn-primary" data-toggle="tooltip" data-placement="right" title="Click to upgrade to next level!">Upgrade</button>
			<h5><span class="glyphicon glyphicon-queen"></span> <span id="goldNeeded"> <?php if($level < 5 ) echo $goldNeeded; else echo "-"; ?> </span>
				<span class="glyphicon glyphicon-grain"></span> <span id="foodNeeded"> <?php if($level < 5) echo $foodNeeded; else echo "-"; ?> </span>
				<span class="glyphicon glyphicon-tree-conifer"></span> <span id="woodNeeded"> <?php if($level < 5) echo $woodNeeded; else echo "-"; ?> </span> </h5>
			<div align="center" style="padding-top:1%">
				<img src="<?php echo $picture; ?>" class="img-responsive" style="width:500px">
			</div>
		</div>

		<script>
			$(document).ready(function()
			{
				$('[data-toggle="tooltip"]').tooltip();

				var goldDelay = <?php echo $productionIncrement['goldinc']; ?>;
				var foodDelay = <?php echo $productionIncrement['foodinc']; ?>;
				var woodDelay = <?php echo $productionIncrement['woodinc']; ?>;
				var nowGold, nowFood, nowWood, beforeGold = new Date(), beforeFood = new Date(), beforeWood = new Date();

				setInterval(function () {
					nowGold = new Date();
					var elapsedTime = (nowGold.getTime() - beforeGold.getTime());
					$('#goldinc').html(function (i, val) {
						var value;
						if (elapsedTime > goldDelay)
							value = Math.floor(elapsedTime / goldDelay);
						else
							value = 1;

						beforeGold = new Date();
						return (parseInt(val) + value).toString();
					})
				}, goldDelay);

				setInterval(function () {
					nowFood = new Date();
					var elapsedTime = (nowFood.getTime() - beforeFood.getTime());
					$('#foodinc').html(function (i, val) {
						var value;
						if (elapsedTime > foodDelay)
							value = Math.floor(elapsedTime / foodDelay);
						else
							value = 1;

						beforeFood = new Date();
						return (parseInt(val) + value).toString();
					})
				}, foodDelay);

				setInterval(function () {
					nowWood = new Date();
					var elapsedTime = (nowWood.getTime() - beforeWood.getTime());
					$('#woodinc').html(function (i, val) {
						var value;
						if (elapsedTime > woodDelay)
							value = Math.floor(elapsedTime / woodDelay);
						else
							value = 1;

						beforeWood = new Date();
						return (parseInt(val) + value).toString();
					})
				}, woodDelay);


				var level = parseInt("<?php echo $level?>");
				if(level == 5 ) $('#button').prop("disabled", true);

				$('#button').on('click', function () {

					var form_data = {
						farm: "<?php echo $idfarm; ?>",
						isAjaxCalling: true
					};

					$.ajax({
						url: '<?php echo site_url('upgrade-farm'); ?>',
						type: 'POST',
						data: form_data,
						success: function(msg){
							if(msg)
							{
								if (msg.outcome == "true")
								{
									displayMessage("Farm has been upgraded.", "alert-success", $('#container'));
									setTimeout(removeMessages, 1500);
								}
								else
								{
									displayMessage("Not enough resources.", "alert-danger", $('#container'));
									setTimeout(removeMessages, 1500);
								}
							}
						},
						dataType: "json"
					});

					function displayMessage(msg, alert, field)
					{
						var t1 = "<div id=\"msgAlert\" class=\"alert " + alert + " fade in flash_message\">";
						var t2 = "<strong>" + msg + "</strong>";
						var t3 = "</div>";

						field.before(t1 + t2 + t3);
					}

					function removeMessages()
					{
						$('#msgAlert').remove();
						location.reload(true);
					}
				});

				});
		</script>

	</body>
	
</html>