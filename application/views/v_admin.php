<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Edom Fantasy - Admin</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
		
		<link rel="icon" href="assets/images/bgr/favicon.ico" type="image" sizes="16x16">
		
		<style>
			body {
				background-image:url("assets/images/bgr/nationbgr.jpg");
			}
			@font-face {font-family: "Vivaldi Italic V1";
				src: url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.eot"); /* IE9*/
				src: url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.eot?#iefix") format("embedded-opentype"), /* IE6-IE8 */
				url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.woff2") format("woff2"), /* chrome、firefox */
				url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.woff") format("woff"), /* chrome、firefox */
				url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.ttf") format("truetype"), /* chrome、firefox、opera、Safari, Android, iOS 4.2+*/
				url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.svg#Vivaldi Italic V1") format("svg"); /* iOS 4.1- */
			}
			thead {
				font-weight: bold;
				color: white;
				font-size: 15px;
			}
			h3 {
				font-family: Vivaldi Italic V1;
				font-weight: bold;
				font-size: 40px;
			}
			textarea {
				resize: none;
			}
			.navbar-brand {
				font-family: Vivaldi Italic V1;
				font-weight: bold;
				font-size: 40px;
				color: white;
			}
			.panel-transparent {
				background: none;
			}
			.panel-heading {
				font-family: Vivaldi Italic V1;
				font-weight: bold;
				color: white;
				font-size: 40px;
			}
		</style>
	</head>
	
	<body>
		<nav class="navbar navbar-inverse" style="margin-bottom:5px">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="about">Edom Fantasy</a>
				</div>
				<ul class="nav navbar-nav">
					<li class="active"><a href="admin"><span class="glyphicon glyphicon-home"></span> Home</a></li>
					<li><a href="messages-admin">Messages<?php if ($numOfMessages != 0) echo " (".$numOfMessages.")"; ?></a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="logout"><span class="glyphicon glyphicon-log-out"></span> Log out</a></li>
				</ul>
			</div>
		</nav>
		
	
		<!--Open player profile with all informations-->
		<div class="container" align="right" style="padding-right:0px; margin-right:5px">
			<form role="search">
				<div class="form-group input-group col-sm-3">
					<input form="" id="player" type="text" class="form-control" placeholder="Search for player..." maxlength="20" onkeydown="searchPlayer_enter(event)">
					<span class="input-group-btn">
						<button id="playerbtn" class="btn btn-default" type="button" onClick="searchPlayer()">
							<span class="glyphicon glyphicon-search"></span>
						</button>
					</span>
				</div>
			</form>
		</div>
		
		<div class="panel panel-transparent">
			<div class="panel-heading" align="center">Moderators</div>
			<div class="container">
				<table class="table" style="width: 50%" align="center">
					<thead>
						<tr>
							<th class="col-md-3"><p align="center">Username</p></th>
							<th class="col-md-3"><p align="center">Password</p></th>
							<th class="col-md-2"><p align="center">Send Message</p></th>
							<th class="col-md-1"><p align="center">Delete</p></th>
						</tr>
					</thead>
					<tbody>

					<?php
					foreach ($moderators as $mod)
					{ ?>

						<tr class="active" id="<?php echo $mod['username']; ?>">
							<td><p align="center"><?php echo $mod['username']; ?></p></td>
							<td><p align="center"><?php echo $mod['password']; ?></p></td>
							<td><p align="center"><a class="sendmessage" href="#" data-toggle="modal" data-target="#sendmessagemodal"><span class="glyphicon glyphicon-envelope" style="color:black"></span></a></p></td>
							<td><p align="center"><a class="icon" href="#"><span class="glyphicon glyphicon-trash" style="color:black"></span></a></p></td>
						</tr>

						<?php
					} ?>

					</tbody>
				</table>
			</div>
		</div>
		
		<div align="center" style="padding-bottom:2%"><button id="createnewmod" type="button" class="btn btn-primary">Create New Moderator</button></div>
		
		<div class="panel panel-transparent">
			<div class="panel-heading" align="center">Players</div>
			<div class="container">
				<table class="table" style="width: 50%" align="center">
					<thead>
						<tr>
							<th class="col-md-1"><p align="center">Rank</p></th>
							<th class="col-md-3"><p align="center">Player</p></th>
							<th class="col-md-2"><p align="center">Nation</p></th>
							<th class="col-md-3"><p align="center">Edom Points</p></th>
							<th class="col-md-1"><p align="center">Blocked</p></th>
							<th class="col-md-1"><p align="center">Delete</p></th>
						</tr>
					</thead>
					<tbody>

					<?php
					$rank = 1;
					foreach ($players as $player)
					{ ?>

						<tr class="active" id="<?php echo $player['username']; ?>">
							<td><p align="center"><?php echo $rank++ . "."; ?></p></td>
							<td><a href="player-profile-admin?username=<?php echo $player['username'] ?>" style="color:black"><p align="center"><b><?php echo $player['username']; ?></b></p></a></td>
							<td><p align="center"><?php echo $player['nation']; ?></p></td>
							<td><p align="center"><?php echo $player['edompoints']; ?></p></td>
							<td><p align="center"><a href="#" class="blockuser"><span class="<?php if($player['blocked'] == 0) echo "glyphicon glyphicon-remove"; else echo "glyphicon glyphicon-ok"; ?>" style="color:black"></span></a></p></td>
							<td><p align="center"><a href="#" class="icon"><span class="glyphicon glyphicon-trash" style="color:black"></span></a></p></td>
						</tr>

						<?php
					} ?>

					</tbody>
				</table>
			</div>
		</div>

		<div class="modal fade" id="sendmessagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h3 class="modal-title" id="myModalLabel" align="center" style="font-family: Vivaldi Italic V1; font-weight: bold; font-size: 40px;">Send Message</h3>
					</div>
					<div class="modal-body">

						<?php
						$attributes = array("role" => "form", "id" => "form_send-message");
						echo form_open('send-message-to-mod', $attributes);
						?>

						<div class="form-group">
							<label for="subject">Subject:</label>
							<input type="text" class="form-control" id="subject" maxlength="30" style="text-align:left">
							<label for="content">Content:</label>
							<textarea class="form-control" rows="5" id="content" maxlength="200"></textarea>
						</div>
						<div class="form-group" align="center">
							<button type="submit" class="btn btn-default">Send</button>
						</div>

						<?php
						echo form_close();
						?>

					</div>
				</div>
			</div>
		</div>


		<script>
			$(document).ready(function()
			{
				var msgReceiver;

				$('.sendmessage').on('click', function()
				{
					msgReceiver = $(this).closest('tr').attr('id');
				});

				$('#createnewmod').on('click', function()
				{
					var form_data = {
						isAjaxCalling: true
					};
					$.ajax({
						url: '<?php echo site_url('create-new-mod'); ?>',
						type: 'POST',
						data: form_data,
						success: function(msg)
						{
							location.reload(true)
						}
					});
				});

				$('.icon').on('click', function (event)
				{
					event.preventDefault();

					var confirmDeletion = confirm("Are you sure you want to delete this user?");
					if (confirmDeletion == true)
					{
						var username = $(this).closest('tr').attr('id');
						var form_data = {
							username: username,
							isAjaxCalling: true
						};
						$.ajax({
							url: '<?php echo site_url('delete-user'); ?>',
							type: 'POST',
							data: form_data,
							success: function(msg)
							{
								location.reload(true)
							}
						});
					}
				});

				$('.blockuser').on('click', function (event)
				{
					event.preventDefault();

					var confirmBlockade = confirm("Are you sure you want to block this user?");
					if (confirmBlockade == true)
					{
						var blockClass = $(this).find('span');
						var blockClassAtr = blockClass.attr('class');
						if (blockClassAtr == "glyphicon glyphicon-remove")
						{
							blockClass.removeClass("glyphicon glyphicon-remove");
							blockClass.addClass("glyphicon glyphicon-ok");
						}
						else
						{
							blockClass.removeClass("glyphicon glyphicon-ok");
							blockClass.addClass("glyphicon glyphicon-remove");
						}

						var username = $(this).closest('tr').attr('id');
						var form_data = {
							username: username,
							isAjaxCalling: true
						};
						$.ajax({
							url: '<?php echo site_url('block-user'); ?>',
							type: 'POST',
							data: form_data
						});
					}
				});

				var	formSendMessage = $('#form_send-message');
				formSendMessage.submit(function(event)
				{
					removeMessages();

					var subject = $.trim($('#subject').val()),
						content = $.trim($('#content').val());

					if (content.length == 0)
					{
						displayMessage("Fill the content.", "alert-danger", formSendMessage);
						return false;
					}
					else
					{
						if (subject.length == 0)
							subject = 'No Subject';


						/* AJAX CALL */
						var form_data = {
							receiver: msgReceiver,
							subject: subject,
							content: content,
							isAjaxCalling: true
						};
						$.ajax({
							url: '<?php echo site_url('send-message-to-mod'); ?>',
							type: 'POST',
							data: form_data,
							success: function(msg) {
								if (msg == true)
								{
									$('#subject').val("");
									$('#content').val("");
									displayMessage("Message sent.", "alert-success", formSendMessage);
								}

								else
									displayMessage("Message not sent.", "alert-danger", formSendMessage);
							}
						});
						return false;
					}
				});

				function displayMessage(msg, alert, field)
				{
					var t1 = "<div id=\"msgAlert\" class=\"alert " + alert + " fade in flash_message\">";
					var t2 = "<strong>" + msg + "</strong>";
					var t3 = "</div>";

					field.before(t1 + t2 + t3);
				}

				function removeMessages()
				{
					$('#msgAlert').remove();
				}

				$('#sendmessage').on('click', function() {removeMessages();});
			});

			function searchPlayer()
			{
				window.open("player-profile-admin?username=" + document.getElementById('player').value, "_self");
			}

			function searchPlayer_enter(event)
			{
				if (event.keyCode == 13)
					window.open("player-profile-admin?username=" + document.getElementById('player').value, "_self");
			}
		</script>
		
	</body>
	
</html>