<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Edom Fantasy - Player</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
		
		<link rel="icon" href="assets/images/bgr/favicon.ico" type="image" sizes="16x16">
		
		<style>
			body {
				background-image:url("assets/images/bgr/nationbgr.jpg");
			}
			@font-face {font-family: "Vivaldi Italic V1";
				src: url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.eot"); /* IE9*/
				src: url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.eot?#iefix") format("embedded-opentype"), /* IE6-IE8 */
				url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.woff2") format("woff2"), /* chrome、firefox */
				url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.woff") format("woff"), /* chrome、firefox */
				url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.ttf") format("truetype"), /* chrome、firefox、opera、Safari, Android, iOS 4.2+*/
				url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.svg#Vivaldi Italic V1") format("svg"); /* iOS 4.1- */
			}
			p {
				font-family: Vivaldi Italic V1;
				font-weight: bold;
				font-size: 30px;
				color: white;
			}
			h3 {
				font-family: Vivaldi Italic V1;
				font-weight: bold;
				font-size: 40px;
			}
			input {
				text-align: center;
			}
			textarea {
				resize: none;
			}
			.navbar-brand {
				font-family: Vivaldi Italic V1;
				font-weight: bold;
				font-size: 40px;
				color: white;
			}
		</style>
	</head>
	
	<body>
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="about">Edom Fantasy</a>
				</div>
				<ul class="nav navbar-nav">
					<li><a href="home"><span class="glyphicon glyphicon-home"></span> Home</a></li>
					<li><a href="players">Players</a></li>
					<li><a href="reports">Reports<?php if ($numOfReports != 0) echo " (".$numOfReports.")"; ?></a></li>
					<li><a href="messages">Messages<?php if ($numOfMessages != 0) echo " (".$numOfMessages.")"; ?></a></li>
				</ul>
				<!--Current resources Gold, Food, Wood-->
				<ul class="nav navbar-nav navbar-right">
					<li><a><span class="glyphicon glyphicon-queen"></span><span id="goldinc"> <?php echo $currentResources['gold']; ?></span></a></li>
					<li><a><span class="glyphicon glyphicon-grain"></span><span id="foodinc"> <?php echo $currentResources['food']; ?></span></a></li>
					<li><a><span class="glyphicon glyphicon-tree-conifer"></span><span id="woodinc"> <?php echo $currentResources['wood']; ?></span></a></li>
					<li><a></a></li> <!--Empty space-->
					<li><a href="logout"><span class="glyphicon glyphicon-log-out"></span> Log out</a></li>
				</ul>
			</div>
		</nav>
		
		
		<div align="center">

			<div>
				<img src="<?php echo $picture; ?>" class="img-thumbnail" width="450px" alt="Nation">
			</div>
			
			<div style="padding-top:1%">
				<p>Username: <?php echo $username; ?></p>
				<p>Nation: <?php echo $nation; ?></p>
				<p>Edom Points: <?php echo $edompoints; ?></p>
				<br>
				<p><a id="sendattack" href="#" data-toggle="modal" data-target="#sendattackmodal" style="color:white">Send Attack</a></p>
				<p><a id="sendreinforcement" href="#" data-toggle="modal" data-target="#sendreinforcementmodal" style="color:white">Send Reinforcement</a></p>
				<p><a id="sendresources" href="#" data-toggle="modal" data-target="#sendresourcesmodal" style="color:white">Send Resources</a></p>
				<p><a id="sendmessage" href="#" data-toggle="modal" data-target="#sendmessagemodal" style="color:white">Send Message</a></p>
			</div>
		</div>
		
		
		<div class="modal fade" id="sendattackmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h3 class="modal-title" id="myModalLabel" align="center">Send Attack</h3>
					</div>
					<div class="modal-body">

						<?php
						$attributes = array("role" => "form", "id" => "form_send-attack");
						echo form_open('send-attack', $attributes);
						?>

							<div class="form-group" align="center">
								<label for="sword"><?php echo $army['sword'] . " ("; echo $numOfSoldiers['sword'] . ")"; ?></label>
								<input type="text" class="form-control" id="sword" maxlength="10" style="width:50%">
								<label for="spear"><?php echo $army['spear'] . " ("; echo $numOfSoldiers['spear'] . ")"; ?></label>
								<input type="text" class="form-control" id="spear" maxlength="10" style="width:50%">
								<label for="cavalry"><?php echo $army['cavalry'] . " ("; echo $numOfSoldiers['cavalry'] . ")"; ?></label>
								<input type="text" class="form-control" id="cavalry" maxlength="10" style="width:50%">
								<label for="bow"><?php echo $army['bow'] . " ("; echo $numOfSoldiers['bow'] . ")"; ?></label>
								<input type="text" class="form-control" id="bow" maxlength="10" style="width:50%">
							</div>
							<div class="form-group" align="center">
								<button type="submit" class="btn btn-default">Send</button>
							</div>

						<?php
						echo form_close();
						?>

					</div>
				</div>
			</div>
		</div>
		
		<div class="modal fade" id="sendreinforcementmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h3 class="modal-title" id="myModalLabel" align="center">Send Reinforcement</h3>
					</div>
					<div class="modal-body">

						<?php
						$attributes = array("role" => "form", "id" => "form_send-reinforcement");
						echo form_open('send-reinforcement', $attributes);
						?>

							<div class="form-group" align="center">
								<label for="sword"><?php echo $army['sword'] . " ("; echo $numOfSoldiers['sword'] . ")"; ?></label>
								<input type="text" class="form-control" id="r_sword" maxlength="10" style="width:50%">
								<label for="spear"><?php echo $army['spear'] . " ("; echo $numOfSoldiers['spear'] . ")"; ?></label>
								<input type="text" class="form-control" id="r_spear" maxlength="10" style="width:50%">
								<label for="cavalry"><?php echo $army['cavalry'] . " ("; echo $numOfSoldiers['cavalry'] . ")"; ?></label>
								<input type="text" class="form-control" id="r_cavalry" maxlength="10" style="width:50%">
								<label for="archery"><?php echo $army['bow'] . " ("; echo $numOfSoldiers['bow'] . ")"; ?></label>
								<input type="text" class="form-control" id="r_bow" maxlength="10" style="width:50%">
							</div>
							<div class="form-group" align="center">
								<button type="submit" class="btn btn-default">Send</button>
							</div>

						<?php
						echo form_close();
						?>

					</div>
				</div>
			</div>
		</div>
		
		<div class="modal fade" id="sendresourcesmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h3 class="modal-title" id="myModalLabel" align="center">Send Resources</h3>
					</div>
					<div class="modal-body">

						<?php
						$attributes = array("role" => "form", "id" => "form_send-resources");
						echo form_open('send-resources', $attributes);
						?>

							<div class="form-group" align="center">
								<label for="gold"><span class="glyphicon glyphicon-queen"></span> Gold</label>
								<input type="text" class="form-control" id="gold" maxlength="8" style="width:50%">
								<label for="food"><span class="glyphicon glyphicon-grain"></span> Food</label>
								<input type="text" class="form-control" id="food" maxlength="8" style="width:50%">
								<label for="wood"><span class="glyphicon glyphicon-tree-conifer"></span> Wood</label>
								<input type="text" class="form-control" id="wood" maxlength="8" style="width:50%">
							</div>
							<div class="form-group" align="center">
								<button type="submit" class="btn btn-default">Send</button>
							</div>

						<?php
						echo form_close();
						?>

					</div>
				</div>
			</div>
		</div>
		
		<div class="modal fade" id="sendmessagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h3 class="modal-title" id="myModalLabel" align="center">Send Message</h3>
					</div>
					<div class="modal-body">

						<?php
						$attributes = array("role" => "form", "id" => "form_send-message");
						echo form_open('send-message-player-to-mod', $attributes);
						?>

							<div class="form-group">
								<label for="subject">Subject:</label>
								<input type="text" class="form-control" id="subject" maxlength="30" style="text-align:left">
								<label for="content">Content:</label>
								<textarea class="form-control" rows="5" id="content" maxlength="200"></textarea>
							</div>
							<div class="form-group" align="center">
								<button type="submit" class="btn btn-default">Send</button>
							</div>

						<?php
						echo form_close();
						?>

					</div>
				</div>
			</div>
		</div>


		<script>
			$(document).ready(function()
			{
				var goldDelay = <?php echo $productionIncrement['goldinc']; ?>;
				var foodDelay = <?php echo $productionIncrement['foodinc']; ?>;
				var woodDelay = <?php echo $productionIncrement['woodinc']; ?>;
				var nowGold, nowFood, nowWood, beforeGold = new Date(), beforeFood = new Date(), beforeWood = new Date();

				setInterval(function () {
					nowGold = new Date();
					var elapsedTime = (nowGold.getTime() - beforeGold.getTime());
					$('#goldinc').html(function (i, val) {
						var value;
						if (elapsedTime > goldDelay)
							value = Math.floor(elapsedTime / goldDelay);
						else
							value = 1;

						beforeGold = new Date();
						return (parseInt(val) + value).toString();
					})
				}, goldDelay);

				setInterval(function () {
					nowFood = new Date();
					var elapsedTime = (nowFood.getTime() - beforeFood.getTime());
					$('#foodinc').html(function (i, val) {
						var value;
						if (elapsedTime > foodDelay)
							value = Math.floor(elapsedTime / foodDelay);
						else
							value = 1;

						beforeFood = new Date();
						return (parseInt(val) + value).toString();
					})
				}, foodDelay);

				setInterval(function () {
					nowWood = new Date();
					var elapsedTime = (nowWood.getTime() - beforeWood.getTime());
					$('#woodinc').html(function (i, val) {
						var value;
						if (elapsedTime > woodDelay)
							value = Math.floor(elapsedTime / woodDelay);
						else
							value = 1;

						beforeWood = new Date();
						return (parseInt(val) + value).toString();
					})
				}, woodDelay);


				var formSendAttack = $('#form_send-attack');
				var formSendReinforcement = $('#form_send-reinforcement');
				var formSendResources = $('#form_send-resources');
				var	formSendMessage = $('#form_send-message');

				formSendAttack.submit(function(event)
				{
					removeMessages();

					var sword = $.trim($('#sword').val());
					var spear = $.trim($('#spear').val());
					var cavalry = $.trim($('#cavalry').val());
					var bow = $.trim($('#bow').val());

					if (!sword)
						sword = 0;
					if (!spear)
						spear = 0;
					if (!cavalry)
						cavalry = 0;
					if (!bow)
						bow = 0;

					if (sword == 0 && spear == 0 && cavalry == 0 && bow == 0)
					{
						displayMessage("Fill the content.", "alert-danger", formSendAttack);
						return false;
					}

					var form_data = {
						defender: '<?php echo $username; ?>',
						sword: sword,
						spear: spear,
						cavalry: cavalry,
						bow: bow,
						isAjaxCalling: true
					};

					$.ajax({
						url: '<?php echo site_url('send-attack'); ?>',
						type: 'POST',
						data: form_data,
						success: function(msg)
						{
							if (msg == -1)
							{
								$('#sword').val("");
								$('#spear').val("");
								$('#cavalry').val("");
								$('#bow').val("");
								displayMessage("Attack sent. Check reports.", "alert-success", formSendAttack);
							}
							else if(msg != false && msg != -2)
								displayMessage("Next attack in " + msg + " seconds.", "alert-danger", formSendAttack);
							else
								displayMessage("Can't send attack.", "alert-danger", formSendAttack);
						}
					});

					return false;
				});

				formSendReinforcement.submit(function(event)
				{
					removeMessages();

					var sword = $.trim($('#r_sword').val());
					var spear = $.trim($('#r_spear').val());
					var cavalry = $.trim($('#r_cavalry').val());
					var bow = $.trim($('#r_bow').val());

					if (!sword)
						sword = 0;
					if (!spear)
						spear = 0;
					if (!cavalry)
						cavalry = 0;
					if (!bow)
						bow = 0;

					if (sword == 0 && spear == 0 && cavalry == 0 && bow == 0)
					{
						displayMessage("Fill the content.", "alert-danger", formSendReinforcement);
						return false;
					}

					var form_data = {
						receiver: '<?php echo $username; ?>',
						sword: sword,
						spear: spear,
						cavalry: cavalry,
						bow: bow,
						isAjaxCalling: true
					};

					$.ajax({
						url: '<?php echo site_url('send-reinforcement'); ?>',
						type: 'POST',
						data: form_data,
						success: function(msg) {
							if (msg == true)
							{
								$('#r_sword').val("");
								$('#r_spear').val("");
								$('#r_cavalry').val("");
								$('#r_bow').val("");
								displayMessage("Reinforcement sent.", "alert-success", formSendReinforcement);
							}
							else
								displayMessage("Can't send reinforcement.", "alert-danger", formSendReinforcement);
						}
					});

					return false;
				});

				formSendResources.submit(function(event)
				{
					removeMessages();

					var gold = $.trim($('#gold').val()),
						food = $.trim($('#food').val()),
						wood = $.trim($('#wood').val());

					if (!gold)
						gold = 0;
					if (!food)
						food = 0;
					if (!wood)
						wood = 0;

					if (gold == 0 && food == 0 && wood == 0)
					{
						displayMessage("Fill the content.", "alert-danger", formSendResources);
						return false;
					}

					var form_data = {
						receiver: '<?php echo $username; ?>',
						gold: gold,
						food: food,
						wood: wood,
						isAjaxCalling: true
					};

					$.ajax({
						url: '<?php echo site_url('send-resources'); ?>',
						type: 'POST',
						data: form_data,
						success: function(msg) {
							if (msg == true)
							{
								$('#gold').val("");
								$('#food').val("");
								$('#wood').val("");
								displayMessage("Resources sent.", "alert-success", formSendResources);
							}
							else
								displayMessage("Can't send resources.", "alert-danger", formSendResources);
						}
					});

					return false;
				});

				formSendMessage.submit(function(event)
				{
					removeMessages();

					var subject = $.trim($('#subject').val()),
						content = $.trim($('#content').val());

					if (content.length == 0)
					{
						displayMessage("Fill the content.", "alert-danger", formSendMessage);
						return false;
					}
					else
					{
						if (subject.length == 0)
							subject = 'No Subject';


						/* AJAX CALL */
						var form_data = {
							receiver: '<?php echo $username; ?>',
							subject: subject,
							content: content,
							isAjaxCalling: true
						};
						$.ajax({
							url: '<?php echo site_url('send-message'); ?>',
							type: 'POST',
							data: form_data,
							success: function(msg) {
								if (msg == true)
								{
									$('#subject').val("");
									$('#content').val("");
									displayMessage("Message sent.", "alert-success", formSendMessage);
								}

								else
									displayMessage("Message not sent.", "alert-danger", formSendMessage);
							}
						});
						return false;
					}
				});

				function displayMessage(msg, alert, field)
				{
					var t1 = "<div id=\"msgAlert\" class=\"alert " + alert + " fade in flash_message\">";
					var t2 = "<strong>" + msg + "</strong>";
					var t3 = "</div>";

					field.before(t1 + t2 + t3);
				}

				function removeMessages()
				{
					$('#msgAlert').remove();
				}

				$('#sendattack').on('click', function() {removeMessages();});
				$('#sendresources').on('click', function() {removeMessages();});
				$('#sendmessage').on('click', function() {removeMessages();});
			});
		</script>

	</body>
	
</html>