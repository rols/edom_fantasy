<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Edom Fantasy - AdminPlayer</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
		
		<link rel="icon" href="assets/images/bgr/favicon.ico" type="image" sizes="16x16">
		
		<style>
			body {
				background-image:url("assets/images/bgr/nationbgr.jpg");
			}
			@font-face {font-family: "Vivaldi Italic V1";
				src: url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.eot"); /* IE9*/
				src: url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.eot?#iefix") format("embedded-opentype"), /* IE6-IE8 */
				url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.woff2") format("woff2"), /* chrome、firefox */
				url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.woff") format("woff"), /* chrome、firefox */
				url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.ttf") format("truetype"), /* chrome、firefox、opera、Safari, Android, iOS 4.2+*/
				url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.svg#Vivaldi Italic V1") format("svg"); /* iOS 4.1- */
			}
			textarea {
				resize: none;
			}
			p.headings {
				font-family: Vivaldi Italic V1;
				font-weight: bold;
				font-size: 30px;
				color: white;
			}
			p.glyph {
				font-family: tahoma;
				font-weight: normal;
				font-size: 25px;
				color: white;
			}
			thead {
				font-weight: bold;
				color: white;
				font-size: 15px;
			}
			.navbar-brand {
				font-family: Vivaldi Italic V1;
				font-weight: bold;
				font-size: 40px;
				color: white;
			}
			.panel-transparent {
				background: none;
			}
			.panel-heading {
				font-family: Vivaldi Italic V1;
				font-weight: bold;
				color: white;
				font-size: 30px;
			}
		</style>
	</head>
	
	<body>
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="about">Edom Fantasy</a>
				</div>
				<ul class="nav navbar-nav">
					<li><a href="admin"><span class="glyphicon glyphicon-home"></span> Home</a></li>
					<li><a href="messages-admin">Messages<?php if ($numOfMessages != 0) echo " (".$numOfMessages.")"; ?></a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="logout"><span class="glyphicon glyphicon-log-out"></span> Log out</a></li>
				</ul>
			</div>
		</nav>
		
		
		<div align="center">
			<div>
				<img src="<?php echo $picture; ?>" class="img-thumbnail" width="450px" alt="Nation">
			</div>
			
			<div style="padding-top:1%">
				<p class="headings">Username: <?php echo $username; ?></p>
				<p class="headings">Nation: <?php echo $nation; ?></p>
				<p class="headings">Edom Points: <?php echo $edompoints; ?></p>
				<p class="glyph"><span class="glyphicon glyphicon-queen" style="font-size:22px"></span> <?php echo $currentResources['gold']; ?> - <?php echo $currentProduction['gold'] ?>/h</p>
				<p class="glyph"><span class="glyphicon glyphicon-grain"></span> <?php echo $currentResources['food']; ?> - <?php echo $currentProduction['food'] ?>/h</p>
				<p class="glyph"><span class="glyphicon glyphicon-tree-conifer"></span> <?php echo $currentResources['wood']; ?> - <?php echo $currentProduction['wood'] ?>/h</p>

		<!--Player's army-->
				<p class="headings"><a href="#armycollapse" data-toggle="collapse" style="color:white">Army</a></p>
				<div id="armycollapse" class="collapse">
					<div class="panel panel-transparent">
						<div class="container">
							<table class="table" style="width:40%" align="center">
								<thead>
									<tr>
										<th class="col-md-2"><p align="center">Unit</p></th>
										<th class="col-md-2"><p align="center">Count</p></th>
									</tr>
								</thead>
								<tbody>

								<?php
								foreach ($armies as $army)
								{ ?>

									<tr class="active">
										<td><p align="center"><?php echo $army['name']; ?></p></td>
										<td><p align="center"><?php echo $army['count'] ?></p></td>
									</tr>

								<?php
								} ?>

								</tbody>
							</table>
						</div>
					</div>
					
					<div class="panel panel-transparent">
						<div class="panel-heading" align="center">Sent Reinforcement</div>
						<div class="container">
							<table class="table" style="width:40%" align="center">
								<thead>
									<tr>
										<th class="col-md-3"><p align="center">Player</p></th>
										<th class="col-md-3"><p align="center">Unit</p></th>
										<th class="col-md-2"><p align="center">Count</p></th>
									</tr>
								</thead>
								<tbody>

								<?php
								foreach ($sentReinforcements as $reinforcement)
								{ ?>

									<tr class="active" id="<?php echo $reinforcement['id'] ?>">
										<td><p align="center" ><?php echo $reinforcement['player']; ?></p></td>
										<td><p align="center"><?php echo $reinforcement['type']; ?></p></td>
										<td><p align="center"><?php echo $reinforcement['count']; ?></p></td>
									</tr>

								<?php
								} ?>

								</tbody>
							</table>
						</div>
					</div>
					
					<div class="panel panel-transparent">
						<div class="panel-heading" align="center">Received Reinforcement</div>
						<div class="container">
							<table class="table" style="width:40%" align="center">
								<thead>
									<tr>
										<th class="col-md-3"><p align="center">Player</p></th>
										<th class="col-md-3"><p align="center">Unit</p></th>
										<th class="col-md-2"><p align="center">Count</p></th>
									</tr>
								</thead>
								<tbody>

								<?php
								foreach ($receivedReinforcements as $reinforcement)
								{ ?>

									<tr class="active" id="<?php echo $reinforcement['id'] ?>">
										<td><p align="center"><?php echo $reinforcement['player']; ?></p></td>
										<td><p align="center"><?php echo $reinforcement['type'] ?></p></td>
										<td><p align="center"><?php echo $reinforcement['count']; ?></p></td>
									</tr>

								<?php
								} ?>

								</tbody>
							</table>
						</div>
					</div>
				</div>
		<!--Player's army-->
				
		<!--Player's buildings-->
				<p class="headings"><a href="#buildingscollapse" data-toggle="collapse" style="color:white">Buildings</a></p>
				<div id="buildingscollapse" class="collapse">
					<div class="panel panel-transparent">
						<div class="container">
							<table class="table" style="width:40%" align="center">
								<thead>
									<tr>
										<th class="col-md-2"><p align="center">Building</p></th>
										<th class="col-md-1"><p align="center">Level</p></th>
									</tr>
								</thead>
								<tbody>
									<tr class="active">
										<td><p align="center">Gold Farm</p></td>
										<td><p align="center"><?php echo $farms['gold1']; ?></p></td>
									</tr>
									<tr class="active">
										<td><p align="center">Gold Farm</p></td>
										<td><p align="center"><?php echo $farms['gold2']; ?></p></td>
									</tr>
									<tr class="active">
										<td><p align="center">Food Farm</p></td>
										<td><p align="center"><?php echo $farms['food1']; ?></p></td>
									</tr>
									<tr class="active">
										<td><p align="center">Food Farm</p></td>
										<td><p align="center"><?php echo $farms['food2']; ?></p></td>
									</tr>
									<tr class="active">
										<td><p align="center">Wood Farm</p></td>
										<td><p align="center"><?php echo $farms['wood1']; ?></p></td>
									</tr>
									<tr class="active">
										<td><p align="center">Wood Farm</p></td>
										<td><p align="center"><?php echo $farms['wood2']; ?></p></td>
									</tr>
									<tr class="active">
										<td><p align="center"><?php echo $buildings['barracks']; ?></p></td>
										<td><p align="center"><?php echo $buildings['barracksLevel']; ?></p></td>
									</tr>
									<tr class="active">
										<td><p align="center"><?php echo $buildings['archery']; ?></p></td>
										<td><p align="center"><?php echo $buildings['archeryLevel']; ?></p></td>
									</tr>
									<tr class="active">
										<td><p align="center"><?php echo $buildings['stable']; ?></p></td>
										<td><p align="center"><?php echo $buildings['stableLevel']; ?></p></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
		<!--Player's buildings-->
				
		<!--Player's reports-->
				<p class="headings"><a href="#reportscollapse" data-toggle="collapse" style="color:white">Reports</a></p>
				<div id="reportscollapse" class="collapse">
					<div class="panel panel-transparent">
						<div class="container">
							<table class="table" style="width:50%" align="center">
								<thead>
									<tr>
										<th class="col-md-2"><p align="center">Type</p></th>
										<th class="col-md-2"><p align="center">Time</p></th>
										<th class="col-md-1"><p align="center">Report</p></th>
										<th class="col-md-1"><p align="center">Delete</p></th>
									</tr>
								</thead>
								<tbody>

								<?php
								foreach ($reports as $report)
								{ ?>

									<tr id="<?php echo $report['idrep']; ?>" class="active">
										<td><p align="center"><?php echo $report['type']; ?></p></td>
										<td><p align="center"><?php echo $report['time']; ?></p></td>
										<td><p align="center"><a class="report" href="#" data-toggle="modal" data-target="#reportmodal"><span class="glyphicon glyphicon-envelope" style="color:black"></span></a></p></td>
										<td><p align="center"><a href="#" class="deletereport"><span class="glyphicon glyphicon-trash" style="color:black"></span></a></p></td>
									</tr>

								<?php
								} ?>

								</tbody>
							</table>
						</div>
					</div>
				</div>
		<!--Player's reports-->
				
		<!--Player's messages-->
				<p class="headings"><a href="#messagescollapse" data-toggle="collapse" style="color:white">Messages</a></p>
				<div id="messagescollapse" class="collapse">
					<div class="panel panel-transparent">
						<div class="container">
							<table class="table" style="width: 50%" align="center">
								<thead>
									<tr>
										<th class="col-md-2"><p align="center">From</p></th>
										<th class="col-md-2"><p align="center">To</p></th>
										<th class="col-md-2"><p align="center">Subject</p></th>
										<th class="col-md-1"><p align="center">Delete</p></th>
									</tr>
								</thead>
								<tbody>

								<?php
								foreach ($messages as $message)
								{ ?>

									<tr id="<?php echo $message['idmes']; ?>" class="active">
										<td><p align="center"><?php echo $message['sender']; ?></p></td>
										<td><p align="center"><?php echo $message['receiver']; ?></p></td>
										<td><a class="message" href="#" data-toggle="modal" data-target="#messagemodal" style="color:black"><p align="center"><b><?php echo $message['subject']; ?></b></p></a></td>
										<td><p align="center"><a href="#" class="deletemessage"> <span class="glyphicon glyphicon-trash" style="color:black"></span></a></p></td>
									</tr>

								<?php
								} ?>

								</tbody>
							</table>
						</div>
					</div>
				</div>
		<!--Player's messages-->

		<!--Change info-->
				<p class="headings"><a id="changeinfo" href="#" data-toggle="modal" data-target="#changeinfomodal" style="color:white">Change Info</a></p>
		<!--Change info-->
				
			</div>
		</div>
		<br>


		<div class="modal fade" id="reportmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog modal-md">
				<div class="modal-content">
					<div class="modal-header">
						<button id="closeRep" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h3 class="modal-title" id="report_type" align="center"></h3>
					</div>
					<div class="modal-body">
						<p id="report_text" align="center"></p>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="messagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-header">
						<button id="closeMsg" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h3 class="modal-title" id="subject_text" align="center"></h3>
					</div>
					<div class="modal-body">
						<p id="content_text" style="word-wrap:break-word" align="center"></p>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="changeinfomodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h3 class="modal-title" id="myModalLabel" align="center">Change Info</h3>
					</div>
					<div class="modal-body">

						<?php
						$attributes = array("role" => "form", "id" => "form_change-info");
						echo form_open('change-info', $attributes);
						?>

							<div class="form-group" align="center">
								<label for="resgold">Gold</label>
								<input type="text" class="form-control" id="resgold" maxlength="11" style="width:50%" placeholder="Set new...">
								<label for="resfood">Food</label>
								<input type="text" class="form-control" id="resfood" maxlength="11" style="width:50%" placeholder="Set new...">
								<label for="reswood">Wood</label>
								<input type="text" class="form-control" id="reswood" maxlength="11" style="width:50%" placeholder="Set new...">
								<hr>
								<label for="gold1level">Gold Farm 1 Level</label>
								<input type="text" class="form-control" id="gold1level" maxlength="1" style="width:50%" placeholder="Set new...">
								<label for="gold2level">Gold Farm 2 Level</label>
								<input type="text" class="form-control" id="gold2level" maxlength="1" style="width:50%" placeholder="Set new...">
								<label for="food1level">Food Farm 1 Level</label>
								<input type="text" class="form-control" id="food1level" maxlength="1" style="width:50%" placeholder="Set new...">
								<label for="food2level">Food Farm 2 Level</label>
								<input type="text" class="form-control" id="food2level" maxlength="1" style="width:50%" placeholder="Set new...">
								<label for="wood1level">Wood Farm 1 Level</label>
								<input type="text" class="form-control" id="wood1level" maxlength="1" style="width:50%" placeholder="Set new...">
								<label for="wood2level">Wood Farm 2 Level</label>
								<input type="text" class="form-control" id="wood2level" maxlength="1" style="width:50%" placeholder="Set new...">
								<hr>
								<label for="barrackslevel">Barracks Level</label>
								<input type="text" class="form-control" id="barrackslevel" maxlength="1" style="width:50%" placeholder="Set new...">
								<label for="stablelevel">Stable Level</label>
								<input type="text" class="form-control" id="stablelevel" maxlength="1" style="width:50%" placeholder="Set new...">
								<label for="archerylevel">Archery Level</label>
								<input type="text" class="form-control" id="archerylevel" maxlength="1" style="width:50%" placeholder="Set new...">
								<hr>
								<label for="sword">Sword</label>
								<input type="text" class="form-control" id="sword" maxlength="11" style="width:50%" placeholder="Set new...">
								<label for="spear">Spear</label>
								<input type="text" class="form-control" id="spear" maxlength="11" style="width:50%" placeholder="Set new...">
								<label for="cavalry">Cavalry</label>
								<input type="text" class="form-control" id="cavalry" maxlength="11" style="width:50%" placeholder="Set new...">
								<label for="bow">Bow</label>
								<input type="text" class="form-control" id="bow" maxlength="11" style="width:50%" placeholder="Set new...">
								<hr>
							</div>
							<div class="form-group" align="center">
								<button type="submit" class="btn btn-default">Change</button>
							</div>

						<?php
						echo form_close();
						?>

					</div>
				</div>
			</div>
		</div>

		<script>
			$(document).ready(function()
			{
				var	formChangeInfo = $('#form_change-info');
				formChangeInfo.submit(function(event)
				{
					event.preventDefault();
					removeMessages();

					var resgold = $.trim($('#resgold').val());
					var resfood = $.trim($('#resfood').val());
					var reswood = $.trim($('#reswood').val());
					var gold1level = $.trim($('#gold1level').val());
					var gold2level = $.trim($('#gold2level').val());
					var food1level = $.trim($('#food1level').val());
					var food2level = $.trim($('#food2level').val());
					var wood1level = $.trim($('#wood1level').val());
					var wood2level = $.trim($('#wood2level').val());
					var barrackslevel = $.trim($('#barrackslevel').val());
					var stablelevel = $.trim($('#stablelevel').val());
					var archerylevel = $.trim($('#archerylevel').val());
					var sword = $.trim($('#sword').val());
					var spear = $.trim($('#spear').val());
					var cavalry = $.trim($('#cavalry').val());
					var bow = $.trim($('#bow').val());

					var form_data = {
						username: '<?php echo $username; ?>',
						resgold: resgold,
						resfood: resfood,
						reswood: reswood,
						gold1level: gold1level,
						gold2level: gold2level,
						food1level: food1level,
						food2level: food2level,
						wood1level: wood1level,
						wood2level: wood2level,
						barrackslevel: barrackslevel,
						stablelevel: stablelevel,
						archerylevel: archerylevel,
						sword: sword,
						spear: spear,
						cavalry: cavalry,
						bow: bow,
						isAjaxCalling: true
					};
					$.ajax({
						url: '<?php echo site_url('change-info'); ?>',
						type: 'POST',
						data: form_data,
						success: function(msg)
						{
							if (msg == true)
							{
								$('#resgold').val("");
								$('#resfood').val("");
								$('#reswood').val("");
								$('#gold1level').val("");
								$('#gold2level').val("");
								$('#food1level').val("");
								$('#food2level').val("");
								$('#wood1level').val("");
								$('#wood2level').val("");
								$('#barrackslevel').val("");
								$('#stablelevel').val("");
								$('#archerylevel').val("");
								$('#sword').val("");
								$('#spear').val("");
								$('#cavalry').val("");
								$('#bow').val("");
								displayMessage("Info changed. ", "alert-success", formChangeInfo);
							}
							else
								displayMessage("Can't change info.", "alert-danger", formChangeInfo);
						}
					});

					return false;
				});

				$('.report').on('click', function(){
					var idrep = $(this).closest('tr').attr('id');

					var form_data = {
						idrep: idrep,
						isAjaxCalling: true
					};
					$.ajax({
						url: '<?php echo site_url('read-report-mod'); ?>',
						type: 'POST',
						data: form_data,
						success: function(rep) {
							if (rep)
							{
								$('#report_type').text(rep.type);
								$('#report_text').html(rep.content);
							}
						},
						dataType:"json"
					});
				});

				$('#closeRep').on('click', function()
				{
					$('#report_type').text("");
					$('#report_text').text("");
				});

				$('.message').on('click', function()
				{
					var idmes = $(this).closest('tr').attr('id');

					var form_data = {
						idmes: idmes,
						isAjaxCalling: true
					};
					$.ajax({
						url: '<?php echo site_url('read-message-mod'); ?>',
						type: 'POST',
						data: form_data,
						success: function(msg) {
							if (msg)
							{
								$('#subject_text').html(msg.subject);
								$('#content_text').html(msg.content);
							}
						},
						dataType:"json"
					});
				});

				$('#closeMsg').on('click', function()
				{
					$('#subject_text').text("");
					$('#content_text').text("");
				});

				$('.deletereport').on('click', function (event)
				{
					event.preventDefault();

					var idrep = $(this).closest('tr').attr('id');
					$(this).closest('tr').remove();

					var form_data = {
						idrep: idrep,
						isAjaxCalling: true
					};
					$.ajax({
						url: '<?php echo site_url('delete-report-admin'); ?>',
						type: 'POST',
						data: form_data
					});
				});

				$('.deletemessage').on('click', function (event)
				{
					event.preventDefault();

					var idmes = $(this).closest('tr').attr('id');
					$(this).closest('tr').remove();

					var form_data = {
						idmes: idmes,
						isAjaxCalling: true
					};
					$.ajax({
						url: '<?php echo site_url('delete-message-admin'); ?>',
						type: 'POST',
						data: form_data
					});
				});

				function displayMessage(msg, alert, field)
				{
					var t1 = "<div id=\"msgAlert\" class=\"alert " + alert + " fade in flash_message\">";
					var t2 = "<strong>" + msg + "</strong>";
					var t3 = "</div>";

					field.before(t1 + t2 + t3);
				}

				function removeMessages()
				{
					$('#msgAlert').remove();
				}

				$('#changeinfo').on('click', function() {removeMessages();});
			});
		</script>
		
	</body>
	
</html>