<!DOCTYPE html>

<html lang="en">
    <head>
        <title>Edom Fantasy - Home</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

        <link rel="icon" href="assets/images/bgr/favicon.ico" type="image" sizes="16x16">

        <style>
            body {
                background-image:url("assets/images/bgr/nationbgr.jpg");
            }
            @font-face {font-family: "Vivaldi Italic V1";
                src: url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.eot"); /* IE9*/
                src: url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.eot?#iefix") format("embedded-opentype"), /* IE6-IE8 */
                url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.woff2") format("woff2"), /* chrome、firefox */
                url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.woff") format("woff"), /* chrome、firefox */
                url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.ttf") format("truetype"), /* chrome、firefox、opera、Safari, Android, iOS 4.2+*/
                url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.svg#Vivaldi Italic V1") format("svg"); /* iOS 4.1- */
            }
            h3 {
                font-weight: bold;
                font-size: 22px;
                color: white;
            }
            .navbar-brand {
                font-family: Vivaldi Italic V1;
                font-weight: bold;
                font-size: 40px;
                color: white;
            }
            .about-text {
                color: white;
                font-size: 16px;
            }
        </style>
    </head>

    <body>
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="home">Edom Fantasy</a>
                </div>
            </div>
        </nav>

        <div class="container">
            <div class="row" style="width: 80%; text-align: justify; margin: auto">
                <p class="about-text">
                    Edom Fantasy is a browser game where you can control one of four nation and interact with other players
                    in the world of Edom. Grow your economy, test your diplomatic skills or make an army and attack other
                    players, steal their resources and much more. <br>
                    There are three types of resources: Gold, Food and Wood. You can improve your economy and resource
                    production, which is being measured by hour, by upgrading economy buildings - farms. You can create
                    soldiers in military buildings, but first you must upgrade them. Be careful, not all soldiers are good
                    for attack and defence, there are points which describe use of soldier. <br>
                    Maximum level of building is 5. Each level of farm gain more production for 50%, and each level of military
                    building gain more slots for army. Level 5 gives you unlimited number of slots. There are three types
                    of military buildings - the one that can creates soldiers which carry swords and spears, the one that
                    creates archers and one that creates cavalry. <br>
                    You can receive Edom Points for participation in the game. For each level of building or win in the battle,
                    you gain 100 points and for each soldier created you receive 1 point. <br>
                    For each interaction with player - attack or defence, sending reinforcement or resources you will receive
                    a report. <br>
                    There are four kinds of nations - Men, Elves, Dwarves, Orcs. Each nation has its advantages and flaws.
                </p>
                <br>
                <div>
                    <h3>Men</h3>
                    <img src="assets/images/bgr/bard.jpg" class="img-thumbnail" width="450px" alt="Nation" style="float:right">
                    <p class="about-text">
                        It is recommended for not experienced users to choose men. They are as good at offence as they are
                        at defence. They are good at producing food. They can send one attack in 2 minutes. <br>
                        Starting resources: Gold: 10000 Food: 10000 Wood: 10000 <br>
                        Starting production: Gold: 3000/h Food: 3600/h Wood: 3000/h <br>
                        Each building upgrade cost 30% more than previous. <br>
                        Each building upgrade gain 50% more slots. <br>
                        Plunder percentage: 25% <br>
                        Swordsman: Offence: 60 Defence: 20 <br>
                        Spearman: Offence: 20 Defence: 70 <br>
                        Horseman: Offence: 80 Defence: 40 <br>
                        Archer: Offence: 40 Defence: 30 <br>
                    </p>
                    <h3>Elves</h3>
                    <img src="assets/images/bgr/thranduil.jpg" class="img-thumbnail" width="450px" alt="Nation" style="float:right">
                    <p class="about-text">
                        They have strongest but most expensive army in the game. Powerful archers and wood production is
                        their advantage. They can send one attack in 2 minutes.<br>
                        Starting resources: Gold: 10000 Food: 10000 Wood: 10000 <br>
                        Starting production: Gold: 3000/h Food: 3000/h Wood: 3600/h <br>
                        Each building upgrade cost 35% more than previous. <br>
                        Each building upgrade gain 40% more slots. <br>
                        Plunder percentage: 20% <br>
                        Elven Swordsman: Offence: 65 Defence: 20 <br>
                        Elven Phalanx: Offence: 15 Defence: 80 <br>
                        Elven Rider: Offence: 60 Defence: 45 <br>
                        Elven Archer: Offence: 80 Defence: 35 <br>
                    </p>
                    <h3>Dwarves</h3>
                    <img src="assets/images/bgr/thorin.jpg" class="img-thumbnail" width="450px" alt="Nation" style="float:right">
                    <p class="about-text">
                        They are very good at defence, but bad at offence. They are best at producing gold in the world of Edom.
                        You can send one attack in 3 minutes. <br>
                        Starting resources: Gold: 15000 Food: 15000 Wood: 15000 <br>
                        Starting production: Gold: 4200/h Food: 3000/h Wood: 3000/h <br>
                        Each building upgrade cost 40% more than previous. <br>
                        Each building upgrade gain 40% more slots. <br>
                        Plunder percentage: 10% <br>
                        Dwarven Infantry: Offence: 35 Defence: 50 <br>
                        Dwarven Phalanx: Offence: 15 Defence: 100 <br>
                        Chariot: Offence: 55 Defence: 30 <br>
                        Dwarven Axle: Offence: 25 Defence: 60 <br>
                    </p>
                    <h3>Orcs</h3>
                    <img src="assets/images/bgr/azog.png" class="img-thumbnail" width="450px" alt="Nation" style="float:right">
                    <p class="about-text">
                        Weak but very cheap army. Powerful cavalry. You can send one attack in 1 minute. <br>
                        Starting resources: Gold: 8000 Food: 8000 Wood: 8000 <br>
                        Starting production: Gold: 2800/h Food: 2800/h Wood: 2800/h <br>
                        Each building upgrade cost 40% more than previous. <br>
                        Each building upgrade gain 70% more slots. <br>
                        Plunder percentage: 30% <br>
                        Orc Swordsman: Offence: 45 Defence: 5 <br>
                        Orc Spearman: Offence: 5 Defence: 50 <br>
                        Troll: Offence: 100 Defence: 50 <br>
                        Orc Archer: Offence: 40 Defence: 30 <br>
                    </p>
                </div>
            </div>
        </div>
    </body>
</html>
