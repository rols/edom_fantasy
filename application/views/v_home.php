<!DOCTYPE html>

<html lang="en">
	<head>
		<title>Edom Fantasy - Home</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
		
		<link rel="icon" href="assets/images/bgr/favicon.ico" type="image" sizes="16x16">
		
		<style>
			body {
				background-image:url("assets/images/bgr/nationbgr.jpg");
			}
			@font-face {font-family: "Vivaldi Italic V1";
				src: url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.eot"); /* IE9*/
				src: url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.eot?#iefix") format("embedded-opentype"), /* IE6-IE8 */
				url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.woff2") format("woff2"), /* chrome、firefox */
				url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.woff") format("woff"), /* chrome、firefox */
				url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.ttf") format("truetype"), /* chrome、firefox、opera、Safari, Android, iOS 4.2+*/
				url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.svg#Vivaldi Italic V1") format("svg"); /* iOS 4.1- */
			}
			h3 {
				font-family: Vivaldi Italic V1;
				font-weight: bold;
				font-size: 40px;
				color: white;
			}
			.navbar-brand {
				font-family: Vivaldi Italic V1;
				font-weight: bold;
				font-size: 40px;
				color: white;
			}
		</style>
	</head>
	
	<body>
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="about">Edom Fantasy</a>
				</div>
				<ul class="nav navbar-nav">
					<li class="active"><a href="home"><span class="glyphicon glyphicon-home"></span> Home</a></li>
					<li><a href="players">Players</a></li>
					<li><a href="reports">Reports<?php if ($numOfReports != 0) echo " (".$numOfReports.")" ?></a></li>
					<li><a href="messages">Messages<?php if ($numOfMessages != 0) echo " (".$numOfMessages.")" ?></a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><a><span class="glyphicon glyphicon-queen"></span><span id="goldinc"> <?php echo $currentResources['gold']; ?></span></a></li>
					<li><a><span class="glyphicon glyphicon-grain"></span><span id="foodinc"> <?php echo $currentResources['food']; ?></span></a></li>
					<li><a><span class="glyphicon glyphicon-tree-conifer"></span><span id="woodinc"> <?php echo $currentResources['wood']; ?></span></a></li>
					<li><a></a></li> <!--Empty space-->
					<li><a href="logout"><span class="glyphicon glyphicon-log-out"></span> Log out</a></li>
				</ul>
			</div>
		</nav>
		
		
		<div class="container">
			<div class="row">
				<div class="col-sm-8" style="vertical-align:center">
					<img class="img-responsive" src="<?php echo $picture; ?>">
				</div>
				<div class="col-sm-4" style="color:white; font-weight:bold; font-size:30px; font-family:vivaldi">
					<div class="well" style="background-color:rgba(0, 0, 0, 0); border:0px">
						<p align="center">Gold Production:</p>
						<p align="center"><?php echo $currentProduction['gold']; ?>/h</p>
					</div>
					<div class="well" style="background-color:rgba(0, 0, 0, 0); border:0px">
						<p align="center">Food Production:</p>
						<p align="center"><?php echo $currentProduction['food']; ?>/h</p>
					</div>
					<div class="well" style="background-color:rgba(0, 0, 0, 0); border:0px">
						<p align="center">Wood Production:</p>
						<p align="center"><?php echo $currentProduction['wood']; ?>/h</p>
					</div>
				</div>
			</div>
			<hr>
		</div>

		<div class="container text-center">    
			<h3>Military</h3>
			<br>
			<div class="row" style="padding-left:5%">
				<div class="col-sm-3">
					<a href="army">
						<img src="<?php echo $armyPicture; ?>" class="img-responsive" style="width:97%" alt="Army">
					</a>
					<p style="color:white">Your Army</p>    
				</div>
				<div class="col-sm-3" align="center">
					<a href="building?type=1">
						<img src="assets/images/struct/military.png" class="img-responsive" style="width:100%" alt="Military">
					</a>
					<p style="color:white"><?php echo $barracks?></p>
				</div>
				<div class="col-sm-3" align="center">
					<a href="building?type=2">
						<img src="assets/images/struct/archery.png" class="img-responsive" style="width:70.5%" alt="Archery">
					</a>
					<p style="color:white"><?php echo $archery?></p>
				</div>
				<div class="col-sm-3" align="center">
					<a href="building?type=3">
						<img src="assets/images/struct/cavalry.png" class="img-responsive" style="width:64%" alt="Cavalry">
					</a>
					<p style="color:white"><?php echo $stable?></p>
				</div>  
			</div>
			<hr>
		</div>

		<div class="container text-center">    
			<h3>Economy</h3>
			<br>
			<div class="row">
				<div class="col-sm-2">
					<a href="farm?type=gold1">
						<img src="assets/images/struct/gold.jpg" class="img-responsive" style="width:100%" alt="Gold">
					</a>
					<p style="color:white">Gold Farm</p>
				</div>
				<div class="col-sm-2"> 
					<a href="farm?type=gold2">
						<img src="assets/images/struct/gold.jpg" class="img-responsive" style="width:100%" alt="Gold">
					</a>
					<p style="color:white">Gold Farm</p>    
				</div>
				<div class="col-sm-2"> 
					<a href="farm?type=food1">
						<img src="assets/images/struct/grain.jpg" class="img-responsive" style="width:100%" alt="Food">
					</a>
					<p style="color:white">Food Farm</p>
				</div>
				<div class="col-sm-2"> 
					<a href="farm?type=food2">
						<img src="assets/images/struct/grain.jpg" class="img-responsive" style="width:100%" alt="Food">
					</a>
					<p style="color:white">Food Farm</p>
				</div> 
				<div class="col-sm-2"> 
					<a href="farm?type=wood1">
						<img src="assets/images/struct/wood.jpg" class="img-responsive" style="width:100%" alt="Wood">
					</a>
					<p style="color:white">Wood Farm</p>
				</div>     
				<div class="col-sm-2"> 
					<a href="farm?type=wood2">
						<img src="assets/images/struct/wood.jpg" class="img-responsive" style="width:100%" alt="Wood">
					</a>
					<p style="color:white">Wood Farm</p>
				</div> 
			</div>
		</div>
		<br>

		<script>
			$(document).ready(function()
			{
				var goldDelay = <?php echo $productionIncrement['goldinc']; ?>;
				var foodDelay = <?php echo $productionIncrement['foodinc']; ?>;
				var woodDelay = <?php echo $productionIncrement['woodinc']; ?>;
				var nowGold, nowFood, nowWood, beforeGold = new Date(), beforeFood = new Date(), beforeWood = new Date();

				setInterval(function () {
					nowGold = new Date();
					var elapsedTime = (nowGold.getTime() - beforeGold.getTime());
					$('#goldinc').html(function (i, val) {
						var value;
						if (elapsedTime > goldDelay)
							value = Math.floor(elapsedTime / goldDelay);
						else
							value = 1;

						beforeGold = new Date();
						return (parseInt(val) + value).toString();
					})
				}, goldDelay);

				setInterval(function () {
					nowFood = new Date();
					var elapsedTime = (nowFood.getTime() - beforeFood.getTime());
					$('#foodinc').html(function (i, val) {
						var value;
						if (elapsedTime > foodDelay)
							value = Math.floor(elapsedTime / foodDelay);
						else
							value = 1;

						beforeFood = new Date();
						return (parseInt(val) + value).toString();
					})
				}, foodDelay);

				setInterval(function () {
					nowWood = new Date();
					var elapsedTime = (nowWood.getTime() - beforeWood.getTime());
					$('#woodinc').html(function (i, val) {
						var value;
						if (elapsedTime > woodDelay)
							value = Math.floor(elapsedTime / woodDelay);
						else
							value = 1;

						beforeWood = new Date();
						return (parseInt(val) + value).toString();
					})
				}, woodDelay);
			});
		</script>
		
	</body>
	
</html>