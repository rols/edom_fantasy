<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Edom Fantasy - ModHome</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
		
		<link rel="icon" href="assets/images/bgr/favicon.ico" type="image" sizes="16x16">
		
		<style>
			body {
				background-image:url("assets/images/bgr/nationbgr.jpg");
			}
			@font-face {font-family: "Vivaldi Italic V1";
				src: url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.eot"); /* IE9*/
				src: url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.eot?#iefix") format("embedded-opentype"), /* IE6-IE8 */
				url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.woff2") format("woff2"), /* chrome、firefox */
				url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.woff") format("woff"), /* chrome、firefox */
				url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.ttf") format("truetype"), /* chrome、firefox、opera、Safari, Android, iOS 4.2+*/
				url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.svg#Vivaldi Italic V1") format("svg"); /* iOS 4.1- */
			}
			thead {
				font-weight: bold;
				color: white;
				font-size: 15px;
			}
			.navbar-brand {
				font-family: Vivaldi Italic V1;
				font-weight: bold;
				font-size: 40px;
				color: white;
			}
			.panel-transparent {
				background: none;
			}
			.panel-heading {
				font-family: Vivaldi Italic V1;
				font-weight: bold;
				color: white;
				font-size: 40px;
			}
		</style>
	</head>
	
	<body>
		<nav class="navbar navbar-inverse" style="margin-bottom:5px">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="about">Edom Fantasy</a>
				</div>
				<ul class="nav navbar-nav">
					<li class="active"><a href="moderator"><span class="glyphicon glyphicon-home"></span> Home</a></li>
					<li><a href="messages-mod">Messages<?php if ($numOfMessages != 0) echo " (".$numOfMessages.")"; ?></a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="logout"><span class="glyphicon glyphicon-log-out"></span> Log out</a></li>
				</ul>
			</div>
		</nav>
		
		
		<!--Open player profile with all informations-->
		<div class="container" align="right" style="padding-right:0px; margin-right:5px">
			<form role="search">
				<div class="form-group input-group col-sm-3">
					<input form="" id="player" type="text" class="form-control" placeholder="Search for player..." maxlength="20" onkeydown="searchPlayer_enter(event)">
					<span class="input-group-btn">
						<button id="playerbtn" class="btn btn-default" type="button" onClick="searchPlayer()">
							<span class="glyphicon glyphicon-search"></span>
						</button>
					</span>
				</div>
			</form>
		</div>
		
		<div class="panel panel-transparent">
			<div class="panel-heading" align="center">Players</div>
			<div class="container">
				<table class="table" style="width: 50%" align="center">
					<thead>
						<tr>
							<th class="col-md-1"><p align="center">Rank</p></th>
							<th class="col-md-3"><p align="center">Player</p></th>
							<th class="col-md-2"><p align="center">Nation</p></th>
							<th class="col-md-3"><p align="center">Edom Points</p></th>
						</tr>
					</thead>
					<tbody>

					<?php
					$rank = 1;
					foreach ($players as $player)
					{ ?>

						<tr class="active">
							<td><p align="center"><?php echo $rank++ . "."; ?></p></td>
							<td><a href="player-profile-mod?username=<?php echo $player['username'] ?>" style="color:black"><p align="center"><b><?php echo $player['username']; ?></b></p></a></td>
							<td><p align="center"><?php echo $player['nation']; ?></p></td>
							<td><p align="center"><?php echo $player['edompoints']; ?></p></td>
						</tr>

					<?php
					} ?>

					</tbody>
				</table>
			</div>
		</div>

		<script>
			function searchPlayer()
			{
				window.open("player-profile-mod?username=" + document.getElementById('player').value, "_self");
			}

			function searchPlayer_enter(event)
			{
				if (event.keyCode == 13)
					window.open("player-profile-mod?username=" + document.getElementById('player').value, "_self");
			}
		</script>
		
	</body>
	
</html>