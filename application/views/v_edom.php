<!DOCTYPE html>

<html lang="en">
	<head>
		<title>Edom Fantasy</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
		
		<link rel="icon" href="assets/images/bgr/favicon.ico" type="image" sizes="16x16">

		<style>
			body {
				background-image: url("assets/images/bgr/edombgr.png");
				background-repeat: no-repeat;
				background-size: 100% 100%;
			}
			@font-face {font-family: "Vivaldi Italic V1";
				src: url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.eot"); /* IE9*/
				src: url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.eot?#iefix") format("embedded-opentype"), /* IE6-IE8 */
				url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.woff2") format("woff2"), /* chrome、firefox */
				url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.woff") format("woff"), /* chrome、firefox */
				url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.ttf") format("truetype"), /* chrome、firefox、opera、Safari, Android, iOS 4.2+*/
				url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.svg#Vivaldi Italic V1") format("svg"); /* iOS 4.1- */
			}
			html {
				height: 100%
			}
			h1 {
				font-family: Vivaldi Italic V1;
				font-weight: bold;
				font-size: 50px;
				padding-top: 9%;
				color: white;
			}
			.heading {
				color: white;
			}
			.heading:link {
				text-decoration: none;
			}
			.heading:hover {
				color: white;
				text-decoration: none;
			}
			.heading:visited {
				color: white;
				text-decoration: none;
			}
			h3 {
				font-family: Vivaldi Italic V1;
				font-weight: bold;
				font-size: 40px;
				color: white;
			}
			div {
				padding-top: 3%
			}
			.modal-content {
				background-image:url("assets/images/bgr/regbgr.png");
				background-repeat: no-repeat;
				background-size: 100% 100%;
				width: 101%;
			}
			
		</style>
	</head>
	
	<body>
		<?php
		if (isset($alert_message) && isset($alert_class))
		{ ?>

			<div class="alert <?php echo $alert_class; ?> fade in flash_message">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<strong><?php echo $alert_message; ?></strong>
			</div>

		<?php
		} ?>

		<h1 align="center"><a class="heading" href="about">Edom Fantasy</a></h1>

		<div class="container">
			<div class="row">
				<div class="col-md-2 col-md-offset-5" align="center">

					<?php
						$attributes = array ("class" => "form-horizontal", "role" => "form");
						echo form_open('login', $attributes);
					?>

						<div class="form-group">
							<label for="loginUsername" class="sr-only">Username</label>
							<input type="text" name="username" class="form-control" id="loginUsername" placeholder="Username" maxlength="20" required>
						</div>
						<br>
						<div class="form-group">
							<label for="loginPassword" class="sr-only">Password</label>
							<input type="password" name="password" class="form-control" id="loginPassword" placeholder="Password" maxlength="20" required>
						</div>
						<br>
						<div class="form-group">
							<button type="submit" class="btn btn-default">Log in</button>
						</div>
						<br>
						<p>Need an account?</p>
						<a id="register" href="#" data-toggle="modal" data-target="#registermodal">Register now!</a>
						<br><br><br>
						<a id="forgotpassword" href="#" data-toggle="modal" data-target="#forgotmodal">Forgot password?</a>

					<?php
						echo form_close();
					?>

				</div>
			</div>
		</div>
		
		<div class="modal fade" id="registermodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h3 class="modal-title" id="myModalLabel" align="center">Register</h3>
					</div>
					<div class="modal-body">

						<?php
							$attributes = array("role" => "form");
							echo form_open('register', $attributes);
						?>

							<div class="form-group">
								<label for="registerUsername" class="sr-only">Username</label>
								<input type="text" name="username" title="Only letters and numbers or _ instead of space" class="form-control" id="registerUsername" placeholder="Username" maxlength="20" pattern="([a-zA-Z0-9]|_)*" required>
							</div>
							<div class="form-group">
								<label for="registerEmail" class="sr-only">Email</label>
								<input type="email" name="email" class="form-control" id="registerEmail" placeholder="Email" required>
							</div>
							<div class="form-group">
								<label for="registerPassword" class="sr-only">Password</label>
								<input type="password" name="password" class="form-control" id="registerPassword" placeholder="Password" maxlength="20" required>
							</div>
							<div class="form-group">
								<label for="registerRepeatPassword" class="sr-only">Repeat Password</label>
								<input type="password" name="repeatPassword" class="form-control" id="registerRepeatPassword" placeholder="Repeat Password" maxlength="20" required>
							</div>
							<p align="center">Choose your nation</p>
							<div align="center">
								<label class="radio-inline"><input type="radio" name="nation" value="Men" checked>Men</label>
								<label class="radio-inline"><input type="radio" name="nation" value="Elves">Elves</label>
								<label class="radio-inline"><input type="radio" name="nation" value="Dwarves">Dwarves</label>
								<label class="radio-inline"><input type="radio" name="nation" value="Orcs">Orcs</label>
							</div>
							<br><br>
							<div class="form-group" align="center">
								<button type="submit" class="btn btn-default">Register</button>
							</div>

						<?php
							echo form_close();
						?>

					</div>
				</div>
			</div>
		</div>
		
		<div class="modal fade" id="forgotmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h3 class="modal-title" id="myModalLabel" align="center">Recover Password</h3>
					</div>
					<div class="modal-body">

						<?php
						$attributes = array("role" => "form");
						echo form_open('forgot-password', $attributes);
						?>

							<div class="form-group">
								<label for="forgotEmail" class="sr-only">Email</label>
								<input type="email" name="email" class="form-control" id="forgotEmail" placeholder="Email" required>
							</div>
							<div class="form-group" align="center">
								<button type="submit" class="btn btn-default">Recover Password</button>
							</div>

						<?php
						echo form_close();
						?>

					</div>
				</div>
			</div>
		</div>
		
	</body>

</html>