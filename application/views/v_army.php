<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Edom Fantasy - Army</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
		
		<link rel="icon" href="assets/images/bgr/favicon.ico" type="image" sizes="16x16">
		
		<style>
			body {
				background-image:url("assets/images/bgr/nationbgr.jpg");
			}
			@font-face {font-family: "Vivaldi Italic V1";
				src: url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.eot"); /* IE9*/
				src: url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.eot?#iefix") format("embedded-opentype"), /* IE6-IE8 */
				url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.woff2") format("woff2"), /* chrome、firefox */
				url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.woff") format("woff"), /* chrome、firefox */
				url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.ttf") format("truetype"), /* chrome、firefox、opera、Safari, Android, iOS 4.2+*/
				url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.svg#Vivaldi Italic V1") format("svg"); /* iOS 4.1- */
			}
			thead {
				font-weight: bold;
				color: white;
				font-size: 15px;
			}
			.navbar-brand {
				font-family: Vivaldi Italic V1;
				font-weight: bold;
				font-size: 40px;
				color: white;
			}
			.panel-transparent {
				background: none;
			}
			.panel-heading {
				font-family: Vivaldi Italic V1;
				font-weight: bold;
				color: white;
				font-size: 40px;
			}
		</style>
	</head>
	
	<body>
	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="about">Edom Fantasy</a>
			</div>
			<ul class="nav navbar-nav">
				<li><a href="home"><span class="glyphicon glyphicon-home"></span> Home</a></li>
				<li><a href="players">Players</a></li>
				<li><a href="reports">Reports<?php if ($numOfReports != 0) echo " (".$numOfReports.")"; ?></a></li>
				<li><a href="messages">Messages<?php if ($numOfMessages != 0) echo " (".$numOfMessages.")"; ?></a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a><span class="glyphicon glyphicon-queen"></span><span id="goldinc"> <?php echo $currentResources['gold']; ?></span></a></li>
				<li><a><span class="glyphicon glyphicon-grain"></span><span id="foodinc"> <?php echo $currentResources['food']; ?></span></a></li>
				<li><a><span class="glyphicon glyphicon-tree-conifer"></span><span id="woodinc"> <?php echo $currentResources['wood']; ?></span></a></li>
				<li><a></a></li> <!--Empty space-->
				<li><a href="logout"><span class="glyphicon glyphicon-log-out"></span> Log out</a></li>
			</ul>
		</div>
	</nav>
		
		
		<div class="panel panel-transparent">
			<div class="panel-heading" align="center">Your Army</div>
			<div class="container">
				<table class="table" style="width:40%" align="center">
					<thead>
						<tr>
							<th class="col-md-2"><p align="center">Unit</p></th>
							<th class="col-md-2"><p align="center">Count</p></th>
						</tr>
					</thead>
					<tbody>

					<?php
					foreach ($armies as $army)
					{ ?>

						<tr class="active">
							<td><p align="center"><?php echo $army['name']; ?></p></td>
							<td><p align="center"><?php echo $army['count'] ?></p></td>
						</tr>

					<?php
					} ?>

					</tbody>
				</table>
			</div>
		</div>
		
		<div class="panel panel-transparent">
			<div class="panel-heading" align="center">Sent Reinforcement</div>
			<div class="container">
				<table class="table" style="width:40%" align="center">
					<thead>
						<tr>
							<th class="col-md-3"><p align="center">Player</p></th>
							<th class="col-md-3"><p align="center">Unit</p></th>
							<th class="col-md-2"><p align="center">Count</p></th>
							<th class="col-md-1"><p align="center">Return</p></th>
						</tr>
					</thead>
					<tbody>

					<?php
					foreach ($sentReinforcements as $reinforcement)
					{ ?>

						<tr class="active" id="<?php echo $reinforcement['id'] ?>">
							<td><p align="center" ><?php echo $reinforcement['player']; ?></p></td>
							<td><p align="center"><?php echo $reinforcement['type']; ?></p></td>
							<td><p align="center"><?php echo $reinforcement['count']; ?></p></td>
							<td><p align="center"><a href="#" class="icon" name="bringBack"><span class="glyphicon glyphicon-remove" style="color:black"></span></a></p></td>
						</tr>

					<?php
					} ?>

					</tbody>
				</table>
			</div>
		</div>
		
		<div class="panel panel-transparent">
			<div class="panel-heading" align="center">Received Reinforcement</div>
			<div class="container">
				<table class="table" style="width:40%" align="center">
					<thead>
						<tr>
							<th class="col-md-3"><p align="center">Player</p></th>
							<th class="col-md-3"><p align="center">Unit</p></th>
							<th class="col-md-2"><p align="center">Count</p></th>
							<th class="col-md-1"><p align="center">Return</p></th>
						</tr>
					</thead>
					<tbody>

					<?php
					foreach ($receivedReinforcements as $reinforcement)
					{ ?>

						<tr class="active" id="<?php echo $reinforcement['id'] ?>">
							<td><p align="center"><?php echo $reinforcement['player']; ?></p></td>
							<td><p align="center"><?php echo $reinforcement['type'] ?></p></td>
							<td><p align="center"><?php echo $reinforcement['count']; ?></p></td>
							<td><p align="center"><a href="#" class="icon" name="return"><span class="glyphicon glyphicon-remove" style="color:black"></span></a></p></td>
						</tr>

					<?php
					} ?>

					</tbody>
				</table>
			</div>
		</div>

	<script>
		$(document).ready(function()
		{
			var goldDelay = <?php echo $productionIncrement['goldinc']; ?>;
			var foodDelay = <?php echo $productionIncrement['foodinc']; ?>;
			var woodDelay = <?php echo $productionIncrement['woodinc']; ?>;
			var nowGold, nowFood, nowWood, beforeGold = new Date(), beforeFood = new Date(), beforeWood = new Date();

			setInterval(function () {
				nowGold = new Date();
				var elapsedTime = (nowGold.getTime() - beforeGold.getTime());
				$('#goldinc').html(function (i, val) {
					var value;
					if (elapsedTime > goldDelay)
						value = Math.floor(elapsedTime / goldDelay);
					else
						value = 1;

					beforeGold = new Date();
					return (parseInt(val) + value).toString();
				})
			}, goldDelay);

			setInterval(function () {
				nowFood = new Date();
				var elapsedTime = (nowFood.getTime() - beforeFood.getTime());
				$('#foodinc').html(function (i, val) {
					var value;
					if (elapsedTime > foodDelay)
						value = Math.floor(elapsedTime / foodDelay);
					else
						value = 1;

					beforeFood = new Date();
					return (parseInt(val) + value).toString();
				})
			}, foodDelay);

			setInterval(function () {
				nowWood = new Date();
				var elapsedTime = (nowWood.getTime() - beforeWood.getTime());
				$('#woodinc').html(function (i, val) {
					var value;
					if (elapsedTime > woodDelay)
						value = Math.floor(elapsedTime / woodDelay);
					else
						value = 1;

					beforeWood = new Date();
					return (parseInt(val) + value).toString();
				})
			}, woodDelay);

			$('.icon').on('click', function (event)
			{
				event.preventDefault();

				var name = $(this).closest('a').attr('name');
				var id = $(this).closest('tr').attr('id');
				$(this).closest('tr').remove();

				if(name == "bringBack")
				{
					var form_data = {
						id: id,
						isAjaxCalling: true
					};
					$.ajax({
						url: '<?php echo site_url('bringback-reinforcement'); ?>',
						type: 'POST',
						data: form_data,
						success: function (msg)
						{
							if(msg)
							{
								if(msg.outcome == "true")
								{
									$(this).closest('tr').remove();
								}
							}
						},
						dataType: 'json'
					});
				}
				else if(name == "return")
				{
					var form_data = {
						id: id,
						isAjaxCalling: true
					};
					$.ajax({
						url: '<?php echo site_url('return-reinforcement'); ?>',
						type: 'POST',
						data: form_data,
						success: function (msg)
						{
							if(msg)
							{
								if(msg.outcome == "true")
								{
									$(this).closest('tr').remove();
								}
							}
						},
						dataType: 'json'
					});
				}
			});
		});
	</script>
	
	</body>
	
</html>