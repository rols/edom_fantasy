<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Edom Fantasy - AdminMessages</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
		
		<link rel="icon" href="assets/images/bgr/favicon.ico" type="image" sizes="16x16">
		
		<style>
			body {
				background-image:url("assets/images/bgr/nationbgr.jpg");
			}
			@font-face {font-family: "Vivaldi Italic V1";
				src: url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.eot"); /* IE9*/
				src: url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.eot?#iefix") format("embedded-opentype"), /* IE6-IE8 */
				url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.woff2") format("woff2"), /* chrome、firefox */
				url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.woff") format("woff"), /* chrome、firefox */
				url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.ttf") format("truetype"), /* chrome、firefox、opera、Safari, Android, iOS 4.2+*/
				url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.svg#Vivaldi Italic V1") format("svg"); /* iOS 4.1- */
			}
			thead {
				font-weight: bold;
				color: white;
				font-size: 15px;
			}
			textarea {
				resize: none;
			}
			.navbar-brand {
				font-family: Vivaldi Italic V1;
				font-weight: bold;
				font-size: 40px;
				color: white;
			}
			.panel-transparent {
				background: none;
			}
			.panel-heading {
				font-family: Vivaldi Italic V1;
				font-weight: bold;
				color: white;
				font-size: 40px;
			}
		</style>
	</head>
	
	<body>
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="about">Edom Fantasy</a>
				</div>
				<ul class="nav navbar-nav">
					<li><a href="admin"><span class="glyphicon glyphicon-home"></span> Home</a></li>
					<li class="active"><a href="messages-admin">Messages<?php if ($numOfMessages != 0) echo " (".$numOfMessages.")"; ?></a></li>
				</ul>
				<!--Current resources Gold, Food, Wood-->
				<ul class="nav navbar-nav navbar-right">
					<li><a href="logout"><span class="glyphicon glyphicon-log-out"></span> Log out</a></li>
				</ul>
			</div>
		</nav>
		
		
		<div class="panel panel-transparent">
			<div class="panel-heading" align="center">Messages</div>
			<div class="container">
				<table class="table" style="width: 50%" align="center">
					<thead>
						<tr>
							<th class="col-md-2"><p align="center">From</p></th>
							<th class="col-md-2"><p align="center">To</p></th>
							<th class="col-md-2"><p align="center">Subject</p></th>
							<th class="col-md-1"><p align="center">Delete</p></th>
						</tr>
					</thead>
					<tbody>

					<?php
					foreach ($messages as $message)
					{ ?>

						<tr id="<?php echo $message['idmes']; ?>" class="<?php if ($message['isread'] == 0 && strcmp($_SESSION['username'], $message['sender']) != 0) echo "success"; else echo "active"; ?>">
							<td><p align="center"><?php echo $message['sender']; ?></p></td>
							<td><p align="center"><?php echo $message['receiver']; ?></p></td>
							<td><a class="message" href="#" data-toggle="modal" data-target="#messagemodal" style="color:black"><p align="center"><b><?php echo $message['subject']; ?></b></p></a></td>
							<td><p align="center"><a href="#" class="icon"> <span class="glyphicon glyphicon-trash" style="color:black"></span></a></p></td>
						</tr>

					<?php
					} ?>

					</tbody>
				</table>
			</div>
		</div>


		<div class="modal fade" id="messagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-header">
						<button id="closeMsg" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h3 class="modal-title" id="subject_text" align="center"></h3>
					</div>
					<div class="modal-body">
						<p id="content_text" style="word-wrap:break-word" align="center"></p>
					</div>
				</div>
			</div>
		</div>

		<script>
			$(document).ready(function()
			{
				$('.icon').on('click', function (event)
				{
					event.preventDefault();

					var idmes = $(this).closest('tr').attr('id');
					$(this).closest('tr').remove();

					var form_data = {
						idmes: idmes,
						isAjaxCalling: true
					};
					$.ajax({
						url: '<?php echo site_url('delete-message'); ?>',
						type: 'POST',
						data: form_data
					});
				});

				$('.message').on('click', function()
				{
					var idmes = $(this).closest('tr').attr('id');
					var msgClass = $(this).closest('tr');
					var msgClassAtr = msgClass.attr('class');
					var updateNeeded;

					if (msgClassAtr == 'success')
					{
						msgClass.removeClass('success');
						msgClass.addClass('active');
						updateNeeded = true;
					}
					else
						updateNeeded = false;

					var form_data = {
						idmes: idmes,
						updateNeeded: updateNeeded,
						isAjaxCalling: true
					};
					$.ajax({
						url: '<?php echo site_url('read-message'); ?>',
						type: 'POST',
						data: form_data,
						success: function(msg) {
							if (msg)
							{
								$('#subject_text').html(msg.subject);
								$('#content_text').html(msg.content);
							}
						},
						dataType:"json"
					});
				});

				$('#closeMsg').on('click', function()
				{
					$('#subject_text').text("");
					$('#content_text').text("");
				});
			});
		</script>
		
	</body>
	
</html>