<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Edom Fantasy - Messages</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
		
		<link rel="icon" href="assets/images/bgr/favicon.ico" type="image" sizes="16x16">
		
		<style>
			body {
				background-image:url("assets/images/bgr/nationbgr.jpg");
			}
			@font-face {font-family: "Vivaldi Italic V1";
				src: url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.eot"); /* IE9*/
				src: url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.eot?#iefix") format("embedded-opentype"), /* IE6-IE8 */
				url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.woff2") format("woff2"), /* chrome、firefox */
				url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.woff") format("woff"), /* chrome、firefox */
				url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.ttf") format("truetype"), /* chrome、firefox、opera、Safari, Android, iOS 4.2+*/
				url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.svg#Vivaldi Italic V1") format("svg"); /* iOS 4.1- */
			}
			thead {
				font-weight: bold;
				color: white;
				font-size: 15px;
			}
			textarea {
				resize: none;
			}
			a.message {

			}
			.navbar-brand {
				font-family: Vivaldi Italic V1;
				font-weight: bold;
				font-size: 40px;
				color: white;
			}
			.panel-transparent {
				background: none;
			}
			.panel-heading {
				font-family: Vivaldi Italic V1;
				font-weight: bold;
				color: white;
				font-size: 40px;
			}
		</style>
	</head>
	
	<body>
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="about">Edom Fantasy</a>
				</div>
				<ul class="nav navbar-nav">
					<li><a href="home"><span class="glyphicon glyphicon-home"></span> Home</a></li>
					<li><a href="players">Players</a></li>
					<li><a href="reports">Reports<?php if ($numOfReports != 0) echo " (".$numOfReports.")"; ?></a></li>
					<li class="active"><a href="messages">Messages<?php if ($numOfMessages != 0) echo " (".$numOfMessages.")"; ?></a></li>
				</ul>
				<!--Current resources Gold, Food, Wood-->
				<ul class="nav navbar-nav navbar-right">
					<li><a><span class="glyphicon glyphicon-queen"></span><span id="goldinc"> <?php echo $currentResources['gold']; ?></span></a></li>
					<li><a><span class="glyphicon glyphicon-grain"></span><span id="foodinc"> <?php echo $currentResources['food']; ?></span></a></li>
					<li><a><span class="glyphicon glyphicon-tree-conifer"></span><span id="woodinc"> <?php echo $currentResources['wood']; ?></span></a></li>
					<li><a></a></li> <!--Empty space-->
					<li><a href="#" id="modreport" data-toggle="modal" data-target="#modreportmodal"><span class="glyphicon glyphicon-alert"></span> Report</a></li>
					<li><a href="logout"><span class="glyphicon glyphicon-log-out"></span> Log out</a></li>
				</ul>
			</div>
		</nav>
		
		
		<div class="panel panel-transparent">
			<div class="panel-heading" align="center">Messages</div>
			<div class="container">
				<table class="table" style="width: 50%" align="center">
					<thead>
						<tr>
							<th class="col-md-2"><p align="center">From</p></th>
							<th class="col-md-2"><p align="center">To</p></th>
							<th class="col-md-2"><p align="center">Subject</p></th>
							<th class="col-md-1"><p align="center">Delete</p></th>
						</tr>
					</thead>
					<tbody>

					<?php
					foreach ($messages as $message)
					{ ?>

						<tr id="<?php echo $message['idmes']; ?>" class="<?php if ($message['isread'] == 0 && strcmp($_SESSION['username'], $message['sender']) != 0) echo "success"; else echo "active"; ?>">

							<?php
							if ($_SESSION['username'] == $message['sender'])
							{?>

							<td><p align="center"><?php echo $message['sender']; ?></p></td>

							<?php
							} else
							{?>

							<td><a href="player-profile?username=<?php echo $message['sender'] ?>" style="color:black"><p align="center"><b><?php echo $message['sender']; ?></b></p></a></td>

							<?php
							}
							if ($_SESSION['username'] == $message['receiver'])
							{?>

							<td><p align="center"><?php echo $message['receiver']; ?></p></td>

							<?php
							} else
							{?>

							<td><a href="player-profile?username=<?php echo $message['receiver'] ?>" style="color:black"><p align="center"><b><?php echo $message['receiver']; ?></b></p></a></td>

							<?php
							}?>

							<td><a class="message" href="#" data-toggle="modal" data-target="#messagemodal" style="color:black"><p align="center"><b><?php echo $message['subject']; ?></b></p></a></td>
							<td><p align="center"><a href="#" class="icon"> <span class="glyphicon glyphicon-trash" style="color:black"></span></a></p></td>

						</tr>

					<?php
					} ?>

					</tbody>

				</table>
			</div>
		</div>
		
		
		<div class="modal fade" id="messagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-header">
						<button id="closeMsg" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h3 class="modal-title" id="subject_text" align="center"></h3>
					</div>
					<div class="modal-body">
						<p id="content_text" style="word-wrap:break-word" align="center"></p>
					</div>
				</div>
			</div>
		</div>
		
		<div class="modal fade" id="modreportmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h3 class="modal-title" id="myModalLabel" align="center" style="font-family: vivaldi; font-weight: bold; font-size: 40px">Report Problem</h3>
					</div>
					<div class="modal-body">

					<?php
					$attributes = array("role" => "form", "id" => "form_send-message");
					echo form_open('send-message', $attributes);
					?>

					<div class="form-group">
						<label for="subject">Subject:</label>
						<input type="text" class="form-control" id="subject" maxlength="30" style="text-align:left">
						<label for="content">Content:</label>
						<textarea class="form-control" rows="5" id="content" maxlength="200"></textarea>
					</div>
					<div class="form-group" align="center">
						<button type="submit" class="btn btn-default">Send</button>
					</div>

					<?php
					echo form_close();
					?>

					</div>
				</div>
			</div>
		</div>

		<script>
			$(document).ready(function()
			{
				var goldDelay = <?php echo $productionIncrement['goldinc']; ?>;
				var foodDelay = <?php echo $productionIncrement['foodinc']; ?>;
				var woodDelay = <?php echo $productionIncrement['woodinc']; ?>;
				var nowGold, nowFood, nowWood, beforeGold = new Date(), beforeFood = new Date(), beforeWood = new Date();

				setInterval(function () {
					nowGold = new Date();
					var elapsedTime = (nowGold.getTime() - beforeGold.getTime());
					$('#goldinc').html(function (i, val) {
						var value;
						if (elapsedTime > goldDelay)
							value = Math.floor(elapsedTime / goldDelay);
						else
							value = 1;

						beforeGold = new Date();
						return (parseInt(val) + value).toString();
					})
				}, goldDelay);

				setInterval(function () {
					nowFood = new Date();
					var elapsedTime = (nowFood.getTime() - beforeFood.getTime());
					$('#foodinc').html(function (i, val) {
						var value;
						if (elapsedTime > foodDelay)
							value = Math.floor(elapsedTime / foodDelay);
						else
							value = 1;

						beforeFood = new Date();
						return (parseInt(val) + value).toString();
					})
				}, foodDelay);

				setInterval(function () {
					nowWood = new Date();
					var elapsedTime = (nowWood.getTime() - beforeWood.getTime());
					$('#woodinc').html(function (i, val) {
						var value;
						if (elapsedTime > woodDelay)
							value = Math.floor(elapsedTime / woodDelay);
						else
							value = 1;

						beforeWood = new Date();
						return (parseInt(val) + value).toString();
					})
				}, woodDelay);

				var	formSendMessage = $('#form_send-message');
				formSendMessage.submit(function(event)
				{
					removeMessages();

					var subject = $.trim($('#subject').val()),
						content = $.trim($('#content').val());

					if (content.length == 0)
					{
						displayMessage("Fill the content.", "alert-danger", formSendMessage);
						return false;
					}
					else
					{
						if (subject.length == 0)
							subject = 'No Subject';


						/* AJAX CALL */
						var form_data = {
							subject: subject,
							content: content,
							isAjaxCalling: true
						};
						$.ajax({
							url: '<?php echo site_url('send-report-to-mod'); ?>',
							type: 'POST',
							data: form_data,
							success: function(msg) {
								if (msg == true)
								{
									$('#subject').val("");
									$('#content').val("");
									displayMessage("Report sent.", "alert-success", formSendMessage);
								}

								else
									displayMessage("Report not sent.", "alert-danger", formSendMessage);
							}
						});
						return false;
					}
				});

				function displayMessage(msg, alert, field)
				{
					var t1 = "<div id=\"msgAlert\" class=\"alert " + alert + " fade in flash_message\">";
					var t2 = "<strong>" + msg + "</strong>";
					var t3 = "</div>";

					field.before(t1 + t2 + t3);
				}

				function removeMessages()
				{
					$('#msgAlert').remove();
				}

				$('#modreport').on('click', function() {removeMessages();});

				$('.icon').on('click', function (event)
				{
					event.preventDefault();

					var idmes = $(this).closest('tr').attr('id');
					$(this).closest('tr').remove();

					var form_data = {
						idmes: idmes,
						isAjaxCalling: true
					};
					$.ajax({
						url: '<?php echo site_url('delete-message'); ?>',
						type: 'POST',
						data: form_data
					});
				});

				$('.message').on('click', function()
				{
					var idmes = $(this).closest('tr').attr('id');
					var msgClass = $(this).closest('tr');
					var msgClassAtr = msgClass.attr('class');
					var updateNeeded;

					if (msgClassAtr == 'success')
					{
						msgClass.removeClass('success');
						msgClass.addClass('active');
						updateNeeded = true;
					}
					else
						updateNeeded = false;

					var form_data = {
						idmes: idmes,
						updateNeeded: updateNeeded,
						isAjaxCalling: true
					};
					$.ajax({
						url: '<?php echo site_url('read-message'); ?>',
						type: 'POST',
						data: form_data,
						success: function(msg) {
							if (msg)
							{
								$('#subject_text').html(msg.subject);
								$('#content_text').html(msg.content);
							}
						},
						dataType:"json"
					});
				});

				$('#closeMsg').on('click', function()
				{
					$('#subject_text').text("");
					$('#content_text').text("");
				});

			});
		</script>
		
	</body>
	
</html>