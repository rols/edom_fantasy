<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Edom Fantasy - Reports</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
		
		<link rel="icon" href="assets/images/bgr/favicon.ico" type="image" sizes="16x16">
		
		<style>
			body {
				background-image:url("assets/images/bgr/nationbgr.jpg");
			}
			@font-face {font-family: "Vivaldi Italic V1";
				src: url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.eot"); /* IE9*/
				src: url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.eot?#iefix") format("embedded-opentype"), /* IE6-IE8 */
				url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.woff2") format("woff2"), /* chrome、firefox */
				url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.woff") format("woff"), /* chrome、firefox */
				url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.ttf") format("truetype"), /* chrome、firefox、opera、Safari, Android, iOS 4.2+*/
				url("assets/fonts/6797572334cc1ebc2d390a52f6d34e01.svg#Vivaldi Italic V1") format("svg"); /* iOS 4.1- */
			}
			thead {
				font-weight: bold;
				color: white;
				font-size: 15px;
			}
			.navbar-brand {
				font-family: Vivaldi Italic V1;
				font-weight: bold;
				font-size: 40px;
				color: white;
			}
			.panel-transparent {
				background: none;
			}
			.panel-heading {
				font-family: Vivaldi Italic V1;
				font-weight: bold;
				color: white;
				font-size: 40px;
			}
		</style>
	</head>
	
	<body>
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="about">Edom Fantasy</a>
				</div>
				<ul class="nav navbar-nav">
					<li><a href="home"><span class="glyphicon glyphicon-home"></span> Home</a></li>
					<li><a href="players">Players</a></li>
					<li class="active"><a href="reports">Reports<?php if ($numOfReports != 0) echo " (".$numOfReports.")"; ?></a></li>
					<li><a href="messages">Messages<?php if ($numOfMessages != 0) echo " (".$numOfMessages.")"; ?></a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><a><span class="glyphicon glyphicon-queen"></span><span id="goldinc"> <?php echo $currentResources['gold']; ?></span></a></li>
					<li><a><span class="glyphicon glyphicon-grain"></span><span id="foodinc"> <?php echo $currentResources['food']; ?></span></a></li>
					<li><a><span class="glyphicon glyphicon-tree-conifer"></span><span id="woodinc"> <?php echo $currentResources['wood']; ?></span></a></li>
					<li><a></a></li> <!--Empty space-->
					<li><a href="logout"><span class="glyphicon glyphicon-log-out"></span> Log out</a></li>
				</ul>
			</div>
		</nav>
		
		
		<div class="panel panel-transparent">
			<div class="panel-heading" align="center">Reports</div>
			<div class="container">
				<table class="table" style="width:50%" align="center">
					<thead>
						<tr>
							<th class="col-md-3"><p align="center">Type</p></th>
							<th class="col-md-3"><p align="center">Time</p></th>
							<th class="col-md-1"><p align="center">Report</p></th>
							<th class="col-md-1"><p align="center">Delete</p></th>
						</tr>
					</thead>
					<tbody>

					<?php
					foreach ($reports as $report)
					{ ?>

						<tr id="<?php echo $report['idrep']; ?>" class="<?php if ($report['isread'] == 0) echo "success"; else echo "active"; ?>">
							<td><p align="center"><?php echo $report['type']; ?></p></td>
							<td><p align="center"><?php echo $report['time']; ?></p></td>
							<td><p align="center"><a class="report" href="#" data-toggle="modal" data-target="#reportmodal"><span class="glyphicon glyphicon-envelope" style="color:black"></span></a></p></td>
							<td><p align="center"><a href="#" class="icon"><span class="glyphicon glyphicon-trash" style="color:black"></span></a></p></td>
						</tr>

					<?php
					} ?>

					</tbody>
				</table>
			</div>
		</div>
		
		
		<div class="modal fade" id="reportmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog modal-md">
				<div class="modal-content">
					<div class="modal-header">
						<button id="closeRep" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h3 class="modal-title" id="report_type" align="center"></h3>
					</div>
					<div class="modal-body">
						<p id="report_text" align="center"></p>
					</div>
				</div>
			</div>
		</div>

		<script>
			$(document).ready(function()
			{
				var goldDelay = <?php echo $productionIncrement['goldinc']; ?>;
				var foodDelay = <?php echo $productionIncrement['foodinc']; ?>;
				var woodDelay = <?php echo $productionIncrement['woodinc']; ?>;
				var nowGold, nowFood, nowWood, beforeGold = new Date(), beforeFood = new Date(), beforeWood = new Date();

				setInterval(function () {
					nowGold = new Date();
					var elapsedTime = (nowGold.getTime() - beforeGold.getTime());
					$('#goldinc').html(function (i, val) {
						var value;
						if (elapsedTime > goldDelay)
							value = Math.floor(elapsedTime / goldDelay);
						else
							value = 1;

						beforeGold = new Date();
						return (parseInt(val) + value).toString();
					})
				}, goldDelay);

				setInterval(function () {
					nowFood = new Date();
					var elapsedTime = (nowFood.getTime() - beforeFood.getTime());
					$('#foodinc').html(function (i, val) {
						var value;
						if (elapsedTime > foodDelay)
							value = Math.floor(elapsedTime / foodDelay);
						else
							value = 1;

						beforeFood = new Date();
						return (parseInt(val) + value).toString();
					})
				}, foodDelay);

				setInterval(function () {
					nowWood = new Date();
					var elapsedTime = (nowWood.getTime() - beforeWood.getTime());
					$('#woodinc').html(function (i, val) {
						var value;
						if (elapsedTime > woodDelay)
							value = Math.floor(elapsedTime / woodDelay);
						else
							value = 1;

						beforeWood = new Date();
						return (parseInt(val) + value).toString();
					})
				}, woodDelay);


				$('.icon').on('click', function (event)
				{
					event.preventDefault();

					var idrep = $(this).closest('tr').attr('id');
					$(this).closest('tr').remove();

					var form_data = {
						idrep: idrep,
						isAjaxCalling: true
					};
					$.ajax({
						url: '<?php echo site_url('delete-report'); ?>',
						type: 'POST',
						data: form_data
					});
				});

				$('.report').on('click', function(){
					var idrep = $(this).closest('tr').attr('id');
					var repClass = $(this).closest('tr');
					var repClassAtr = repClass.attr('class');
					var updateNeeded;

					if (repClassAtr == 'success')
					{
						repClass.removeClass('success');
						repClass.addClass('active');
						updateNeeded = true;
					}
					else
						updateNeeded = false;

					var form_data = {
						idrep: idrep,
						updateNeeded: updateNeeded,
						isAjaxCalling: true
					};
					$.ajax({
						url: '<?php echo site_url('read-report'); ?>',
						type: 'POST',
						data: form_data,
						success: function(rep) {
							if (rep)
							{
								$('#report_type').text(rep.type);
								$('#report_text').html(rep.content);
							}
						},
						dataType:"json"
					});
				});

				$('#closeRep').on('click', function()
				{
					$('#report_type').text("");
					$('#report_text').text("");
				});

			});
		</script>
		
	</body>
	
</html>